***************************
🏅 - Éléments de correction
***************************


Préalable
=========

Pour tester le fonctionnement des vues, il faudra du contenus, et en quantité
suffisante pour tester correctement. Pour cela, vous pouvez utiliser le module
**Devel generate** pour créer, très rapidement des contenus factices et facilement
supprimables.

.. code-block:: bash

  # Root of the Drupal project

  # Create 30 activities
  vendor/bin/drush genc 12 --bundles activity
  # Create 50 users
  vendor/bin/drush genu 50


Niveau 1  : Page "Tous les articles"
====================================

* Créer une page ;
* Rendre les entités ``Article`` sous le format ``teaser`` ;
* Rajouter un critère de filtrage sur le champ de taxonomie des sports ;
* Exposer ce filtre de recherche ;
* Rajouter deux critères de tri : sur les contenus épinglés, puis par date de
  création ;
* Placer un bloc texte dans la zone ``header`` et utiliser les tokens pour afficher
  le nombre de résultats ;


Niveau 2 : Interface d'administration
=====================================

* Créer une page ;
* Rendre les champs sous forme de tableau ;
* Choisir le champs ``nid`` de l'article et l'exclure de l'affichage ;
* Ajouter les autres champs : sport, classe d'âge, horaire ;
* Configurer le champ horaire pour rendre un affichage custom, avec le format de
  date ``H\H:i`` ;
* Ajouter un champ ``Custom text`` et créer le lien d'action en utilisant les
  tokens disponibles - ``<a href="node/{{ nid }}/edit">Éditer</a>``
* Ajouter les critères de filtrage - sport, classe d'âges - et les exposer aux
  visiteurs ;
* Placer le lien de menu dans le menu d'administration du site - ex:
  Configuration > système [1]_ ;

.. [1] On pourra au préalable créer en back-office une entrée dans le menu
   d'administration dédiée aux interfaces de gestion de l'association.


Niveau 3 : Utiliser les filtres contextuels
===========================================

:underline:`Nouvelle vue :`

* Créer une page - path: ``/sam-manage-registred`` ;
* Rendre les champs sous forme de tableau ;
* Choisir le champs ``uid`` de l'utilisateur et l'exclure de l'affichage ;
* Ajouter les autres champs : mail, adresse, newsletter... ;
* Ajouter un champ ``Custom text`` et créer le lien d'action en utilisant les
  tokens disponibles - ``<a href="user/{{ uid }}/edit">Éditer</a>``
* Ajouter un filtre contextuel sur le champ référence des activités ;

:underline:`Vue Niveau 2 : Gestion des activités :`

* Transformer le lien du bouton d'action :
  ``<a href="sam-manage-registred/{{ nid }}">Voir les inscrits</a>``


Niveau 4 : Utiliser les relations entre entités
===============================================

:underline:`Vue Niveau 2 : Gestion des activités :`

* Ajouter une relation sur le terme de taxonomie des sports ;
* Ajouter une relation sur les utilisateurs - le système proposera de relier les
  entités utilisateurs référencés dans le champ de taxonomie des sport ;
* Ajouter dans le tableau les champs désormais disponibles : coach, mail ;
