.. role:: underline


****************
🤿 - Bien vues !
****************

⏱️ | 50 min.

!!!!

Nous allons adapter et enrichir les contenus de notre site avec différentes vues,
pour le front-office mais également pour l'administration courante de l'association
en back-office.

:underline:`Niveau 1 : Créer une page "Tous les articles"`

  Faire une page qui agrège tous les contenus de type ``Article`` et qui soit
  accessible dans le menu principal de navigation :

  * Rendre les articles sous forme teaser ;
  * Proposer un filtre de recherche sur le sport concerné ;
  * Ordonnancer les résultats en prenant en compte les articles épinglés, puis
    par ordre décroissant de publication ;
  * Afficher le nombre de résultats obtenus ;
  * Remonter 12 résultats par page ;
  * Utiliser la mini-pagination

:underline:`Niveau 2 : Créer une interface d'administration pour les activités
sportives`

  Faire une interface qui agrège tous les contenus de type ``activity`` et qui soit
  accessible dans le menu d'administration du site :

  * Faire une page et rendre les résultats sous forme de tableau ;
  * Créer un lien d'action Éditer" pour accéder à la fiche ;
  * Afficher plusieurs filtres de recherche : sport, classe d'âges ;
  * Faire fonctionner le filtre de recherche en ajax ;
  * Ordonnancer les résultats par sport puis par classe d'âges ;
  * Afficher le nombre de résultats obtenus ;
  * Remonter 25 résultats par page ;
  * Accessible aux gestionnaires uniquement ;
  * informations à afficher :

      * Nom ;
      * Sport ;
      * catégorie d'âge ;
      * Horaire - hh**H**mm;
      * Nombre d'inscrit ;

  Un petit plus si votre vue est rendue dans le thème de l'administration... Je
  vous laisse chercher 🤗 !

:underline:`Niveau 3 : Utiliser les filtres contextuels`

  Créer une vue dynamique qui permet de gérer les inscrits pour chaque activité
  sportive.

  * Faire une page et rendre les résultats sous forme de tableau ;
  * Afficher quelques informations sur l'utilisateur :  mail, adresse, newsletter... ;
  * Créer un lien d'action ``Éditer`` pour accéder au formulaire d'édition de
    l'utilisateur ;

  Reprendre la vue Niveau 2, et modifier le bouton d'action pour accéder aux
  inscrits à l'activité.

  Et toujours le petit plus si votre vue est rendue dans le thème de l'administration !

:underline:`Niveau 4 : Utiliser les relations entre entités`

  Reprendre la vue Niveau 2, et afficher le nom et le courriel du coach.
