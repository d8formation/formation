.. |br| raw:: html

  <br />


*****************
BDD : généralités
*****************


.. admonition:: Documentation
  :class: Note

  `Database API`_ ;

.. _Database API : https://www.drupal.org/docs/8/api/database-api


Comme nous l'avons déjà abordé dans la partie concernant la `structure des données`_,
Drupal manipule essentiellement des entités - qu'elles soient créées en back-office
ou totalement personnalisées. C'est le premier niveau de la gestion des informations,
qui est totalement intégré au framework, mais vous pouvez aller plus loin :

* Vous pouvez utiliser une ou plusieurs autres bases de données existantes ;
* Vous pouvez créer vos propres tables dans la base de données ;

De fait, il faut considérer deux types de données avec pour chacun des mécanismes
spécifiques pour les manipuler :

* Les entités, informations entièrement gérées par le framework ;
* Vos propres données, non structurée par le framework ;

.. _structure des données: ../01_concepts_drupal/structure_donnees.html#configurer-une-entite-de-contenu


Le schéma de la base de données de Drupal
=========================================

La base de données Drupal est structurée par :

* L'implémentation des modules - qu'ils soient du core ou communautaires ;
* Les tables créées avec les champs des entités ;
* Vos tables personnelles ;

.. figure:: _static/database_field_management.png
    :align: center
    :width: 100%


Utiliser une autre base de données
==================================

Pour pouvoir utiliser une autre base de données, il faut la déclarer dans le
fichier de configuration ``setting.php``

.. code-block:: php

  $databases['second_db']['default'] = [
    'database' => 'db_ext',
    'username' => 'root',
    'password' => '',
    'prefix' => '',
    'host' => 'path/to/another/host',
    'port' => '3306',
    'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
    'driver' => 'mysql',
  ];


Créer ses propres tables
========================

La `Schema API`_
est une API qui vous permet de décrire vos tables, vos clé et vos index. Deux
solutions pour créer vos tables, fonction du contexte du projet :

.. _Schema API: https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Database!database.api.php/group/schemaapi/8.8.x

**À l'initiation du projet, lors de la création de votre module**

  Le `hook_schema()`_, déclaré dans le fichier ``[module_name].install`` de votre
  module, permet de créer des tables dans la base de données du système.

.. _hook_schema(): https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Database%21database.api.php/function/hook_schema/8.8.x

**Dans un projet existant**

  Le `hook_update_N()`_, déclaré dans le fichier ``[module_name].install`` permet
  mettre à jour la base de données du système. Il se déclenche de deux manières :

  * En ligne de commande, avec Drush : ``drush updb``
  * En back-office, à l'adresse ``/update.php``

.. _hook_update_N(): https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Extension!module.api.php/function/hook_update_N/8.8.x


Pour aller plus loin
====================

* `Introduction to Schema API`_ *versionné D7 mais toujours valable dans les
  grandes lignes pour D8/D9* ;
* `Introduction to update API`_ ;

.. _Introduction to Schema API: https://www.drupal.org/node/146843
.. _Introduction to update API: https://www.drupal.org/docs/8/api/update-api/introduction-to-update-api-for-drupal-8
