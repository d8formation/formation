.. |br| raw:: html

  <br />

.. role:: red
.. role:: underline


********
Les vues
********


Les vues sont des agrégations de contenus, filtrées et ordonnancées selon de nombreux
critères configurables en back-office. Elles permettent de créer de nombreuses
interfaces :

* Des pages de contenus : liste d'articles, liste d'annonces, les prochains événements... ;
* Des interfaces d'administration : gestion des articles, des utilisateurs... ;
* Des listes de liens... ;

:underline:`Une illustration :` les interfaces de gestion des utilisateurs, des
contenus sont des vues. Elles sont donc personnalisables !

Les vues peuvent être rendues dans des pages, des blocs, des flux rss. Elles peuvent
être dynamiques et générées de manière contextuelle fonction d'un id, d'une date
passé en paramètre.

Créer une vue
=============

Un premier niveau de configuration :

* Le nom de la vue ;
* Les paramètres d’agrégation ;
* Le composant à créer : une page, un block ;

.. figure:: _static/views_add.png
  :align: center
  :width: 100%

Configuration spécifique
========================

:red:`1` : Le ou les types d'entités à agréger ; |br|
:red:`2` : Le format pour rendre les résultats : une liste, un tableau... ; |br|
:red:`3` : Le rendu graphique : un mode d'affichage **OU** les champs ; |br|
:red:`4` : Les critères de la recherche ; |br|
:red:`5` : L'ordonnancement des résultats ; |br|
:red:`6` : Les paramètres spécifiques de la vue : pour une page, un block, un
flux rss ... ;

.. figure:: _static/views_config.png
  :align: center
  :width: 100%

Pour aller plus loin dans la configuration
==========================================

Différentes options d'affichage
-------------------------------

:red:`4` : On expose aux visiteurs un ou plusieurs filtres de recherche sous la
forme d'un formulaire ; |br|
:red:`12` : **Et** ... on configure ce formulaire : exposition, ajax... ; |br|
:red:`7` : Des blocs d'information dans les zones *header* et *footer* de la vue ; |br|
:red:`8` : Un message à afficher en l'absence de résultats ; |br|
:red:`9` : La pagination / le nombre de résultats à remonter ; |br|

Des tokens peuvent être utilisés pour afficher des informations sur les données
de la vue.

.. figure:: _static/views_available_tokens.png
  :align: center
  :width: 100%

La sélection des données
------------------------

:red:`10` : Des filtres contextuels sont utilisables pour afficher les résultats
en fonction d'un paramètre passé dans l'url ; |br|
*Ex : la vue par défaut des termes de taxonomie* |br|
:red:`11` : Établir une relation entre les entités offre plus de filtres pour trier
et ordonnancer les résultats ; |br|

Pour le rendu graphique
-----------------------

Pour adapter au thème du site le rendu de la vue, il est possible de surcharger les
templates utilisés par défaut en les redéclarant dans le dossier de thème utilisé
par le site.

Liste des templates utilisés : ``/core/modules/views/templates``

.. figure:: _static/views_templates.png
  :align: center
  :width: 50%
