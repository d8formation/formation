.. |br| raw:: html

  <br />


*********
Les menus
*********


Gestion des menus
=================

Dans une installation standard, plusieurs menus sont disponibles  :

* Menu d'administration ;
* Menu principal ;
* Menu footer ;
* Menu utilisateur ;
* Menu tools ;

.. figure:: _static/menu_adm.png
  :align: center
  :width: 100%

Il est possible de créer ses propres menus, avec des liens en pointant vers :

* Un contenu spécifique du site : page institutionnelle, article... ;
* Une vue : liste d'articles, d'événements, de livres... ;
* Un controller provenant d'un module personnel : une interface de gestion spécifique... ;
* Une adresse extérieure à votre site ;

Un lien possède deux caractéristiques :

* Un niveau de profondeur ;
* Un poids, c'est à dire une position dans le menu ;


Menus et blocs
==============

Chaque menu existant, natif ou personnalisé, génère automatiquement un bloc,
positionnable dans le layout. Les paramètres de configuration sont les mêmes que
pour un bloc classique, à savoir :

* Par type de contenu ;
* Par page ;
* Par rôle ;

.. figure:: _static/menu_block.png
  :align: center
  :width: 100%
