<?php

namespace Drupal\sam\Service\TpService;

use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Language\LanguageManager;

/**
 * Class Lv2MailService.
 */
class Lv2MailService {

  /**
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * @var \Drupal\Core\Language\LanguageManager
   */
  protected $languageManager;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Mail\MailManagerInterface $mail_manager
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   */
  public function __construct(MailManagerInterface $mail_manager,
    LanguageManager $language_manager
  ) {
    $this->mailManager = $mail_manager;
    $this->languageManager = $language_manager;
  }

  /**
   * Send a notification to coach.
   */
  public function sendRegistrationActivityNotification($module, $key, $to, $activity, $name) {

    $langcode = $this->languageManager->getDefaultLanguage()->getId();
    $reply = NULL;
    $send = TRUE;
    $params['activity'] = $activity;
    $params['ref_name'] = $name;

    $this->mailManager->mail($module, $key, $to, $langcode, $params, $reply, $send);
  }
}
