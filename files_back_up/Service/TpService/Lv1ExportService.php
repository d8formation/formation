<?php

namespace Drupal\sam\Service\TpService;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class Lv1ExportService.
 */
class Lv1ExportService {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Get all members, by pole, registred since first day of the current year.
   * Return csv data.
   */
  public function exportRegistredUsers($tid) {
    $uids = $this->entityTypeManager->getStorage('user')
      ->getQuery()
      ->condition('field_activities', $tid)
      ->execute();
    $csv = $this->getCsvData($uids);

    return $this->returnCsv($csv, $tid);
  }

  /**
   * Get data in csv.
   */
  private function getCsvData(array $uids) {
    $handle = fopen('php://temp', 'w+');
    $header = [
      'Nom d\'utilisateur',
      'Email',
      'Enregistré le',
    ];
    fputcsv($handle, $header);

    $users = $this->entityTypeManager->getStorage('user')
      ->loadMultiple($uids);
    foreach ($users as $user) {
      $csv = [
        $user->getAccountName(),
        $user->getEmail(),
        date('d/m/Y', $user->getCreatedTime()),
      ];
      fputcsv($handle, $csv);
    }
    rewind($handle);
    $csv_data = stream_get_contents($handle);
    fclose($handle);

    return $csv_data;
  }

  /**
   * Return data in csv.
   */
  private function returnCsv($csv, $name) {
    $response = new Response();
    $response->headers->set('Content-Type', 'text/csv');
    $filename = 'export_' . $name . '_' . date("Y-m-d", time()) . '.csv';
    $response->headers->set('Content-Disposition', 'attachment; filename=' . $filename);
    $response->setContent($csv);

    return $response;
  }
}
