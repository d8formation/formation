<?php

//
// TP Views
//

/**
 * Implements hook_views_pre_render().
 */
function sam_views_pre_render($view) {
  if ($view->id() == 'sam_manage_activity' ) {
    foreach ($view->result as $index => $value) {
      $inscrits = $value->_entity->get('field_registred')->first()->getValue();
      $places = $value->_entity->get('field_available')->first()->getValue();
      $available = $places['value'] - $inscrits['value'];
      $view->result[$index]->_entity->set('field_available', ['value' => $available]);
    }
  }
}


// /**
//  * Implements hook_entity_operation_alter().
//  */
// function sam_entity_operation_alter(array &$operations, EntityInterface $entity) {
//   if ($entity->bundle() == 'activity') {
//     $operations['manage_suscriber'] = [
//       'title' => t('Manage suscriber'),
//       'weight' => 0,
//       'url' => Url::fromRoute('view.sam_manage_registred.page_1', ['arg_0' => $entity->id()]),
//     ];
//   }
// }
//


/**
 * Implements hook_ENTITY_TYPE_presave().
 * Implements hook_form_alter().
 */
function sam_user_presave(EntityInterface $entity) {
  // Subscribes to activities.
  if ($entity->isNew()) {
    if (!$entity->get('field_activities')->isEmpty()) {
      $field_activities = $entity->get('field_activities')->getValue();
      $activities = \array_column($field_activities, 'target_id');
      manage_subscription($activities, 'registration', $entity);
    }
  }
  // Manages subscription to activities.
  else {
    $new_activities = [];
    if (!$entity->get('field_activities')->isEmpty()) {
      $field_activities = $entity->get('field_activities')->getValue();
      $new_activities = \array_column($field_activities, 'target_id');
    }
    $old_activities = [];
    if (!$entity->original->get('field_activities')->isEmpty()) {
      $old_field_activities = $entity->original->get('field_activities')->getValue();
      $old_activities = \array_column($old_field_activities, 'target_id');
    }
    $registration = \array_diff($new_activities, $old_activities);
    $unsubscribe = \array_diff($old_activities, $new_activities);
    manage_subscription($registration, $entity->getAccountName(), 'registration');
    manage_subscription($unsubscribe, $entity->getAccountName(), 'unsubscribe');

  }
}

function sam_user_delete(EntityInterface $entity) {
  if (!$entity->get('field_activities')->isEmpty()) {
    $field_activities = $entity->get('field_activities')->getValue();
    $activities = \array_column($field_activities, 'target_id');
    manage_subscription($activities, $entity->getAccountName(), 'unsubscribe');
  }
}


/**
 * Manages subscription to activities :
 * - Update node activities,
 * - Send mail to coach and registred user.
 */
function manage_subscription($nids, $account, $op) {
  $activities = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($nids);
  foreach ($activities as $activity) {
    if (!$activity->hasField('field_registred')) {
      \Drupal::messenger()->addWarning($this->t('It\'s not possible to register for this activity'));
      continue;
    }
    $inscrit = $activity->get('field_registred')->first()->getValue();

    // TP Service niveau 2.
    // Get coach informations.
    $sport = $activity->get('field_sport')->first()->getValue();
    $term = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($sport['target_id']);
    $coach = $term->get('field_coach')->first()->getValue();
    $coach_account = \Drupal::entityTypeManager()->getStorage('user')->load($coach['target_id']);
    $coach_name = $coach_account->name->value;
    $coach_mail = $coach_account->mail->value;
    $sport = $activity->get('field_sport')->referencedEntities();
    $sport = \reset($sport);
    $coach = $sport->get('field_coach')->referencedEntities();
    $coach = \reset($coach);
    $coach_name = $coach->getAccountName();
    $coach_mail = $coach->getEmail();

    switch ($op) {
      case 'subscribe' :
        $inscrit['value'] += 1;
        // TP Service : Send mail to coach and user
        \Drupal::service('sam.mail_service')->sendRegistrationActivityNotification(
          'sam',
          'coach_notification_' . $op,
          $coach_mail,
          $activity->getTitle(),
          $account_name,
        );
        \Drupal::service('sam.mail_service')->sendRegistrationActivityNotification(
          'sam',
          'user_notification_' . $op,
          $account_mail,
          $activity->getTitle(),
          $coach_name,
        );
        break;
      case 'unsubscribe' :
        $inscrit['value'] -= 1;
        // dump(); die;
        // TP Service : Send mail to coach and user
        \Drupal::service('sam.mail_service')->sendRegistrationActivityNotification(
          'sam',
          'user_notification_' . $op,
          $coach_mail,
          $activity->getTitle(),
          $account_name,
        );
        \Drupal::service('sam.mail_service')->sendRegistrationActivityNotification(
          'sam',
          'user_notification_' . $op,
          $account_mail,
          $activity->getTitle(),
          $coach_name
        );
        break;
    }
    $activity->set('field_registred', $inscrit);
    $activity->save();
    \Drupal::service('sam.mail_service')->sendRegistrationActivityNotification(
      'sam',
      'coach_notification_' . $op,
      $coach_mail,
      $activity->getTitle(),
      $account,
    );
  }
}
