.. |br| raw:: html

  <br />

.. role:: underline


*********************
La gestion des thèmes
*********************

Il faut distinguer deux aspects de la gestion des thèmes :

* le thème pour le front-office,
* le thème pour le back-office.

Alors que le thème de front-office est accessible à tous les visiteurs - normal
🤗 - l'accès au thème d'administration et à son menu nécessite une permission spécifique.

Un thème est une composante modulaire dans le fonctionnement de Drupal. Il peut
être communautaire, prêt à l'emploi ou construit sur mesure ou à partir d'un thème
existant.

.. figure:: _static/theme_administration.png
  :align: center
  :width: 100%

Grandes notions générales
=========================

Dans une installation standard, Drupal utilise deux thèmes par défaut : Bartik
pour le front-office, et Seven, pour l'administration. D'autres thèmes sont
disponibles.

Deux aspects à considérer dans la configuration d'un thème :

:underline:`Des éléments de configuration générale`

  Ils sont propres à chaque thème et dépendent de ses fonctionnalités.

  .. figure:: _static/theme_settings.png
    :align: center
    :width: 100%

:underline:`Le layout du thème`

  C'est le découpage de votre page et il est propre à chaque thème. Un aperçu de
  ce layout est disponible dans une interface qui affiche le rendu de la structure
  des régions dans une page.

  Il y a autant de layouts différents qu'il n'y a de thèmes installés. Une illustration
  très concrète : le découpage des régions et la composition des blocs sont différentes
  entre le layout du front-office et le layout du back-office.

  .. figure:: _static/theme_block_layout.png
    :align: center
    :width: 100%

Les thèmes communautaires
=========================

Il existe beaucoup de `thèmes communautaires`_ utilisables, soit à part entière,
ou comme base pour être adapté / étendu. Comme pour les modules, l'installation
et l'activation d'un thème se fait avec Composer et Drush.

.. _thèmes communautaires: https://www.drupal.org/project/project_theme

.. code-block:: console

  # Root of the Drupal project
  composer require drupal/[theme_name]
  vendor/bin/drush then -y [theme_name]
