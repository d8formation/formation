.. |br| raw:: html

  <br />

.. role:: underline


**************************
Configuration des contenus
**************************


Nous avons abordé les principaux éléments qui permettent de créer la structure
des données d'un site. D'autres paramètres sont à prendre en compte pour finaliser
la configuration des contenus :

* La gestion des images ;
* Les alias url ;
* Le format des dates ;
* La configuration de l'éditeur visuel ;


Les styles  d'image
===================

Les styles d'image permettent de définir le format d'affichage différents pour les
images du site, fonction du contexte de l'affichage : pleine page, liste, bandeau...

Dans une installation standard, 3 styles d'image sont implémentés : Thumbnail,
Medium, Large.

.. figure:: _static/content_image_style_adm.png
  :align: center
  :width: 100%

Plusieurs effets peuvent être appliqués à l'image d'origine : mise à l’échelle,
recadrage, rotation... |br|
La prévisualisation est une bonne aide pour apprécier le rendu final de l'image.

:underline:`Remarque :` Mentionner la dimension de l'image dans le label, mais
attention au nom machine !

.. figure:: _static/content_image_style_define.png
  :align: center
  :width: 100%

Les styles d'image s'utilisent dans l'interface de configuration d'affichage d'une
entité possédant un champ image. Chaque mode d'affichage peut avoir son propre
style d'image : format vignette pour les listes, grand format pour les pages,
bandeau pour un carrousel...

.. figure:: _static/content_image_style_manage.png
  :align: center
  :width: 100%

.. admonition:: Pour aller plus loin
  :class: note

  Le couple de modules **Breakpoint** & **Responsive image** - modules du core -
  permettent de rendre les images dans `une balise picture html5`_ grâce à une
  interface dans laquelle on définit un ensemble de styles d'image, en lien avec
  les points de rupture du thème.

.. _une balise picture html5: https://codepen.io/zewebmaster/pen/mdPBRvg

Les alias url
=============

Le `module pathauto`_ est la solution pour automatiser la création des alias url
de vos contenus. Accessible en back-office, le module, le module nous permet de :

* Définir des patrons de chemin pour les alias url de vos contenus ;
* Générer / regénérer en masse des alias url ;

.. _module pathauto: https://www.drupal.org/project/pathauto

.. figure:: _static/content_pathauto_adm.png
  :align: center
  :width: 100%

Plusieurs schémas de patron peuvent être définis selon vos types de contenus, avec
différents niveaux de configuration :

* Par type d'entité ;
* Par langue ;
* Possibilité d'utiliser des tokens ;

.. figure:: _static/content_pathauto_define.png
  :align: center
  :width: 100%


Les formats de dates
====================

C'est un paramètre accessible dans le formulaire l'interface de configuration
d'affichage d'une entité possédant un champ date. Une dizaine de format
préconfiguré sont disponibles.

Les formats de date sont personnalisables, en utilisant `les paramètres de la fonction date php`_.

.. _les paramètres de la fonction date php: https://www.php.net/manual/fr/function.date.php

.. figure:: _static/content_date_adm.png
  :align: center
  :width: 100%

.. figure:: _static/content_date_define.png
  :align: center
  :width: 100%

Configuration de l'éditeur visuel
=================================

Les fonctionnalités accessibles dans l'éditeur WYSIWYG sont configurables :

* Les éléments de mise en page disponibles dans la barre d'outils ;
* Les restrictions d'usage par rôle ;

.. figure:: _static/content_wysiwyg_adm.png
  :align: center
  :width: 100%

.. figure:: _static/content_wysiwyg_configuration.png
  :align: center
  :width: 100%
