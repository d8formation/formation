.. role:: underline


************************
🚵‍ - Réglage de braquet
************************

⏱️ | 30 / 40 min.

!!!!

Nous allons pousser un plus loin la configuration de nos contenus, en ajustant :

:underline:`Les styles d'image :`

  Définir deux styles d'image et les affecter à deux modes d'affichage : full & teaser ;

:underline:`Les formats de date :`

  Définir un format de date : [jour], :underline:`le` [dd/mm/yy], :underline:`at` [hh] :underline:`H` [mm] - et l'affecter à un mode de vue ;

:underline:`Les alias url :`

  * Installer `le module Pathauto <https://www.drupal.org/project/pathauto>`_ ;
  * Définir des patrons d'alias - différents - pour les articles & les activités sportives ;
  * Générer en masse les alias url pour les contenus de votre site ;

:underline:`Les permissions :`

  Nous allons ajuster les permissions pour les différentes catégories d'utilisateur.

  **Pour les coach :**

    * Permissions de créer, modifier, supprimer le type de contenu activité -
      uniquement son propre contenu ;
    * Permissions d'accéder au menu du back-office ;

  **Pour les gestionnaires :**

    * Permissions de créer, modifier, supprimer n'importe quel type de contenus ;
    * Permissions de créer, modifier, supprimer les termes de taxonomie ;
    * Permissions d'accéder au menu du back-office ;
    * Permissions d'accéder au thème d'administration ;
