.. |br| raw:: html

  <br />

.. role:: underline


***************************
🏅 - Éléments de correction
***************************


Les styles d'images
===================

✅ Être attentif au label affiché en back-office : mentionner la taille ; |br|
✅ Être attentif au nom machine du style : qu'il reste simple ; |br|
✅ Être attentif à la configuration des modes d'affichage des entités ; |br|


Format de date
==============

Pas de difficultés particulières, si ce n'est la nécessité d'échapper les caractères
signifiants !


Installation du module Pathauto
===============================

.. parsed-literal::

  # Root of the Drupal project
  composer require drupal/pathauto
  vendor/bin/drush en -y pathauto


Permissions
===========

:underline:`Sur la gestion des types de contenus :`

.. figure:: ../../_static/TP_permissions_node.png
  :align: center
  :width: 100%


:underline:`Sur la gestion des taxonomies :`

.. figure:: ../../_static/TP_permissions_taxo.png
  :align: center
  :width: 100%

:underline:`Sur la gestion du thème d'administration :`

.. figure:: ../../_static/TP_permissions_system.png
  :align: center
  :width: 100%

:underline:`Sur la gestion de la toolbar d’administration :`

.. figure:: ../../_static/TP_permissions_toolbar.png
  :align: center
  :width: 100%
