.. |br| raw:: html

  <br />


****************
Quelques rappels
****************


Programmation orientée objet
============================

.. admonition:: Documentation
  :class: Note

  * `Les fondamentaux de la POO`_
  * `Les prérequis Drupal`_

.. _Les fondamentaux de la POO: https://www.php.net/manual/fr/language.oop5.php
.. _Les prérequis Drupal: https://www.drupal.org/docs/creating-custom-modules/getting-started-background-prerequisites

La POO se base sur les grandes notions suivantes :

* Classe et objet ;
* Encapsulation ;
* Héritage et polymorphisme ;
* Classe abstraite et interface ;

Classe vs objet
---------------

* Une classe, c'est un ensemble de variables (attributs) et de fonctions (méthodes) ;
* Le constructeur d'une classe est une méthode publique qui sert à hydrater les
  attributs et fournir à une instance de celle ci ;
* Un objet, c'est une instance de la classe, autrement dit un objet "concret" ;
* À partir de l'objet, on peut accéder aux attributs et aux méthodes publiques :
  getters, setters... ;

Encapsulation
-------------

Ce sont les règles qui vont déterminer la portée des attributs et des méthodes
d'une classe pour les autres classes du système. On compte trois niveaux d'accessibilité :

* ``public`` : accessible de partout, depuis l'intérieur ou l'extérieur de la classe ;
* ``private`` : accessible uniquement depuis la classe de référence ;
* ``protected`` : accessible uniquement depuis la classe de référence ou les classes
  qui en héritent.

Concept d’héritage et polymorphisme
-----------------------------------

* Une **classe fille** étend une **classe parente** avec le mot clé ``extends``.
  Elle hérite de tous les attributs et de toutes les méthodes ``public`` et ``protected``
  de la classe parente.
* Avec la notion de polymorphisme, une méthode peut être redéfinie dans une
  **classe fille** en remplacement de la méthode de sa **classe parente**.

Classe abstraite
----------------

Une **classe abstraite** se définit avec le mot clé ``abstract``.

Dans cette classe sont déclarées nécessairement - *mais pas exclusivement* - des
**méthodes abstraites**, c'est à dire de simple définition la signature de la
méthode, mais pas son implémentation.

Une classe abstraite ne peut pas être instanciée, mais d'autres classes peuvent
en hériter. Dans ce cas, toutes les méthodes marquées comme abstraites doivent
être redéclarées - avec la même visibilité, ou une visibilité moins restreinte
- par la **classe fille**.

Interface
---------

Une **interface** se définit avec le mot clé ``interface``.

Techniquement, c'est une classe entièrement abstraite dont toutes les méthodes
sont publiques et définies par leurs signatures. Son rôle est d'imposer un comportement
à une classe l'implémentant.

Une classe implémente une interface en utilisant le mot clé ``implements``.
Ce faisant, toutes les méthodes définies dans l'interface doivent être redéclarées.

.. admonition:: Ne pas confondre
  :class: hint

  Une classe abstraite représente une notion de sous-ensemble et de propriétés
  communes alors qu'une interface vise à imposer des comportements à une classe.


Namespace
=========

.. admonition:: Documentation
  :class: Note

  `Documentation Namespace`_

.. _Documentation Namespace: https://www.php.net/manual/fr/language.namespaces.php

Les **namespaces** fournissent un moyen de regrouper / d'encapsuler des classes,
interfaces, fonctions ou constantes. Ils sont conçus pour résoudre deux problèmes
que l'on peut rencontrer communément lors de la création d'applications :

* des conflits de nomenclature entre les classes ;
* la lisibilité du code avec la capacité de faire des alias ;


L'injection de dépendances
==========================

Ce patron de conception - *design pattern* - permet de découpler les liens de
dépendances entre les classes, les objets ... qui ne sont plus exprimés dans le
code de manière statique mais déterminés dynamiquement à l'exécution.

* `Documentation Symfony`_
* `Documentation Drupal`_

.. _Documentation Symfony: https://symfony.com/doc/3.4/service_container.html
.. _Documentation Drupal: https://www.drupal.org/node/2133171

L'injection de dépendances est beaucoup utilisée dans Drupal 8, en particulier
dans la gestion des services. Il repose sur deux composantes :

* Un conteneur, qui regroupe toutes les dépendances à injecter ;
* Des services, mappés sur des fichiers de configuration yaml, provenant du core
  ou d'un module (custom ou communautaire) ;
