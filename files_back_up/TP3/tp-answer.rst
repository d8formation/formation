.. |br| raw:: html

  <br />

.. role:: underline


***************************
🏅 - Éléments de correction
***************************


Menus
=====

:underline:`Menu sport :`

  * Créer, en back-office, un menu spécifique, contenant les liens vers les pages
    de nos termes de taxonomie sport.
  * Positionner le bloc dans le layout de votre thème ;

:underline:`Menu footer :`

  * Créer, en back-office, un menu spécifique, contenant les liens vers les pages
    de nos termes de taxonomie sport.
  * Positionner le bloc dans le layout de votre thème ;


Les blocs personnalisés
=======================

:underline:`Bloc contact :`

  Créer un nouveau type de bloc et ajouter les champs nécessaires :

  * Un champs adresse ;
  * Des champs contact : téléphone, mail ;
  * Un champs horaires d’accueil ;

  Instancier un bloc et positionner le dans le layout de bloc.

:underline:`Bloc Coach :`

  Pour afficher les informations d'une entité référente - ici ``User`` :

  1. Créer un champs ``reference`` du type de l'entité souhaitée  ; |br|
  2. Paramétrer correctement le formulaire de contribution : autocomplete par défaut ; |br|
  3. Configurer le mode de rendu du champs : utiliser un mode d'affichage ; |br|
  4. Pour l'entité référencée, créer, si besoin, un mode d'affichage spécifique ; |br|

  Il faudra ensuite instancier autant de blocs que nécessaire : au maximum, un bloc
  par sport concerné, et configurer correctement les paramètres d'affichage.


Le formulaire
=============

Pas de difficultés particulières dans la création du formulaire. C'est une entité,
on retrouve donc la logique de configuration déjà abordée :

✅ Être attentif à la nomenclature des champs utilisés ; |br|
✅ Être attentif à la configuration du formulaire de contribution ; |br|
✅ Être attentif à la configuration des modes d'affichage ; |br|


Rendu du formulaire
===================

* Pour afficher le formulaire sur une page, pas de configuration particulière :
  récupérer l'adresse de la page et faite un lien de un menu ;
* Pour rendre le formulaire dans un bloc, il faut installer un module complémentaire :
  le `module Contact storage`_. |br|
  Pour en savoir plus, je vous invite à consulter `cet article`_.

.. _module Contact storage: https://www.drupal.org/project/contact_storage
.. _cet article: https://www.flocondetoile.fr/blog/drupal-8-inject-contact-form-inside-content-type-5-steps


Sécurisation du formulaire
==========================

.. parsed-literal::

  # Root of the Drupal project
  composer require drupal/honeypot
  vendor/bin/drush en -y honeypot

  composer require drupal/captcha
  vendor/bin/drush en -y captcha
