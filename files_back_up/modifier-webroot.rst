.. |br| raw:: html

  <br />


******************************
Composer : modifier le webroot
******************************

Par défaut, les sources de l'application se trouvent dans le dossier web. |br|
Il est possible de changer cette configuration : conteneurisation de l'application, contrainte de serveur partagé ...

.. NOTE::
  Réaliser ce changement de configuration dès le début de votre projet sera plus facile.

Composer.json
=============

Dans la rubrique ``extra``
* Changer le web-root du ``drupal-scaffold``;
* Changer les chemins pour l’installeur ;

.. code-block:: json

  {
    "extra": {
      "drupal-scaffold": {
          "locations": {
              "web-root": "web/"
          }
      },
      "installer-paths": {
          "web/core": ["type:drupal-core"],
          "web/libraries/{$name}": ["type:drupal-library"],
          "web/modules/contrib/{$name}": ["type:drupal-module"],
          "web/profiles/contrib/{$name}": ["type:drupal-profile"],
          "web/themes/contrib/{$name}": ["type:drupal-theme"],
          "drush/Commands/contrib/{$name}": ["type:drupal-drush"],
          "web/modules/custom/{$name}": ["type:drupal-custom-module"],
          "web/themes/custom/{$name}": ["type:drupal-custom-theme"]
      }
    }
  }

Réinstaller le projet
=====================

.. NOTE::
  Si vous faites ce changement au cours de votre développement, penser à bien
  récupérer vos sources custom avant de supprimer le dossier sources

1. Supprimer le dossier web ;
2. Suppimer le composer.lock ;
3. Relancer la commande ``composer install`` ;
