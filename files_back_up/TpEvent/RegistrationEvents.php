<?php

namespace Drupal\sam\Event\TpEvent;

use Symfony\Component\EventDispatcher\Event;
use Drupal\Core\Entity\EntityInterface;

/**
 * Class UserRegistrationEvent.
 */
class RegistrationEvents extends Event {

  // All constant links to the RegistrationEventSubscriber class.
  const USER_CREATE = 'sam.user_create';
  const NEWSLETTER_SUBSCRIBE = 'sam.newsletter_subscribe';

  /**
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * @var string
   */
  protected $mail;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityInterface $entity = NULL, string $mail = '') {
    $this->entity = $entity;
    $this->mail = $mail;
  }

  /**
   * @return \Drupal\Core\Entity\EntityInterface
   */
  public function getEntity() {
    return $this->entity;
  }
  /**
   * @return \Drupal\Core\Entity\EntityInterface
   */
  public function getEmail() {
    return $this->mail;
  }

}
