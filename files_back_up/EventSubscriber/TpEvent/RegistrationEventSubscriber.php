<?php

namespace Drupal\sam\EventSubscriber\TpEvent;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\sam\Event\RegistrationEvents;
use Drupal\Core\Mail\MailManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * Class UserRegistrationEventSubscriber.
 */
class RegistrationEventSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * @var \Drupal\Core\StringTranslation\TranslationInterface
   */
  protected $stringTranslation;

  /**
   * {@inheritdoc}.
   */
  public function __construct(EntityTypeManagerInterface $entity_manager,
    MailManagerInterface $mail_manager,
    TranslationInterface $string_translation
  ) {
    $this->mailManager = $mail_manager;
    $this->entityTypeManager = $entity_manager;
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}.
   */
  public static function getSubscribedEvents() {
    $events[RegistrationEvents::USER_CREATE][] = ['sendMailToCoach'];
    // $events[RegistrationEvent::NEWSLETTER_SUBSCRIBE][] = ['sendConfirmationMail'];
    $events['sam.newsletter_subscribe'][] = ['sendConfirmationMail'];
    return $events;
  }

  /**
   * @param \Drupal\sam\Event\RegistrationEvents $event
   */
  public function sendMailToCoach(RegistrationEvents $event) {
    $account = $event->getEntity();

    if ($account->hasField('field_sport')) {
      $field_sport = $account->get('field_sport')->getValue();
      if (!empty($field_sport)) {
        $field_sport = reset($field_sport);
        $sport = $this->entityTypeManager->getStorage('taxonomy_term')->load($field_sport['target_id']);
        $field_coach = $sport->get('field_coach')->getValue();
        if (!empty($field_coach)) {
          $field_coach = reset($field_coach);
          $coach = $this->entityTypeManager->getStorage('user')->load($field_coach['target_id']);

          // prepare mail
          $module = 'sam';
          $key = 'user_registration';
          $to = $coach->getEmail();
          $reply = NULL;
          $send = TRUE;
          $langcode = $coach->getPreferredLangcode();
          $params['message'] = $this->t('Update your listing !');

          $this->mailManager->mail($module, $key, $to, $langcode, $params, $reply, $send);
        }
      }
    }
  }

  /**
   * @param \Drupal\sam\Event\RegistrationEvents $event
   */
  public function sendConfirmationMail(RegistrationEvents $event) {

    $to = $event->getEmail();
    // $langcode = $account->getPreferredLangcode();

    $params['message'] = $this->t('Your are now suscribe to our newsletter .... ');
    $this->mailManager->mail('sam', 'newsletter_subscription', $to, 'FR', $params, NULL, TRUE);
  }

}
