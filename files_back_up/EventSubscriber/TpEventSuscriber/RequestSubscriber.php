<?php

namespace Drupal\sam\EventSubscriber\TpEventSuscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Twig\Environment;

/**
 * Class RequestSubscriber.
 */
final class RequestSubscriber implements EventSubscriberInterface {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * @var \Twig\Environment
   */
  private $twig;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager,
    Environment $twig
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->twig = $twig;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = ['onController'];
    return $events;
  }

  public function onController(FilterResponseEvent $event) {
    $request = $event->getRequest();
    if ($request->attributes->get('_route') == 'entity.node.canonical') {
      $node = $request->attributes->get('node');
      if ($node->bundle() == 'article') {
        if ($node->hasField('field_sport')) {
          $field_sport = $node->get('field_sport')->getValue();
          if (!empty($field_sport)) {
            $field_sport = reset($field_sport);
            $sport = $this->entityTypeManager->getStorage('taxonomy_term')->load($field_sport['target_id']);
            $name = mb_strtolower($sport->getName());
            $this->twig->addGlobal('sport_content', $name);
          }
        }
      }
    }
  }
}
