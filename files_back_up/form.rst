.. |br| raw:: html

  <br />

.. role:: underline


***************
Les formulaires
***************


Le module du core ``Contact`` permet de créer des formulaires.

Conceptuellement, un formulaire est une entité préconfigurée, que l'on enrichit
et configure en back-office. :underline:`Particularité`, le formulaire de
contribution - habituellement paramétré pour les administrateurs - se pense ici
pour les usagers du site, qui seront les contributeurs de cette entité.

Drupal ne stocke pas en base de données les formulaires renseignées.


Une entité préconfiguré
=======================

Un formulaire est une entité. Elle est préconfigurée avec un certain nombres de
champs initialisé :

* Courriel de l'expéditeur ;
* Sujet et message ;
* Checkbox de mise en copie ;
* Option de prévisualisation ;

Petite subtilité de fonctionnement de cette entité :

* Ce sont les usagers qui vont contribuer sur ce type d'entité ;
* Le mode d'affichage - utile à la prévisualisation - a beaucoup moins de sens
  ici, puisque le formulaire n'a pas vocation à être publiée.


Gestion des formulaires
=======================

Une interface permet de gérer les formulaires. Outre les configurations classiques
des entités en back-office - les champs, l'ordonnancement de la saisie, l'affichage
- d'autres parmètres sont personnalisables :

* La liste des destinataires ;
* Le contenu du message flash affiché à la soumission du formulaire ;
* Un message de réponse automatique ;

.. figure:: _static/contact_form_adm.png
  :align: center
  :width: 100%

.. figure:: _static/contact_form_configuration.png
  :align: center
  :width: 100%

Des modules complémentaires
===========================

`Module Webform`_ :

* C'est un module bien documenté ;
* Il permet de construire des formulaires multistep ;
* Il offre plus de configuration pour l'affichage des formulaires ;

`Module Contact storage`_ :

* Ce module permet de stocker en base les messages reçus ;
* Il permet de rendre les formulaires dans un bloc ;

`Module Honeypot`_, `module Captcha`_ :

* Ce sont des modules bien documentés ;
* Honeypot n'est pas du tout invasif pour sécuriser les formulaires ;

.. _Module Webform: https://www.drupal.org/project/webform
.. _Module Contact storage: https://www.drupal.org/project/contact_storage
.. _Module Honeypot: https://www.drupal.org/project/honeypot
.. _module Captcha: https://www.drupal.org/project/captcha
