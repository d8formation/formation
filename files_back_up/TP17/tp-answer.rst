.. # define break line
.. |br| raw:: html

  <br />

**********************
Éléments de correction
**********************

Niveau 1
========

Gestion du formulaire d'inscription
-----------------------------------

On effectue les requêtes directement sur notre table custom. On utilise donc le
service ``\Drupal::database();``, en procédant à son injection lors dans la
construction du formulaire.

Deux requêtes à réaliser :

  * une requête de type select, pour vérifier la pré-existance de l'adresse mail
    dans notre base de données -> dans la fonction ``validateForm`` ;
  * un requête insert, pour sauvegarder les données -> dans la fonction ``submitForm`` ;

.. code-block:: php

  /**
   * @var \Drupal\Core\Database\Driver\mysql\Connection
   */
  protected $database;
  $container->get('database'),
  $this->database = $database;

  public function validateForm(array &$form, FormStateInterface $form_state) {
    $mail = $form_state->getValue('mail');
    $query = $this->database->select('newsletters_subscription', 'nl')
      ->condition('nl.email', $mail);
    $result = $query->countQuery()->execute()->fetchField();
    if ($result != 0) {
      $form_state->setErrorByName('mail', $this->t('This email is already registred'));
    }
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $fields = [
      'contact_name' => $values['name'],
      'email' => $values['mail'],
      'postal_code' => $values['postal_code'],
      'locality' => $values['locality'],
      'subscription' => 1,
      'last_update' => time(),
    ];
    $this->database->insert('newsletters_subscription')
      ->fields($fields)
      ->execute();
  }

Mise à jour du schéma de données
--------------------------------

Je vous renvoie à la documentation, elle est assez explicite.

Implémenter votre fonction dans le fichier sam.install de votre application.

.. code-block:: php

  function sam_update_8801() {
    $spec = [
      'description' => 'Sport tid',
      'type' => 'int',
      'unsigned' => TRUE,
      'not null' => TRUE,
      'default' => 0,
    ];
    $schema = Database::getConnection()->schema();
    $schema->addField('newsletters_subscription', 'sport', $spec);
  }

Pour jouer votre mise à jour, deux possibilités :

  * en back-office, à l'adresse ``/update.php`` ;
  * avec la commande drush, ``drush updb`` ;

.. note::
  L'information de la mise à jour d'un schema est stockée dans la table ``key_value``. |br|
  Vous pouvez retrouver cette information avec la commande Drush |br|
  ``drush php-eval "echo drupal_get_installed_schema_version('sam');"``

!!!!

Niveau 2 - Requête avec agrégation
==================================

La requête
----------

Pour obtenir ce resultat, il faut utiliser la ``QueryAggregateInterface`` qui
étends la ``QueryInterface``.

On utilise la fonction ``aggregate`` pour définir le champ et la fonction pour
réaliser l'agrégation :

  * le champ : le niveau de pratique du sport ;
  * la fonction : COUNT ;

.. code-block:: php

  $results = $this->entityTypeManager->getStorage('user')->getAggregateQuery()
    ->aggregate('field_niveau', 'COUNT')
    ->condition('field_sport', $pole,)
    ->groupBy('field_niveau')
    ->execute();

!!!!

Niveau 3 - Requête simple
=========================

Implémentation d'un hook_update()
---------------------------------

Il s'implémente dans le fichier ``sam.install``.

Deux traitements à réaliser :

  * La création de la table : son schéma est relativement simple. |br|
    Noter la déclaration d'une contrainte d'unicité sur le couple sport/compte.
  * Ajouter quelques entrées dans la table.


.. code-block:: php

  // Schéma de la table
  $spec = [
    'description' => '',
    'fields' => [
      'id' => [
        'type' => 'serial',
        'not null' => TRUE,
      ],
      'sport_id' => [
        'description' => 'Sport term id',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'account_id' => [
        'description' => 'Account id',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
    ],
    'unique keys' => [
      'sport_id__account_id' => ['sport_id', 'account_id'],
    ],
    'primary key' => ['id'],
  ];

  $schema = Database::getConnection()->schema();
  $schema->createTable('sport_interest', $spec);

  // Création des entrées
  $entries = [
    [
      'sport_id' => '10',
      'account_id' => '1',
    ],
  ];

  foreach ($entries as $entry) {
    Database::getConnection()->insert('sport_interest')->fields($entry)->execute();
  }


Adaptation du formulaire d’inscription
--------------------------------------

On adapte la fonction ``validateForm`` du formulaire pour interroger notre seconde
table et vérifier si l'utilisateur n'est pas déjà inscrit à la newsletter du sport
choisi.

Notez, que si le mail de l'utilisateur est déjà renseigné, il est possible de
passer son id à la fonction ``submitForm`` en utilisant la variable ``$form_state``.


.. code-block:: php

  $mail = $form_state->getValue('mail');
  $sport = $form_state->getValue('sport');
  $query = $this->database->select('newsletters_subscription', 'nl')
    ->fields('nl', ['id'])
    ->condition('nl.email', $mail);
  $id = $query->execute()->fetchField();
  if ($id != FALSE) {
    $form_state->set('newsletters_subscription_id', $id);
    $query = $this->database->select('sport_interest', 'si')
      ->condition('si.sport_id', $sport)
      ->condition('si.account_id', $id);
    $result = $query->countQuery()->execute()->fetchField();
    if ($result != 0) {
      $form_state->setErrorByName('mail', $this->t('This email is already registred for this sport'));
    }
  }

Dans la fonction ``submitForm``,

  * On enregistre le courriel de l'utilisateur, si celui n'est pas connu dans la
    base.
  * On enregistre le sport dans la table correspondante.

Notez que la fonction insert vous renvoie la valeur de l'auto-incrément de
l'enregistrement qui vient d'être fait - si un tel champs existe dans le design
de la table.

.. code-block:: php

  $id = $form_state->get('newsletters_subscription_id');

  if (is_null($id)) {
    $fields = [
      'contact_name' => $values['name'],
      'email' => $values['mail'],
      'postal_code' => $values['postal_code'],
      'locality' => $values['locality'],
      'subscription' => 1,
      'last_update' => time(),
    ];

    $id = $this->database->insert('newsletters_subscription')
      ->fields($fields)
      ->execute();
  }

  $fields = [
    'sport_id' => $values['sport'],
    'account_id' => $id,
  ];

  $this->database->insert('sport_interest')
    ->fields($fields)
    ->execute();


.. note::

  Pour créer de requêtes d’agrégation de type MAX, COUNT ... on utilise la fonction
  addExpression(). |br|
  `Liste de opérateurs utilisables <https://www.drupal.org/docs/8/api/database-api/functions-and-operators>`_
  |br|
  Ainsi, si vous souhaitez connaître le dernier enregistrement dans une table,
  vous utiliserez la requête suivante :

 .. code-block:: php

    $query = $this->database->select('newsletters_subscription', 'nl');
    $query->addExpression('MAX(id)');
    $id = $query->execute()->fetchField();


Création du Controller
----------------------
Il faut construire une requête en

.. code-block:: php

  $query = $this->database->select('newsletters_subscription', 'nl');
  $query->leftjoin('sport_interest', 'si', 'nl.id = si.account_id');
  $query->fields('nl', ['contact_name', 'email', 'last_update']);
  $query->condition('si.sport_id', $sport);
  $results = $query->execute()->fetchAll();


!!!!


.. note::
  Je vous renvoie au formulaire pour plus de détails sur l'implémentation.
