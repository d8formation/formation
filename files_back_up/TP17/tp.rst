.. # define break line
.. |br| raw:: html

  <br />


Données, c'est données... Reprendre, c'est pirater 🏴‍☠️!
==========================================================

Dans ce TP, nous allons travailler plusieurs aspects de la gestion de base de
données :

* Les requête sur les tables, en complétant le travail fait sur le formulaire
  d'inscription à la newsletters ;
* Les requêtes sur les entités, en faisant remonter des données sur le contenu
  du site : utilisateur, node ...


⏱ : 30 / 45 min.


Niveau 1
--------

  Reprendre le formulaire d'inscription à la newsletter pour :

    * Implémenter un mécanisme de validation, qui vérifie la pré-existance de
      l'adresse mail renseignée ;
    * Enregistrer le résultat du formulaire en base de données ;


Niveau 2
--------

  Afficher, dans un block ou un Controller, la répartition des utilisateurs
  en groupe de niveau pour un sport données.

  Question subsidiaire :

    Suivant le mode d'affichage choisi, imaginez une solution pour gérer le choix
    du sport de manière dynamique.


Niveau 3
--------

  Compléter la gestion de la newsletter, en créant une nouvelle table qui permette
  de prendre en compte le sport sur lequel se porte l'inscription.

  Reprendre le formulaire d'inscription pour prendre en compte cette nouvelle
  contrainte.

  Créer quelques entrées dans votre table, puis dans un Controller, construire
  une requête qui permette de connaître la liste des emails inscrites par sport.
