.. |br| raw:: html

  <br />


**********
Les events
**********


Quelques notions
================

Les event sont un concept issu de Symfony. Ce sont des actions reconnues par
le système et qui peuvent être interceptées.

Il fonctionne un peu près à la manière des hooks, à ceci près que :

* Ce sont des implémentations orientées objet ;
* Ils sont réutilisables;
* Ce sont des services ;

Deux aspects à considérer :

* vous pouvez déclarer vos propres événements, et les déclencher une ou plusieurs
  actions particulière au moyen l'EventDispatcher en utilisant son service dédié ;

* Vous pouvez souscrire à un événement particulier, déclenché par le core ou un
  module, pour altérer les variables qu'il contient, ou encore déclencher une
  action spécifique, au moyen d'EventSubscriber ;

Si vous voulez aller plus loin, je vous invite à consulter
`la documentation dédiée <https://api.drupal.org/api/drupal/core!core.api.php/group/events/8.8.x>`_.


Créer son Event et le déclencher
================================

Création de l'Event
-------------------

Une classe Event défini :

* Un ensemble de constantes qui définissent des cas d'usage et les Suscribers
  correspondant ;
* Les variables accessibles et des méthodes pour les manipuler ;

.. code-block:: php

  namespace Drupal\custom_module\Event;

  use Symfony\Component\EventDispatcher\Event;
  use Drupal\Core\Entity\EntityInterface;

  class CustomEvent extends Event {

    const CUSTOM_HANDLER = 'custom_module.custom_subscriber';

    protected $entity;

    public function __construct(EntityInterface $entity) {
      $this->entity = $entity;
    }

    public function getEntity() {
      return $this->entity;
    }
  }

Déclencher l'Event
------------------

Vous pouvez le faire dans n'importe quel script, (validation de formulaire, validation de panier,
modification de contenu...).

* Vous déclenchez l'Event et le cas d'usage correspondant ;
* Vous passez en paramètre les variables attendus par le constructeur de la classe ;

.. code-block:: php

  // Les classe à utiliser
  use Drupal\Core\Entity\EntityInterface;
  use Drupal\custom_module\Event\CustomEvent;

  /** @var Drupal\Core\Entity\EntityInterface  $entity **/
  \Drupal::service('event_dispatcher')->dispatch(CustomEvent::CUSTOM_HANDLER, new CustomEvent($entity));


Déclarer les subscribers
------------------------

Ce sont des classes qui portent les traitements à réaliser. Ils sont mappés,
dans l'Event par les constantes de la classe.

Côté implémentation, ils sont utilisables comme des services et sont déclarés dans
le fichiers de configuration du module.

Pour implémenter un Subscriber :

* Vous implémentez la classe qui contient les traitements à réaliser ;
* Vous la déclarez comme un service, dans vore fichiers de déclaration des services
  ``custom_module.services.yml``

.. code-block:: yaml

  # Déclaration du service
  custom_module.custom_subscriber:
    class: Drupal\custom_module\EventSubscriber\CustomEventSubscriber
    tags:
      - { name: 'event_subscriber' }


.. code-block:: php

  namespace Drupal\custom_module\EventSubscriber;

  use Drupal\custom_module\Event\CustomEvent;
  use Drupal\Core\Entity\EntityInterface;
  use Symfony\Component\EventDispatcher\EventSubscriberInterface;

  class CustomEventSubscriber implements EventSubscriberInterface {

    // Définition des événements auxquels le suscriber doit réagir
    public static function getSubscribedEvents() {
      $events[CustomEvent::CUSTOM_HANDLER][] = ['customCallback'];
      return $events;
    }

    // Traitement à réaliser
    public function customCallback(CustomEvent $event) {
      $entity = $event->getEntity();
      \Drupal::logger('custom_module.entity.update')->notice('Update @type: @title. Created by: @owner', [
        '@type' => $entity->getType(),
        '@title' => $entity->label(),
        '@owner' => $entity->getOwner()->getDisplayName()
      ]);
    }
  }


Réagir à un Event
=================

Le principe est à peu près identique, à la différence près que les Events auxquels
souscrire sont déjà définis, par le core de Drupal ou un module communautaire.

* `Event issu de Symfony <https://api.drupal.org/api/drupal/vendor!symfony!http-kernel!KernelEvents.php/class/KernelEvents/8.8.x>`_

* `Event issu du core <https://api.drupal.org/api/drupal/core%21core.api.php/group/events/8.8.x>`_

En cela, l’implémentation est plus simple puisqu'il s'agit uniquement de définir
un service et la classe susbscriber correspondante. C'est dans la méthode
``getSubscribedEvents()`` qui définira les Events auxquels il doit réagir.

**Un exemple de subscriber réagissant à l'émission d'une requête**


.. code-block:: yaml

  # Déclaration du service
  custom_module.request_subscriber:
    class: Drupal\custom_module\EventSubscriber\RequestSubscriber
    arguments: ['@state']
    tags:
      - { name: 'event_subscriber' }

.. NOTE::
  C'est un service. On peut lui passer en argument les autres services qui lui
  sont nécessaires.

.. code-block:: php

  namespace Drupal\custom_module\EventSubscriber;

  use Drupal\Core\Routing\TrustedRedirectResponse;
  use Drupal\Core\State\StateInterface;
  use Symfony\Component\EventDispatcher\EventSubscriberInterface;
  use Symfony\Component\HttpKernel\Event\GetResponseEvent;
  use Symfony\Component\HttpKernel\KernelEvents;

  class RequestSubscriber implements EventSubscriberInterface {

    /**
     * @var \Drupal\Core\State\StateInterface
     */
    protected $stateInterface;

    public function __construct(StateInterface $state_interface) {
      $this->stateInterface = $state_interface;
    }

    static function getSubscribedEvents() {
      $events[KernelEvents::REQUEST][] = ['checkForRedirection'];
      return $events;
    }

    function checkForRedirection(GetResponseEvent $event) {
      $enabled = $this->stateInterface->get('system.maintenance_mode');
      if ($enabled === 1) {
        $event->setResponse(new TrustedRedirectResponse('http://qwant.fr/'));
      }
    }
  }

.. NOTE::
  Une petite commande Drush pour mettre votre site en mode maintenance

  .. parsed-literal::

    drush sset system.maintenance_mode 1 --input-format=integer

Un autre exemple d'utilisation, pour altérer des routes existantes : |br|
* `https://www.drupal.org/node/2187643 <https://www.drupal.org/node/2187643>`_
