.. # define break line
.. |br| raw:: html

  <br />

.. role:: underline


*****************
La notion de hook
*****************


.. admonition:: Documentation
  :class: Note

  * `Comprendre les hooks`_
  * `Liste des hooks du core`_

.. _Comprendre les hooks: https://www.drupal.org/docs/8/creating-custom-modules/understanding-hooks
.. _Liste des hooks du core: https://api.drupal.org/api/drupal/core!core.api.php/group/hooks/

Le principe de hook est un concept historique de Drupal. C'est un mécanisme de
déclencheur qui permet d'insérer des traitements à réaliser dans un workflow, afin
d'altérer, de supplanter le comportement par défaut du système.


Déclarer un hook
================

1. Dans le fichier ``[module_name].module`` ;
2. En préfixant tous les hooks du nom machine du module : ``[module_name]_mail()``, ``[module_name]_theme()`` ;
3. Vider les caches après chaque implémentation d'un nouveau hook ;

.. code-block:: bash

  # Root of the Drupal project
  vendor/bin/drush cr


Un premier exemple de hook
==========================

Exemple de bonne pratique, le ``hook_help()`` permet de documenter le système.
C'est le module ``help`` qui déclenche ce hook. Il implémente un lien dans l'interface
d'aide ``/admin/help`` vers une page qui détaille le propos et les fonctionnalités
du module.

.. figure:: _static/module_help.png
    :align: center
    :width: 100%

:underline:`Implémentation du hook :`

.. code-block:: php

  use Drupal\Core\Routing\RouteMatchInterface;

  /**
   * Implements hook_help().
   */
  function training_help($route_name, RouteMatchInterface $route_match) {
    switch ($route_name) {
      case 'help.page.training':
        $output = '';
        $output .= '<h3>' . t('About') . '</h3>';
        $output .= '<p>' . t('This module is dedicated to learning the basics of development with drupal') . '</p>';
        return $output;
    }
  }

:underline:`Déclenchement du hook :`

  L'interface de documentation - ``/admin/help`` - est généré par un controller
  du module Help.

  La route ``help.main`` pointe vers le ``HelpController`` et déclenche la méthode
  ``helpMain()``.

  .. code-block:: yaml

    # help.routing.yml
    help.main:
      path: '/admin/help'
      defaults:
        _controller: '\Drupal\help\Controller\HelpController::helpMain'
        _title: 'Help'
      requirements:
        _permission: 'access administration pages'

  Cette interface est générée de manière dynamique, à partir des modules activés,
  implémentant un ``hook_help()``. Tout se joue dans la méthode ``listTopics()``,
  définie dans la classe ``HookHelpSection``.

  .. code-block:: php

    $links = $plugin->listTopics();
    if (is_array($links) && count($links)) {
      $this_output['#links'] = $links;
    }

  Le controller ne connait pas la configuration du système. Il récupère les
  informations en invoquant les hooks implémentés.

  `public function ModuleHandler::getImplementations`_

  .. code-block:: php

    public function listTopics() {
      $topics = [];
      foreach ($this->moduleHandler->getImplementations('help') as $module) {
        $title = $this->moduleHandler->getName($module);
        $topics[$title] = Link::createFromRoute($title, 'help.page', ['name' => $module]);
      }
      ksort($topics);
      return $topics;
    }

.. _public function ModuleHandler::getImplementations: https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Extension!ModuleHandler.php/function/ModuleHandler%3A%3AgetImplementations


Hook et famille de hook
=======================

Considérons deux typologies de hooks :

* des hooks uniques, comme le ``hook_help()``, ``hook_cron()``...,
* des hooks génériques qui se déclinent en fonction d'un contexte :
  ``hook_form_alter()``, ``hook_preprocess()``...

:underline:`Un exemple avec le hook_form_alter() :`

le ``hook_form_alter()`` - `api du module Form`_ - compte parmi les hooks les plus
fréquemment utilisés car il permet d'altérer n'importe quel formulaire de votre
système.

.. _api du module Form: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Form%21form.api.php/

Dans ce hook, trois variables sont accessibles :

* ``$form_id`` : l'id du formulaire,
* ``$form`` : le formulaire, défini par ces champs,
* ``$form_state`` : l'état du formulaire, à l'appel de la fonction.

.. code-block:: php

  use Drupal\Core\Form\FormStateInterface;

  /**
   * Implements hook_form_alter()
   */
  function training_form_alter(&$form, FormStateInterface $form_state, $form_id) {
    // Process
  }

Le ``hook_form_alter()`` se décline également en plusieurs hooks, chacun propre
à un contexte : |br|

+------------------------------+--------------------------------------------+
| Hook                         | Contexte                                   |
+==============================+============================================+
| hook_form_alter              | Global : Tous les formulaires              |
+------------------------------+--------------------------------------------+
| hook_form_BASE_FORM_ID_alter | Tous les formulaires de même BASE_FORM_ID  |
+------------------------------+--------------------------------------------+
| hook_form_FORM_ID_alter      | Pour un formulaire spécifique              |
+------------------------------+--------------------------------------------+

On retrouve ce principe de généricité dans d'autres familles de hook, entre autre,
parmi les plus utilisés :

* hook sur `le traitement des entités`_,
* hook sur `le traitement du thème`_.

.. _le traitement des entités: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Entity%21entity.api.php/9.x
.. _le traitement du thème: https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Render!theme.api.php/9.x
