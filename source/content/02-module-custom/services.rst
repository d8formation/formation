.. |br| raw:: html

  <br />


************
Les services
************


Nous avons précédemment comment utiliser les services mis à disposition par Drupal
ou un module communautaire.

Nous allons dans cette partie nous concentrer sur la création de nos propres
services. Cela se passe en deux étapes :

* la déclaration du services dans le fichiers de configuration ``[module_name].services.yml``
  de votre module,
* la déclaration de la classe portant les traitements dans le dossier ``/src/Service``.


Déclarer un service
===================

La déclaration du service prend en compte différents arguments :

* ``class`` : C'est le namespace de la classe de votre service,
* ``arguments`` (optionnel) : Il permet de définir les autres services dont dépend le
  votre, |br|
  *Dit autrement, il définit les paramètres que vous passez au constructeur de la
  classe de votre service*
* ``tags`` (optionnel) : Ils permettent d'indiquer à votre système que votre service
  appartient à une catégorie de services particuliers. |br|
  *Ex : les Event susbcriber*

.. code-block:: yaml

  services:
    training.training_service:
      class: Drupal\training\Service\TrainingService
      arguments: ['@current_user']
      tags:

.. NOTE::
  Pour en savoir plus sur les tags, vous pouvez vous référer également à la
  `documentation de Symfony`_. |br|

.. _documentation de Symfony: https://symfony.com/doc/current/service_container/tags.html


Créer la classe
===============

C'est un classe php classique, dont le constructeur prend en paramètre les services
que nous avons déclaré en argument dans le fichier de déclaration.

.. code-block:: php

  namespace Drupal\training\Service;

  use Drupal\Core\Session\AccountProxy ;

  class TrainingService {

    protected $currentUser;

    public function __construct(AccountProxy $current_user) {
      $this->currentUSer = $current_user;
    }

    public function getAccountName(){
      return $this->currentUSer->getAccountName() ;
    }
    public function do_foo(){
      return 'foo' ;
    }
    public function do_bar(){
      return 'bar' ;
    }
  }


Utiliser votre service
======================

C'est le même principe que nous avons déjà abordé ensemble, à savoir :

* dans un script à l'aide de la classe statique Drupal,
* dans une classe, grâce à l'injection de dépendances.

.. tabs::

  .. tab:: Dans un script

    .. code-block:: php

      use Drupal\training\Service\TrainingService;

      \Drupal::service('training.training_service')->method()

  .. tab:: Dans une classe

    .. code-block:: php

      use Drupal\Core\Controller\ControllerBase;
      use Drupal\training\Service\TrainingService;
      use Symfony\Component\DependencyInjection\ContainerInterface;

      class TrainingController extends ControllerBase {

        protected $trainingService;

        public static function create(ContainerInterface $container) {
          return new static(
            $container->get('training.training_service')
          ) ;
        }
        public function __construct(TrainingService $training_service) {
          $this->trainingService = $training_service;
        }
        public function render() {
          $foo = $this->trainingService->doFoo();
        }
      }


Je vous renvoie à la `partie précédente`_ consacrée à l'utilisation des services.

.. _partie précédente: injection_services.html
