.. |br| raw:: html

  <br />

.. role:: underline


*********************
Manipuler les entités
*********************


.. admonition:: Documentation
  :class: Note

  `Documentation détaillée sur les entités`_

.. _Documentation détaillée sur les entités: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Entity%21entity.api.php/group/entity_api

Toutes les données sont des entités. Elles se manipulent aux travers d'interfaces,
propres à chaque type d'entités prédéfinies, qui permettent d’accéder aux données
et aux configurations qui leurs sont spécifiques.

Structurellement, les entités natives possèdent leurs interfaces propres, qui
étendent, fonction de leurs spécificités, des interfaces génériques à toutes les
entités.

On y accède via l' ``entityTypeManager``, service qui permet de charger et de
rendre


Entités et routes
=================

Toutes les routes sont déclarées dans les annotations des classes qui définissent
les entités :

* `Node Class`_,
* `Term Class`_,
* `User Class`_.

.. _Node Class: https://api.drupal.org/api/drupal/core%21modules%21node%21src%21Entity%21Node.php/class/Node
.. _Term Class: https://api.drupal.org/api/drupal/core!modules!taxonomy!src!Entity!Term.php/class/Term
.. _User Class: https://api.drupal.org/api/drupal/core!modules!user!src!Entity!User.php/class/User

Toutes les routes ont une structure commune.

.. code-block:: console

  entity.[entity_type].canonical     // argument → ['entity_type' => $id] ;
  entity.[entity_type].edit-form     // argument → ['entity_type' => $id] ;
  entity.[entity_type].delete-form   // argument → ['entity_type' => $id] ;


Les principales interfaces
==========================

* `NodeInterface`_,
* `TermInterface`_,
* `BlockInterface`_,
* `UserInterface`_.

.. _NodeInterface: https://api.drupal.org/api/drupal/core%21modules%21node%21src%21NodeInterface.php/interface/NodeInterface
.. _TermInterface: https://api.drupal.org/api/drupal/core%21modules%21taxonomy%21src%21TermInterface.php/interface/TermInterface
.. _BlockInterface: https://api.drupal.org/api/drupal/core%21modules%21block%21src%21BlockInterface.php/interface/BlockInterface
.. _UserInterface: https://api.drupal.org/api/drupal/core%21modules%21user%21src%21UserInterface.php/interface/UserInterface

Ces interfaces implémentent des méthodes spécifiques propres à chaque type d'entité.
Elles étendent également des interfaces génériques à toutes les entités, pour accéder
aux données transverses :

:underline:`La base des entités :`

* `EntityInterface`_ pour accéder aux informations communes à toutes les entités :
  uuid & id, type d'entité, titre...,
* `FieldableEntityInterface`_ pour manipuler les champs d'une entité : contrôle,
  accès aux valeurs - set, get, validation...,
* `RefinableCacheableDependencyInterface`_ pour la gestion du cache.

.. _EntityInterface: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Entity%21EntityInterface.php/interface/EntityInterface
.. _FieldableEntityInterface: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Entity%21FieldableEntityInterface.php/interface/FieldableEntityInterface
.. _RefinableCacheableDependencyInterface: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Cache%21RefinableCacheableDependencyInterface.php/interface/RefinableCacheableDependencyInterface

:underline:`La gestion du multilangue :`

* `TranslatableInterface`_ pour gérer les traductions,
* `TranslatableRevisionableInterface`_ pour les métadonnées relatives aux traductions.

.. _TranslatableInterface: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21TypedData%21TranslatableInterface.php/interface/TranslatableInterface
.. _TranslatableRevisionableInterface : https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Entity%21TranslatableRevisionableInterface.php/interface/TranslatableRevisionableInterface

:underline:`La gestion des révisions :`

* `RevisionableInterface`_ pour manipuler le contenu des révisions,
* `RevisionLogInterface`_ pour les métadonnées des révisions.

.. _RevisionableInterface: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Entity%21RevisionableInterface.php/interface/RevisionableInterface
.. _RevisionLogInterface: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Entity%21RevisionLogInterface.php/interface/RevisionLogInterface

:underline:`La gestion de la publication :`

* `EntityPublishedInterface`_ pour manipuler les options de publication,
* `EntityChangedInterface`_ pour les métadonnées relatives aux options de publication.

.. _EntityPublishedInterface: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Entity%21EntityPublishedInterface.php/interface/EntityPublishedInterface
.. _EntityChangedInterface: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Entity%21EntityChangedInterface.php/interface/EntityChangedInterface

:underline:`La gestion de la configuration de l'entité :`

* `AccessibleInterface`_ pour gérer l'accessibilité à l'entité,
* `SynchronizableInterface`_ pour gérer la synchronisation de la configuration.

.. _AccessibleInterface: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Access%21AccessibleInterface.php/interface/AccessibleInterface
.. _SynchronizableInterface: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Entity%21SynchronizableInterface.php/interface/SynchronizableInterface

:underline:`Spécifique à la gestion des plugins :`

* `ConfigEntityInterface`_ pour accéder aux données de configurations,
* `ThirdPartySettingsInterface`_ pour la gestion des dépendances.

.. _ConfigEntityInterface: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Config%21Entity%21ConfigEntityInterface.php/interface/ConfigEntityInterface
.. _ThirdPartySettingsInterface: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Config%21Entity%21ThirdPartySettingsInterface.php/interface/ThirdPartySettingsInterface

:underline:`Spécifique aux utilisateurs :`

* `AccountInterface`_ pour gérer les informations de base d'un compte utilisateur :
  nom d'utilisateur, mail, informations de connexion...

.. _AccountInterface: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Session%21AccountInterface.php/interface/AccountInterface


L'entityTypeManager
===================

C'est le service - interface `EntityTypeManagerInterface`_ - qui permet d'interagir
avec les entités.

.. _EntityTypeManagerInterface: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Entity%21EntityTypeManagerInterface.php/interface/EntityTypeManagerInterface

:underline:`Accéder aux données d'une entité :`

  La méthode ``getStorage(string $entity_type_id)`` nous permet d’accéder à
  l' `EntityStorageInterface`_, interface à partir de laquelle nous allons pouvoir
  réaliser plusieurs actions, entre autre :

  * charger des entités : ``load()``, ``loadMultiple()``, ``loadByProperties()`` ... ;
  * gérer des entités : ``create()``, ``delete()``, ``save()`` ... ;
  * réaliser des requêtes : ``getQuery()``, ``getAggregateQuery()`` ;

.. _EntityStorageInterface: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Entity%21EntityStorageInterface.php/interface/EntityStorageInterface

:underline:`Rendre une entité :`

  La méthode ``getViewBuilder(string $entity_type_id)`` nous permet d’accéder à
  l' `EntityViewBuilderInterface`_, interface à partir de laquelle nous allons
  pouvoir :

  * rendre par programmation, une entité entière ou des champs spécifiques :
    ``view()``, ``viewField()``, ``viewFieldItem()`` ;
  * gérer les données de cache ; ``resetCache()``, ``getCacheTags()`` ;

.. _EntityViewBuilderInterface: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Entity%21EntityViewBuilderInterface.php/interface/EntityViewBuilderInterface


Gérer les champs d'une entité
==============================

Une fois l'entité chargée, on accède  à la valeur et aux propriétés des champs
au travers de la `FieldItemListInterface`_.

* ``getValue()``, ``getString()`` pour récupérer la/les valeur(s) du champ,
* ``first()``, ``filter()`` pour filtrer les valeurs du champ,
* ``appendItem()``, ``removeItem()`` pour ajouter / supprimer des valeurs au champ,
* ``isEmpty()`` pour vérifier si le champ contient une valeur.

.. _FieldItemListInterface: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Field%21FieldItemListInterface.php/interface/FieldItemListInterface

.. code-block:: php

  // Get field item with entityTypeManager.
  $field = $entity->get('field_name');

  // Manage value with FieldItemListInterface
  // cardinalité 1
  $value = $field->first()->getValue();
  // cardinalité 0..*
  $values = $field->getValue();


Accéder à l'entityTypeManager
=============================

:underline:`Dans un composant :`

  Par `injection de services`_,  à l'instanciation de la classe :

  .. code-block:: php

    protected $entityTypeManager;

    public static function create(ContainerInterface $container) {
      return new static(
        $container->get('entity_type.manager')
      ) ;
    }
    public function __construct(EntityTypeManagerInterface $entity_type_manager) {
      $this->entityTypeManager = $entity_type_manager;
    }

.. _injection de services: services-injection.html

:underline:`Dans un script :`

  Via `la classe statique Drupal`_, avec plusieurs méthodes implémnetées pour
  manipuler les entités.

  .. code-block:: php

    $em = \Drupal::entityTypeManager();
    $query = \Drupal::entityQuery('entity_type_name');
    $aggregate_query = \Drupal::entityQueryAggregate('entity_type_name');

.. _la classe statique Drupal: https://api.drupal.org/api/drupal/core!lib!Drupal.php/class/Drupal


Quelques exemples
=================

:underline:`Charger une entité et accéder à la valeur d'un champ :`

.. code-block:: php

  // $entity_type_id = 'node';
  // $entity_type_id = 'taxonomy_term';
  // $entity_type_id = 'user';
  $entity = \Drupal::entityTypeManager()
    ->getStorage($entity_type_id)
    ->load($id);

  // cardinalité 1
  if(!$entity->get('field_name')->isEmpty()) {
    $field_name = $entity->get('field_name')
      ->first()
      ->getValue();
    $name = $field_name['value'];
  }

  // cardinalité 0..*
  if(!$entity->get('field_colors')->isEmpty()) {
    $field_colors = $entity->get('field_colors')
      ->getValue();
    foreach($field_colors as $color) {
    }
  }

:underline:`Charger tous les entités d'un certain type :`

.. code-block:: php

  $entities = \Drupal::entityTypeManager()
    ->getStorage('node')
    ->loadByProperties(['type' => 'article']);
  foreach($entities as $$entity) {
  }

:underline:`Mettre à jour une entité :`

.. code-block:: php

  $entity = \Drupal::entityTypeManager()
    ->getStorage($entity_type_id)
    ->load($id);
  $entity->set('field_name', array $value);
  $entity->save();

:underline:`Faire une requête :`

.. code-block:: php

  $entity = \Drupal::entityTypeManager()
    ->getStorage($entity_type_id)
    ->getQuery()
    ->condition('field_name', $value, '=')
    ->execute();

:underline:`Rendre une entité par programmation :`

.. code-block:: php

  $view_builder = \Drupal::entityTypeManager()
    ->getViewBuilder($entity_type_id);

  $build = $view_builder->view($entity, 'view_mode_name');

  // $build is a render array.
  $rendered = \Drupal::service('renderer')
    ->render($build);


Aparté sur la gestion des données
=================================

La classe abstraite `ContentEntityBase`_, classe parente de la plupart des entités
- Node, Term, User, Comment - implémente la méthode magique ``__get()`` pour accéder
aux données des champs. Elle implémente également la `ContentEntityInterface`_ qui
implémente une méthode ``get($field_name)`` pour accéder aux données des champs.

.. _ContentEntityBase: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Entity%21ContentEntityBase.php/class/ContentEntityBase
.. _ContentEntityInterface: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Entity%21ContentEntityInterface.php/interface/ContentEntityInterface

.. code-block:: php

  // Using magic method.
  $field_item = $entity->field_name;         // FieldItemList object
  // Using interface.
  $field_item = $entity->get('field_name');  // FieldItemList object


De la même manière, le classe `FieldItemList`_, qui représente un champs d'entité,
implémente également la méthode magique ``__get()`` . Elle implémente également
la `FieldItemListInterface`_ qui implémente une méthode ``getValue()`` pour accéder
à la valeur du champ.

.. _FieldItemList: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Field%21FieldItemList.php/class/FieldItemList

.. code-block:: php

  // Using magic method.
  $value = $entity->field_name->value;                    // string $value
  // Using interface.
  $field_value = $entity->get('field_name')->getValue();  // array ['value' => $value]


Les deux approches sont correctes, néanmoins, utiliser les méthodes magiques
nécessitent de bien connaître les données manipulées, notamment la structure
des données du champs, la cardinalité... Utiliser les interfaces est plus verbeux,
mais elles fournissent plus de méthodes pour manipuler les données des champs des
entités.

:underline:`Un exemple concret :`

  Prenons un champ, de type "Référence", qui accepte une infinité de valeur -
  cardinalité ``0..*`` - ex : Dans les contenus de types "Article", champ
  ``field_tags``.

  * les méthodes magiques ne donne accès qu'à la première valeur du champs,
  * l'interface propose plusieurs méthodes pour récupérer une ou plusieurs valeurs,
    sous différentes formes.

  .. code-block:: php

    // Using magic method.
    // Only the first reference - string $referenced_target_id.
    $ref_target_id = $entity->field_ref_name->target_id;
    $ref_target_id = $entity->get('field_ref_name')->target_id;

    // Using interface.
    // Get the first reference.
    $ref_target_id = $entity->get('field_ref_name')->first()->getValue();  // return [['target_id' => $value]]
    // Get all references.
    $ref_target_ids = $entity->get('field_ref_name')->getValue();          // return [['target_id' => $value], ['target_id' => $value], ... ]
    $ref_entities = $entity->get('field_ref_name')->referencedEntities();  // return [$entity, $entity, ... ]
