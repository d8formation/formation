.. |br| raw:: html

  <br />

.. role:: underline


*********
Les blocs
*********


.. admonition:: Documentation
  :class: Note

  `La Block API`_.

.. _La Block API: https://www.drupal.org/docs/8/api/block-api

Les blocs font parties de la famille des plugins, c'est à dire un ensemble de
composants configurables et réutilisables.

:underline:`Quelques exemples de plugins :`

* les blocs, qui peuvent être instanciés dans différentes régions du site, avec
  des configurations différentes,
* des widgets que l'on peut utiliser pour la configuration de plusieurs entités,
* des formatters, pour modifier l'affichage par défaut d'un champs.

La block API permet de créer deux types de blocs :

* des blocs simples, affichant une simple information,
* des blocs configurables affichant des données renseignées dans
  `le formulaire de configuration`_.

..  _le formulaire de configuration: ../01-concepts-drupal/block-content.html#configuration-des-blocs


La structure des blocs
======================

Un bloc est une classe, structurée par l'interface
`BlockPluginInterface`_ avec un certain de nombre de méthodes à implémenter,
notamment :

* ``build()`` - pour construire le bloc,
* ``blockForm()`` - avec ``validateForm()`` et ``submitForm()`` - pour enrichir
  le formulaire de configuration.

.. _BlockPluginInterface: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Block%21BlockPluginInterface.php/interface/BlockPluginInterface/

La classe étend une classe de base - ``BlockBase`` - ``BlockPluginTrait``, qui fournit
l'implémentation de base du bloc - on y retrouvera toutes les méthodes des différentes
interfaces structurant le bloc.


Les annotations
===============

La déclaration des blocs se fait dans la classe même par le mécanisme d'annotations
- c'est un trait commun à tous les plugins.

Une fois implémenté, votre bloc est accessible et positionnable dans `le layout`_
en back-office.

.. _le layout: ../01-concepts-drupal/theme-management.html#grandes-notions-generales


Un premier block simple
=======================

.. code-block:: php

  namespace Drupal\training\Plugin\Block;
  use Drupal\Core\Block\BlockBase;

  /**
   * @Block(
   *  id = "training_block",
   *  admin_label = @Translation("Training block"),
   * )
   */
  class TrainingBlock extends BlockBase {

    public function build() {
      // Process
      return [
        '#markup' => $this->t('Hello block \o/'),
      ];
    }
  }

La fonction ``build()`` que vous appliquez votre logique métier pour afficher
vos données.

Comme pour les Controller, la fonction ``build()`` renvoie un `render array`_ avec
les mêmes possibilités de configuration.

.. _render array: controller.html#la-gestion-du-render


Injection de services
=====================

La classe de base vous met à disposition quelques fonctions courantes
mais si vous avez besoin d'un ou plusieurs services de Drupal,

.. code-block:: php

  // Accèder à l'utilisateur courant
  $this->currentUser();
  // Traduction
  $this->t();
  // Accès à la configuration du bloc
  $this->getConfiguration();

Pour étendre ces fonctionnalités, il faudra procéder à `une injection du/des dit services`_.

.. _une injection du/des dit services: services-injection.html#injection-de-service-dans-une-classe

.. code-block:: php

  use Drupal\Core\Block\BlockBase;
  use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
  use Drupal\Core\Mail\MailManagerInterface;
  use Symfony\Component\DependencyInjection\ContainerInterface;

  class TrainingBlock extends BlockBase implements ContainerFactoryPluginInterface {

    protected $mailManager;

    public static function create(ContainerInterface $container,
      array $configuration,
      $plugin_id,
      $plugin_definition
    ) {
        return new static(
          $configuration,
          $plugin_id,
          $plugin_definition,
          $container->get('plugin.manager.mail'),
      );
    }
    public function __construct(array $configuration,
      $plugin_id,
      $plugin_definition,
      MailManagerInterface $mail_manager
    ) {
        parent::__construct($configuration, $plugin_id, $plugin_definition);
        $this->mailManager = $mail_manager;
    }
  }


Précision sur la gestion des blocs
==================================

La déclaration d'un bloc et de son contenu peut s'opérer de deux manières :

* avec les entités blocs, en créant des types de blocs pour en instancier autant
  que nécessaire,
* avec un composant bloc, provenant d'un module personnalisé, dont on pourrait
  définir le contenu à l'aide d'un formulaire de configuration.

Le choix de la solution à adopter dépend bien évidemment du contexte de votre projet : |br|

* Quelles sont les données portées par le bloc ?
* Ses données vont elles évoluer ? Dans quelle mesure ?
* Combien de blocs devront être crées ?
* Qui gérera le site au quotidien ?

Pour vous aider dans ce choix, garder bien en tête quelques différences notables
entre les deux approches :

:underline:`UI vs programmation`

  Dans le premier cas, toutes les opérations se font en back-office.
  Vous définissez vos types de bloc, vous créez vos instances.

  Dans le deuxième cas, vous programmez vos blocs, vous leur définissez
  éventuellement un template.

:underline:`Encombrement du back-office`

  Dans le premier cas, vous avez accès à toutes les instances dans le formulaire
  de positionnement des blocs. Il peut vite devenir très chargé si il y a
  beaucoup de blocs à gérer.

  Dans le deuxième cas, vous n'avez qu'un seul type de bloc déclaré. Les instances
  sont créées à la validation du formulaire.

:underline:`Évolutivité`

  Dans le premier cas, le fait que tout se passe en back-office facilite, pour les
  gestionnaire du site, la possibilité de faire évoluer les blocs.

  Dans un deuxième cas,il faut rentrer obligatoirement dans le coeur du système.


Le formulaire de configuration
==============================

Vous implémentez les deux fonctions :

* ``blockForm`` - formulaire dans lequel vous définissez les champs de votre configuration ;
* ``blockSubmit`` - fonction dans laquelle vous enregistrez les données dans la configuration ;

Vous accédez ensuite à ce formulaire au moment du `positionnement du bloc dans le laytout`_.

..  _positionnement du bloc dans le laytout: ../01-concepts-drupal/block-content.html#configuration-des-blocs

`Un exemple`_ issu de la documentation Drupal.

.. _Un exemple: https://www.drupal.org/docs/8/api/block-api/block-api-overview#s-add-custom-configuration-options-to-your-block

.. code-block:: php

  namespace Drupal\training\Plugin\Block;

  use Drupal\Core\Block\BlockBase;
  use Drupal\Core\Block\BlockPluginInterface;
  use Drupal\Core\Form\FormStateInterface;

  /**
  * @Block(
  *  id = "training_block_with_config_form",
  *  admin_label = @Translation("Training block with configuration form"),
  * )
   */
  class TrainingBlockWithConfigurationForm extends BlockBase implements BlockPluginInterface {

    public function blockForm($form, FormStateInterface $form_state) {
      $form = parent::blockForm($form, $form_state);
      $config = $this->getConfiguration();
      $form['text'] = [
        '#type' => 'textfield',
        '#default_value' => isset($config['text']) ? $config['text'] : '',
      ];
      $form['number'] = [
        '#type' => 'textfield',
        '#default_value' => isset($config['number']) ? $config['number'] : '',
      ];
      return $form;
    }

    public function blockSubmit($form, FormStateInterface $form_state) {
      parent::blockSubmit($form, $form_state);
      $values = $form_state->getValues();
      $this->configuration['text'] = $values['text'];
      $this->configuration['number'] = $values['number'];
    }

    public function build() {
      $config = $this->getConfiguration();
      $text = !empty($config['text']) ? $config['text'] : '' ;
      $number = !empty($config['number']) ? $config['number'] : '' ;
      return [
        '#markup' => 'Texte : ' . $text . '</br>Number : ' . $number,
      ];
    }
  }
