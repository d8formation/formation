.. |br| raw:: html

  <br />

.. role:: underline


***********************
Requêtes sur les tables
***********************


Drupal possède sa propre API pour effectuer des requêtes en base de données.
Cette couche d'abstraction est construite sur l'extension PHP PDO et hérite en
grande partie de sa syntaxe et de sa sémantique.


Requête sur les tables
======================

Pour effectuer des requêtes directement sur les tables, nous allons utiliser le
service ``database``. Nous verrons dans cette partie les grandes étapes à respecter,
à savoir,

* initialiser la connexion,
* construire sa requête,
* consulter les résultats.

Si vous souhaitez aller plus loin dans l'appréhension de ces concepts, vous pouvez
consulter `la Database API`_.

.. _la Database API: https://www.drupal.org/docs/8/api/database-api


Initialiser la connexion
========================

C'est un service ... Donc deux manières de procéder, fonction que l'on travaille
dans une classe ou dans un script.

.. tabs::

  .. tab:: Dans une classe

    **Ex. Controller, formulaire, block ...** |br|
    Toujours favoriser l'injection de dépendances.

    .. code-block:: php

      /**
       * @var \Drupal\Core\Database\Driver\mysql\Connection
       */
      protected $database;

      public function __construct(Connection $database) {
        $this->database = $database;
      }
      public static function create(ContainerInterface $container) {
        return new static(
          $container->get('database')
        );
      }
      public function render(){
        $query = $this->database->select('[table]', '[alias]');
      }

  .. tab:: Dans un script

    **Ex. hook, preprocess...** |br|

    .. code-block:: php

      $connection = \Drupal::database();
      $connection = \Drupal::service('database'); // équivalent
      $query = $connection->select('[table]', '[alias]');


Construire sa requête
=====================

Vous pouvez construire toutes les requêtes usuels, à savoir,

* `select`_,
* `update`_,
* `insert`_,
* `delete`_.

.. _select: https://www.drupal.org/docs/8/api/database-api/dynamic-queries/introduction-to-dynamic-queries
.. _update: https://www.drupal.org/docs/8/api/database-api/update-queries
.. _insert: https://www.drupal.org/docs/8/api/database-api/insert-queries
.. _delete: https://www.drupal.org/docs/8/api/database-api/delete-queries

Globalement, on retrouve toutes les possibilités que peut nous procurer le langage SQL,
avec ces quelques méthodes :

  * `sélection d'attributs`_ : ``fields()`` ;
  * `condition`_ sur la valeur d'un attribut : ``condition()`` ;
  * `calcul sur les attributs`_ : ``addExpression()`` ;
  * des jointures`_ :  ``join(),`` ``innerJoin(),`` ``leftJoin(),`` or ``rightJoin()`` ;
  * des fonctions de `filtre`_ et de `dénombrement`_ : ``count(),`` ``distinct()`` ;
  * `regrouper les résultats`_ pour les agrégations : ``having()``, ``groupBy()`` ;

.. _sélection d'attributs: https://www.drupal.org/docs/8/api/database-api/dynamic-queries/fields
.. _condition: https://www.drupal.org/docs/8/api/database-api/dynamic-queries/conditions
.. _calcul sur les attributs: https://www.drupal.org/docs/8/api/database-api/dynamic-queries/expressions
.. _des jointures: https://www.drupal.org/docs/8/api/database-api/dynamic-queries/joins
.. _filtre: https://www.drupal.org/docs/8/api/database-api/dynamic-queries/distinct
.. _dénombrement: https://www.drupal.org/docs/8/api/database-api/dynamic-queries/count-queries
.. _regrouper les résultats: https://www.drupal.org/docs/8/api/database-api/dynamic-queries/grouping

.. code-block:: php

  $query->fields('table_alias', ['field_name', 'field_name']);
  $query->condition($field, $value, $operator);
  $query->addExpression('COUNT(field_name)', 'count_result_name');
  $query->join('table_name', 't', 't.nid = n.nid');
  $query->countQuery()->execute()->fetchField();
  $query->distinct()->execute()->fetchAll();
  $query->groupBy('n.uid');

Chaque type de requête ayant sa propre syntaxe, je vous invite à consulter plus
en détails la documentation pour chacune d'entre elle.

:underline:`Un exemple de select :`

  .. code-block:: php

    $connection = \Drupal::database();
    $query = $this->database->select('users', 'u')
      ->fields('u', ['uid', 'uuid'])
      ->condition('u.uid' , 1);
    $result = $query->execute();


Consulter les résultats
=======================

Deux manières de traiter le résultat de cette requête :

  :underline:`En parcourant de manière itérative l'ensemble des résultats :`

  .. code-block:: php

    foreach ($result as $record) {
      dump($record);
    }

  :underline:`En agrégeant les résultats :`

  .. code-block:: php

    $result->fetchField()         // la valeur du premier field, du premier resultat ;
    $result->fetchAll()           // Un tableau contenant tous les résultats, avec tous les fields retournés ;
    $result->fetchCol()           // Un tableau contenant tous les résultats, mais seulement la valeur du premier field ;
    $result->fetchAssoc()         // Un tableau associatif du premier résultat ;
    $result->fetchAllAssoc($key)  // Un tableau associatif de tous les résultats, mappé sur la clé passé en paramètre ;
    $result->fetchAllKeyed()      // Les résultats sous forme de tableau indexé par la 1ere colonne avec pour valeur la 2e ;

  `Consultez la documentation`_ pour approfondir vos connaissance sur la représentation
  des résultats.

.. _Consultez la documentation: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Database%21Statement.php/class/Statement/
