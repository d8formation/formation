.. |br| raw:: html

  <br />

.. role:: underline


**********
Le routing
**********


.. admonition:: Documentation
  :class: Note

  `Documentation Drupal sur le routing`_

.. _Documentation Drupal sur le routing : https://www.drupal.org/docs/8/api/routing-system

Le système de routing, au sens large, c'est le mécanisme qui va faire le lien
entre une url et une action à réaliser. |br|

C'est dans un fichier de configuration ``[module_name].routing.yml`` que sont
déclarées toutes les routes. La définition d'une route comprend plusieurs
paramètres :

* le composant devant réaliser le traitement : un controller, un formulaire,
* les droits d'accès,
* le typage des paramètres,
* des actions callback pour la gestion du titre, le contrôle d'accès.


Quelques notions de bases
=========================

:underline:`Une route :`

  C'est un patron de chemin - prenant ou non des arguments - vers un composant
  devant exécuter une action.

:underline:`Un path :`

  C'est un chemin défini d'une route. Il conduit à la réalisation d'une action :
  page, formulaire, redirection ...

:underline:`Un lien de menu :`

  C'est une configuration de votre système. Il peut se déclarer en back-office ou
  dans :ref:`les fichiers de configuration des menus` du module.


Les propriétés d'une route
==========================

:underline:`Un exemple :`

  Une route vers un controller, qui prend deux paramètres de type entier dont un
  chemin pourrait être ``/page/1/2``, accessible à tous les visiteurs, connectés
  ou non.

  .. code-block:: yaml

    # [module_name].routing.yml
    [module_name].route_name:
      path: '/page/{foo}/{bar}'                                                     # route pattern
      defaults:
        _controller: \Drupal\[module_name]\Controller\[ControllerName]::[method]    # callback controller
      requirements:
        _permission: access content                                                 # access control
        foo: \d+                                                                    # argument control regex
        bar: \[a-z]+                                                                # argument control regex

Ce sont les paramètres les plus communs d'une route. Pour aller plus loin dans la
configuration des paramètres d'une route, je vous renvoie vers la documentation :

* `Tous les paramètres possibles`_
* `Gestion des paramètres dans une route`_

.. _Tous les paramètres possibles: https://www.drupal.org/node/2092643
.. _Gestion des paramètres dans une route: https://www.drupal.org/docs/8/api/routing-system/parameters-in-routes/using-parameters-in-routes


La gestion des arguments
========================

Les méthodes d'un controller peuvent prendre, en argument, les paramètres des routes
qui leurs sont associées.

* Les paramètres de la route et les arguments du controller doivent être déclarés
  dans le même ordre, avec la même nomenclature.
* Les paramètres tel que ``{node}`` ou ``{user}`` peuvent être chargés dans le
  controller en les typant des bonnes classes.

.. code-block:: bash

  path: '/page/{foo}'              # Route configuration
  public function render($foo) {}  # Controller configuration


  path: '/page/{node}'                                         # Route configuration
  options:
    parameters:
      node:
        type: entity:node
  public function render(\Drupal\node\NodeInterface $node) {}  # Controller configuration


  path: '/page/{user}'                                                    # Route configuration
  options:
    parameters:
      user:
        type: entity:user
  public function render(\Drupal\Core\Account\AccountInterface $user) {}   # Controller configuration


Des routes dynamiques
=====================

Certains aspects de la route - titre de la page, droit d’accès - peuvent être gérés
de manière dynamique, au moyen d'une fonction callback, - dans un controller -
déclarés dans la configuration de la route.

:underline:`Un exemple` : Gérer dynamiquement l'accès à une page ou un formulaire.

.. code-block:: yaml

  requirements:
    _custom_access: '\Drupal\[module_name]\Controller\[ControllerName]::[method]'


.. _les fichiers de configuration des menus:

les fichiers de configuration des menus
=======================================

Il existe plusieurs fichiers de configuration, fonction du niveau de menu du lien
que vous déclarez :

+--------------------------------+----------------------------------+
| File                           | Configuration                    |
+================================+==================================+
| [module_name].links.menu.yml   | Lien de menu dans l'arborescence |
+--------------------------------+----------------------------------+
| [module_name].links.task.yml   | Onglet                           |
+--------------------------------+----------------------------------+
| [module_name].links.action.yml | "Action" - back-office           |
+--------------------------------+----------------------------------+

Pour vous familiariser avec ces configurations, je vous renvoie à `l'API Menu`_.

.. _l'API Menu: https://www.drupal.org/docs/drupal-apis/menu-api

:underline:`Un exemple simple de fichier de configuration de menu :`

.. code-block:: yaml

  # [module_name].links.menu.yml
  [module_name].route_name_link:
    menu_name: main                       # menu machine name
    title: 'Simple page'                  # page title
    route_name: [module_name].route_name  # route
    weight: 1                             # menu weight
