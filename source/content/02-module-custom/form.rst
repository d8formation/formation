.. |br| raw:: html

  <br />

.. role:: underline


***************
Les formulaires
***************


.. admonition:: Documentation
  :class: Note

  `La Form API`_.

.. _La Form API: https://www.drupal.org/docs/8/api/form-api


Ce qu'il faut retenir
=====================

Un formulaire est une classe - assez similaire à un controller - qui étend une
classe de base, `FormBase`_ - pour accéder aux méthodes utilitaires courantes -
et implémente une interface, `FormInterface`_ - pour le comportement de la classe.

D'autres classes de bases peuvent être utilisées pour conditionner l'objet du
formulaire :

* `ConfigFormBase`_ pour un formulaire traitant une configuration du système -
  dans le back-office,
* `ConfirmFormbase`_ pour une formulaire de confirmation - avant la réalisation
  d'une action.

.. _FormBase: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Form%21FormBase.php/class/FormBase/
.. _FormInterface: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Form%21FormInterface.php/interface/FormInterface/
.. _ConfigFormBase: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Form%21ConfigFormBase.php/class/ConfigFormBase/
.. _ConfirmFormbase: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Form%21ConfirmFormBase.php/class/ConfirmFormBase/

Quatre méthodes structure le comportement d'un formulaire :

+----------------+----------------------------------+
| Fonction       | Traitement                       |
+================+==================================+
| getFormId()    | Retourne l'id du formulaire      |
+----------------+----------------------------------+
| buildForm()    | Construit le formulaire          |
+----------------+----------------------------------+
| validateForm() | Valide les données du formulaire |
+----------------+----------------------------------+
| submitForm()   | Traite les données du formulaire |
+----------------+----------------------------------+

Ces méthodes prennent - à minima - deux arguments :

* ``$form`` - le formulaire en tant que tel,
* ``$form_state`` - `l'interface FormStateInterface`_ pour accéder aux règles de
  gestion et aux données du formulaire,
* Et des arguments supplémentaires si nécessaire : paramètre d'une route...

À la manière des render array, un formulaire est rendu dans un tableau,
chaque `input`_ étant un élément de ce tableau, avec ces propres clés de configuration.

.. _l'interface FormStateInterface: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Form%21FormStateInterface.php/interface/FormStateInterface/
.. _input: https://api.drupal.org/api/drupal/elements/


Implémenter un formulaire
=========================

1. Créer une classe **[FormName]Form.php** dans le dossier ``/src/Form/``.
2. Étendre la classe ``FormBase``.
3. implémenter à minima les trois méthodes ``getFormId()``, ``buildForm()``, ``submitForm()``.
4. Déclarer une route dans votre fichier de configuration ``[module_name].routing.yml``.

C'est dans la méthode ``buildForm()`` que vous déclarez tous les champs de votre
formulaire.

.. code-block:: php

  namespace Drupal\training\Form;

  use Drupal\Core\Form\FormBase;
  use Drupal\Core\Form\FormStateInterface;

  class TrainingForm extends FormBase {

    public function getFormId() {
     return 'training_form';
    }
    public function buildForm(array $form, FormStateInterface $form_state) {
      $form = [];
      return $form;
    }
    public function validateForm(array &$form, FormStateInterface $form_state) {
    }
    public function submitForm(array &$form, FormStateInterface $form_state) {
    }
  }

.. code-block:: yaml

  training.form:
    path: '/form/simple'
    defaults:
      _form: '\Drupal\training\Form\TrainingForm'
      _title: 'A simple form'
    requirements:
      _permission: 'access content'


Les champs du formulaire
========================

Les champs se présentent sous forme de tableaux associatifs - render array - dont
les clés définissent les propriétés de l'input. Consultez la documentation pour
appréhender les différents éléments de formulaire utilisables et les clés de
configuration utilisables.

* `Les champs disponibles`_
* `Les clés de configuration - render array`_

.. _Les champs disponibles: https://api.drupal.org/api/drupal/elements/
.. _Les clés de configuration - render array: https://api.drupal.org/api/drupal/developer%21topics%21forms_api_reference.html/7

.. code-block:: php

  $form['input_text'] = [
    '#type' => 'textfield',
    '#title' => $this->t('My object'),
    '#description' => $this->t('The description of the input'),
    '#size' => 60,
    '#maxlength' => 128,
  ];

  $form['checkbox'] = [
    '#type' => 'checkbox',
    '#title' => $this->t('You can check this'),
    '#description' => $this->t('The description of the checbox\'s purpose'),
  ];


Valider un formulaire
=====================

Deux niveaux de validation des données du formulaire :

* une validation automatique due au typage et à la configuration des champs : email,
  date, varchar(5)...,
* une validation logique sur la cohérence des données.

Il faut implémenter la méthode ``validateForm()`` pour réaliser le vérification
de la cohérence des données et interrompre le traitement du formulaire. Dans cette
fonction, la variable ``$form_state`` porte l'état du  formulaire, ce qui permet
de :

* récupérer la valeur des champs grâce aux fonctions
  ``$form_state->getValues()`` et ``$form_state->getValue('[input_name]')``,
* réaliser un contrôle sur les données,
* retourner une erreur sur les champs non conforme grâce la fonction
  ``$form_state->setErrorByName('[input_name]', 'message')``.

.. code-block:: php

  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['date_start'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Start date'),
    ];
    $form['date_end'] = [
      '#type' => 'datetime',
      '#title' => $this->t('End date'),
    ];
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {

    $start = $form_state->getValue('date_start');
    $end = $form_state->getValue('date_end');
    if($end < $start) {
      $form_state->setErrorByName('date_end', $this->t('The end date is not valid'));
    }
  }


Traiter les données
===================

Le traitement des données s'opèrent dans la fonction ``submitForm()`` :

* récupération des données du formulaire grâce aux fonctions ``$form_state->getValues()``
  et ``$form_state->getValue('[input_name]')``,
* traitement des données : MAJ de contenus, insertion en base de données, envois
  de mails...

.. code-block:: php

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->logger('Channel')->notice($values['input_text']);
    $form_state->setRedirect('<front>');
  }


La classe FormBase
==================

`Cette classe`_ de base donne accès à un certain nombre de méthodes utilitaires
courantes et au conteneur d'injection de service.

.. _Cette classe: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Form%21FormBase.php/class/FormBase/

.. code-block:: php

  // Current user entity
  $current_user = $this->currentUser();

  // Request manangement
  $this->getRequest();

  // Manage redirection
  // buildForm() -> return
  return $this->redirect('route_name');
  // validateForm() submitForm() -> no return, set redirection
  $form_state->setRedirect('route_name');

  // Set message flash
  $this->messenger()->addStatus($this->t('Your form have been submitted'));

  // Translation
  $this->t('String to translate')

  // Logger
  $this->logger('My channel')->notice($this->t('My message'));


Afficher le formulaire
======================

:underline:`Associé à une route, rendu dans une page :`

  Nous venons de le voir de l'exemple précédent. Ceci est applicable à tous les
  formulaires destinés à être renseignés par les visiteurs... Mais c'est le cas
  également des formulaires de configuration dont les routes correspondent à des
  entrées de menu du back-office.

:underline:`Rendu dans un composant :`

  Grâce au service ``form_builder`` - FormBuilderInterface - accessible dans les
  classes de base ``ControllerBase`` et ``BlockBase``.

  .. code-block:: php

    // Manage custom form.
    $this->formBuilder();
    // Return a form.
    $form = $this->formBuilder()->getForm('Drupal\[module_name]\Form\[form_name]');
    return $form;


Pour aller plus loin
====================

Les autres types de formulaire
------------------------------

* `Formulaire de configuration`_
* `Formulaire de confirmation`_

.. _Formulaire de configuration: https://www.drupal.org/docs/8/api/form-api/configformbase-with-simple-configuration-api
.. _Formulaire de confirmation: https://www.drupal.org/docs/8/api/form-api/confirmformbase-to-confirm-an-action

State API
---------

C'est un paramètre de configuration d'un champs de formulaire, qui permet de
conditionner son affichage en fonction de l'état d'autres champs.

* `Documentation drupal`_
* `Un article complémentaire`_

.. _Documentation drupal: https://www.drupal.org/docs/8/api/form-api/conditional-form-fields
.. _Un article complémentaire: https://www.lullabot.com/articles/form-api-states

Formulaire Multistep
--------------------

* `Un article intéressant`_

.. _Un article intéressant: https://www.sitepoint.com/how-to-build-multi-step-forms-in-drupal-8
