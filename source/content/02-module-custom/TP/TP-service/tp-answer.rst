.. role:: underline


***************************
🏅 - Éléments de correction
***************************


Niveau 1 - Exporter les inscrits aux activités sportives
========================================================

:underline:`Pour le routing :`

  La route avec un chemin commençant par ``/admin`` permet de bénéficier du thème
  d'administration. Vous définissez **un chemin**, mais ce n'est pas un lien dans
  le menu.

  Pour créer le lien, il faudra ajouter une configuration - ``sam.export_registred_form``
  - dans le fichier ``sam.links.menu.yml``, en déclarant une route parente
  ``system.admin_config_system``.

  Pour créer l'onglet, il faudra le déclarer dans le fichier ``sam.links.task.yml``,
  en déclarant comme route de base, le nom machine de la route de notre controller.
  - ``sam.export_registred_form``

  .. code-block:: yaml

    # sam.routing.yml
    sam.export_registred_form:
      path: '/admin/config/system/exports'
      defaults:
        _form: '\Drupal\sam\Form\TpService\ExportRegistredSelectionForm'
        _title: 'Export interfaces'
      requirements:
        _role: 'administrator'

    # sam.links.menu.yml
    sam.export_registred_form_link:
      title: 'Export registred users'
      description: 'Get a csv file form activity'
      route_name: sam.export_registred_form
      parent: system.admin_config_system

    # sam.links.task.yml
    sam.export_registred_form_task:
      title: 'Export registred user'
      description: 'Get a csv file form activity'
      route_name: sam.export_registred_form
      base_route: sam.export_registred_form

:underline:`Pour le formulaire :`

  Injection de notre classe de service d'export :

  .. code-block:: php

    /** @var \Drupal\sam\Service\TpService\ExportService **/
    protected $exportService;
    $container->get('sam.export_service'),
    $this->exportService = $export_service;

  Implémentation de champ ``autocomplete`` :

  .. code-block:: php

    $form['activity'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Center of interest'),
      '#required' => TRUE,
      '#target_type' => 'node',
      '#selection_settings' => [
        'target_bundles' => [
          'activity',
        ],
      ],
    ];

  La fonction ``submitForm()`` utilise notre service pour exporter les données de l'activité
  et retourner un objet de type ``Response``.

  .. code-block:: php

    public function submitForm(array &$form, FormStateInterface $form_state) {
      $tid = $form_state->getValue('activity');
      $response = $this->exportService->exportRegistredUsers($tid);
      $form_state->setResponse($response);
    }

:underline:`Pour le service :`

  **Déclaration du service**

    Dans le fichier ``sam.services.yml``. On déclare ici la dépendance de notre
    service au gestionnaire d'entités ``@entity_type.manager``.

    .. code-block:: yaml

      # sam.services.yml
      sam.export_service:
        class: Drupal\sam\Service\TpService\ExportService
        arguments: ['@entity_type.manager']
        tags:

  **Déclaration du service**

    Le constructeur de la classe, reprend le service injecté dans notre fichier
    de configuration.

    .. code-block:: php

      /** @var \Drupal\Core\Entity\EntityTypeManagerInterface **/
      protected $$entityTypeManager;
      public function __construct(EntityTypeManagerInterface $entity_type_manager) {
        $this->entityTypeManager = $entity_type_manager;
      }

:underline:`Quelques point de vigilance pour notre classe de service :`

  * Une méthode pour récupérer les inscrits à partir d'une activité choisie par
    l'utilisateur - utilisation de la ``QueryInterface``.
  * Une méthode pour construire le fichier csv à partir des données des inscrits ;
  * Notre service doit nous retourner un objet ``Response``.

.. code-block:: php

  // Get list of registered  uid for an activity.
  $uids = $this->entityTypeManager->getStorage('user')
    ->getQuery()
    ->condition('field_activities', $tid)
    ->execute();

  // Load registred users and create csv row.
  $users = $this->entityTypeManager->getStorage('user')
    ->loadMultiple($uids);
  foreach ($users as $user) {
    $csv = [
      $user->getAccountName(),
      $user->getEmail(),
      date('d/m/Y', $user->getCreatedTime()),
    ];
    fputcsv($handle, $csv);
    }

    // Create the Response object.
    $response = new Response();
    $response->headers->set('Content-Type', 'text/csv');
    $response->headers->set('Content-Disposition', 'attachment; filename=' . $filename);
    $response->setContent($csv);


Niveau 2 - Service Mail
=======================

:underline:`Déclaration du service :`

  Déclaration du service : dans le fichier ``sam.services.yml``. On déclare ici
  la dépendance de notre service au gestionnaire d'entités ``@plugin.manager.mail``.

  En bonus, utiliser le gestionnaire de langue - ``@language_manager`` - permettra
  de définir simplement la langue de l'avant des mails - à défaut d'interroger les
  préférences de langue de l'utilisateur.

  .. code-block:: yaml

    # sam.services.yml
    sam.mail_service:
      class: Drupal\sam\Service\TpService\MailService
      arguments: ['@plugin.manager.mail', '@language_manager']
      tags:

:underline:`Classe du service :`

  Le constructeur de la classe :

  .. code-block:: php

    /** @var \Drupal\Core\Mail\MailManagerInterface **/
    protected $mailManager;
    /** @var \Drupal\Core\Language\LanguageManager **/
    protected $languageManager;
    public function __construct(MailManagerInterface $mail_manager,
      LanguageManager $language_manager
    ) {
      $this->mailManager = $mail_manager;
      $this->languageManager = $language_manager;
    }

  Vous envoyez des mails avec la méthode ``mail()`` de l'`interface`_. Vous pouvez
  utiliser le tableau ``$params`` pour passer des données spécifiques utilisables
  votre ``hook_mail()``.

.. _interface: https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Mail!MailManagerInterface.php/interface/MailManagerInterface

  .. code-block:: php

    $langcode = $this->languageManager->getDefaultLanguage();
    $this->mailManager->mail($module, $key, $to, $langcode, $params, $reply, $send);

  Envoi des notifications au coach : ``sendRegistrationNotification()`` :

  .. code-block:: php

    public function sendRegistrationNotification($module, $key, $to, $activity, $name) {

      $langcode = $this->languageManager->getDefaultLanguage();
      $reply = NULL;
      $send = TRUE;
      $params['activity'] = $activity;
      $params['ref_name'] = $name;

      $this->mailManager->mail($module, $key, $to, $langcode, $params, $reply, $send);
    }

:underline:`Le hook mail() :`

  Dans ce hook, vous déclarez, en fonction de différents contextes d'envois -
  ``$key``. Vous pouvez utiliser le tableau ``$params`` pour accéder à des données
  passées au déclenchement de la fonction ``mail()``.

  .. code-block:: php

    function sam_mail($key, &$message, $params) {
      $message['from'] = $from;
      $message['headers']['Content-Type'] = 'text/html; charset=UTF-8; format=flowed; delsp=yes';
      // Use $params to get some custom datas form mail() invoke.
      // $value = $params['reference'];
      switch ($key) {
        case 'coach_notification_registration':
          $message['subject'] = t('Your custom object.');
          $message['body'][] = t('Your custom message.');
          break;
        case 'user_notification_registration':
          $message['subject'] = t('Your custom object.');
          $message['body'][] = t('Your custom message.');
          break;
      }
    }

:underline:`Implementation hook_ENTITY_TYPE_xxx() :`

  Deux hooks à implémenter :

  * `hook_ENTITY_TYPE_presave()`_ pour la création ou la modification de l'entité.
  * `hook_ENTITY_TYPE_delete()`_ pour la suppression de l'entité.

.. _hook_ENTITY_TYPE_presave(): https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Entity!entity.api.php/function/hook_ENTITY_TYPE_presave
.. _hook_ENTITY_TYPE_delete(): https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Entity!entity.api.php/function/hook_ENTITY_TYPE_delete

  Dans ce hook, vous avez accès à :

  * ``$entity`` : l'entité en cours de sauvegarde  ;
  * ``$entity->original`` : l'état de l'entité avant modification ;

  Vous pouvez donc facilement comparer les deux états, et récupérer les données
  nécessaires :

  * coach : Nom d'utilisateur et mail,
  * utilisateur : Nom d'utilisateur et mail,
  * activité : Nom de l'activité.

  Vous aurez accès à votre service avec la classe statique ``\Drupal``.

  .. code-block:: php

    \Drupal::entityTypeManager()->getStorage($entity_type_name);
    \Drupal::service('mail_service')->sendNotificationMail($params);


:underline:`Implementation hook_form_alter() :`

  Dans un ``hook_user_form_alter()``, en ajoutant une fonction de soumission
  personnalisée :

  * Vous avez accès aux données des valeurs du formulaire.
  * Vous avez accès à l'entité en cours d'édition.

  Attention au poids de votre fonction personnelle par rapport aux autres
  fonctions de soumission native de Drupal :

  * avant le ``save``, vous avez accès à l’état de l'entité avant l'enregistrement
    de l'entité,
  * après le ``save``, vous n'avez accès aux données enregistrés.

  .. code-block:: php

    function sam_user_form_alter(&$form, FormStateInterface $form_state, $form_id) {
      \array_unshift($form['actions']['submit']['#submit'], '_send_email_to_coach');
    }

  .. code-block:: php

    function _send_notification(&$form, FormStateInterface $form_state) {

      $new_activities = $form_state->getValue('field_activities');
      $entity = $form_state->getFormObject()->getEntity();
      $old_activities = $entity->get('field_activities')->getValue();
      // make diff.
      foreach () {
        \Drupal::service('mail_service')->sendNotificationMail($params);
      }
    }
