.. role:: underline


********************
🏸 - À votre service
********************

⏱ | ~ 90 min. / 120 min.

!!!!!

Nous avons jusqu'à présent utilisé beaucoup de services disponible nativement
pour nos traitements courants : ``entity_type.manager``, ``current_user``,
``route_match``, ``database``...

Il est grand temps de créer les nôtres !

L'implémentation d'un service reste relativement simple. Ce TP va nous permettre
de réutiliser des notions précédemment abordées :

* les formulaires,
* les routes et les liens de menu,
* les hooks.


Niveau 1 - Exporter les inscrits aux activités sportives
========================================================

:underline:`Notions abordées :`

  * Implémentation de service.
  * Création de formulaire.
  * Gestion des routes et des menus.
  * Traitement php.

:underline:`Exercice :`

  Créer une interface permettant de générer un export des inscrits pour une activité
  donnée. Cette interface sera accessible en back-office dans le menu administrateur
  - ``admin/config/system/export`` - dans un onglet.

  Plusieurs composants à réaliser dans cette exercices :

  * la création d'un formulaire dans lequel nous pourrons choisir une activité
    sportive,
  * la configuration de la route et du lien de menu,
  * la création d'un service qui va gérer tout la partir export.


Niveau 2 - Service Mail
=======================

:underline:`Notions abordées :`

  * Implémentation de service.
  * Implémentation de hook.
  * Gestion de l'envoi de mail.

:underline:`Exercice :`

  Créer une service permettant de gérer tous nos envois de mail. Vous utiliserez ce
  service dans les circonstances suivantes :

  **A l'inscription / désinscription d'une personne aux activités sportives :**

    * Envoi d'un notification au coach de l'activité.
