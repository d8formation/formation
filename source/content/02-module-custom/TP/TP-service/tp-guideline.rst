.. role:: underline


**********************
🗣️ - Pour vous guider
**********************


Niveau 1 - Exporter les inscrits aux activités sportives
========================================================

:underline:`Pour le routing :`

  * Définir un path pointant vers ``/admin/config/system/`` pour bénéficier du
    thème d'administration ;
  * Définir un lien de menu ayant comme parent une entrée dans le menu d'administration
    ``/admin/config/system/`` ;
  * Définir un premier onglet sur cette page vers notre interface ;

  `Un peu de documentation sur l'API Menu`_.

.. _Un peu de documentation sur l'API Menu: https://www.drupal.org/docs/drupal-apis/menu-api

:underline:`Pour le formulaire :`

  Il n'y a qu'un seul champ à afficher : un input pour choisir une activité. Deux
  possibilités la construction du formulaire :

  * créer un champ de type ``autocomplete``,
  * créer un champ de type ``select``.

  Le formulaire devra retourner un objet de type ``Response``.

  .. code-block:: php

    $form_state->setResponse($response);

:underline:`Pour le service :`

  Vous aurez besoin du gestionnaire d'entité - service ``entity_type.manager`` -
  pour sélectionner les inscrits et récupérer leurs informations.

  * Un fichier de configuration à renseigner : ``sam.services.yml``.
  * Une classe pour le service : ``ExportService.php``.

:underline:`Pour la réponse csv :`

  **Créer un fichier csv :**

    .. code-block:: php

      $handle = fopen('php://temp', 'w+');
      $header = [
        'col 1',
        'col 2',
      ];
      fputcsv($handle, $header);

      foreach ($rows as $row) {
        // get the data about the user
        $csv = [
          $row['first_data'],
          $row['second_data'],
        ];
        fputcsv($handle, $csv);
      }
      rewind($handle);
      $csv = stream_get_contents($handle);
      fclose($handle);

  **La réponse à rendre :**

    C'est cette réponse qui devra être renvoyée par le formulaire.

    .. code-block:: php

      use Symfony\Component\HttpFoundation\Response;

      $response = new Response();
      $response->headers->set('Content-Type', 'text/csv');
      $response->headers->set('Content-Disposition', 'attachment; filename=export.csv');
      $response->setContent($csv);
      return $response;


Niveau 2 - Service Mail
=======================

:underline:`Pour le service :`

  Vous aurez besoin du gestionnaire de mail - service ``plugin.manager.mail`` -
  pour gérer l'envoi de vos mails.

  * Un fichier de configuration à renseigner : ``sam.services.yml``.
  * Une classe pour le service : ``MailService.php``.

:underline:`Gestion des mails avec Drupal :`

  Il vous faudra implémenter `un nouveau hook`_ - ``hook_mail()`` - pour définir
  des gabarits de mails personnalisés.
  Je vous mets un lien vers `une série d'articles`_ très complet sur le sujet.

.. _un nouveau hook: https://api.drupal.org/api/drupal/core!core.api.php/function/hook_mail
.. _une série d'articles: https://code.tutsplus.com/series/using-and-extending-the-drupal-8-mail-api--cms-783

:underline:`Gestion des activités :`

  Il faudra envoyer un mail à chaque modification des données de l'utilisateur.
  Deux possibilités pour l'implémenter :

  * dans un ``hook_form_alter``, dans une fonction de soumission personnelle,
  * dans un ``hook_ENTITY_TYPE_presave``.

:underline:`Utiliser un service :`

  * Dans un script, avec la classe statique ``\Drupal``.
  * Dans une classe, avec une injection de service.
