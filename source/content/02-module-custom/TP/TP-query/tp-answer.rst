.. role:: underline


***************************
🏅 - Éléments de correction
***************************


Injection de service
====================

Vous serez amené à faire deux types de requêtes :

* Des requêtes sur les entités, avec la `QueryInterface`_ - accessible dans le
  controller avec le gestionnaire d'entité ``$this->entityTypeManager()``.
* Des requêtes sur les tables de la base de données, avec `la Database API`_ -
  accessible avec le service ``\Drupal::database()``.

.. _QueryInterface: https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Entity!Query!QueryInterface.php/function/QueryInterface%3A%3Acondition
.. _la Database API: https://www.drupal.org/docs/8/api/database-api

Il faudra procéder à l'injection du service ``database``.

.. code-block:: php

  \Drupal::database();

  /**  @var \Drupal\Core\Database\Driver\mysql\Connection **/
  protected $database;
  $container->get('database'),
  $this->database = $database;


Niveau 1 : Statistiques sur les données du sites
================================================

Route et entrée dans le menu
----------------------------

La route avec un chemin commençant par ``/admin`` permet de bénéficier du thème
d'administration. Vous définissez **un chemin**, mais ce n'est pas un lien dans
le menu.

Pour créer le lien, il faudra ajouter une configuration - ``sam.stat_global``
- dans le fichier ``sam.links.menu.yml``, en déclarant une route parente
``system.admin_config_system``.

Pour créer l'onglet, il faudra le déclarer dans le fichier ``sam.links.task.yml``,
en déclarant comme route de base, le nom machine de la route de notre controller
- ``sam.stat_global``.

.. code-block:: yaml

  # sam.routing.yml
  sam.stat_global:
    path: '/admin/config/system/statistics/main'
    defaults:
      _controller: '\Drupal\sam\Controller\TpQuery\Lv1GlobalStatistics::render'
      _title: 'SAM : Global Statistics'
    requirements:
      _role: 'administrator'

  # sam.links.menu.yml
  sam.stat_global_link:
    title: 'Global statistics'
    description: 'Get global statistics'
    route_name: sam.stat_global
    parent: system.admin_config_system

  # sam.links.task.yml
  sam.stat_global_task:
    title: 'Global statistics'
    description: 'Get global statistics'
    route_name: sam.stat_global
    base_route: sam.stat_global

Initialiser une requête
-----------------------

.. code-block:: php

  $nid = $this->entityTypeManager()->getStorage($entity_machine_name)->->getQuery();
  $query = $this->database->select($table, $alias);

Données utilisateurs
--------------------

:underline:`Le nombre d'utilisateur :`

    Il peut s'obtenir de deux manière différentes :

    * Avec une requête sur les entités.
    * Avec une requête ``SELECT COUNT(*)`` sur la tables  ``users``.

    .. code-block:: php

      // Counts users with database api dynamic query.
      $users = $this->database->select('users', 'u')
         ->countQuery()
         ->execute()
         ->fetchField();

      // Counts users with entity field query.
      $users = $this->entityTypeManager()
        ->getStorage('user')
        ->getQuery()
        ->count()
        ->execute();

:underline:`Les autres données :`

  Elles seront calculées avec les requêtes sur les entités :

  * Le nombre d'inscrit à la newsletter : condition sur la valeur du champ ``field_newsletter``.
  * Le nombre d'inscrits aux activités : condition sur l'existence du champ ``field_activities``.

  .. code-block:: php

    // Get count of subscribers for newsletter.
    $newsletter =  $this->entityTypeManager()
      ->getStorage('user')
      ->getQuery()
      ->condition('field_newsletter', 1)
      ->count()
      ->execute();

    // Get count of registered for activities.
    $registred = $this->entityTypeManager()
      ->getStorage('user')
      ->getQuery()
      ->exists('field_activities')
      ->count()
      ->execute();

Données activités
-----------------

:underline:`Pour le nombre d'activités :`

  De la même que pour les utilisateurs, il s'obtient de deux manières différentes :

  * Avec une requête sur les entités ``node`` - avec une condition sur le type.
  * Avec  une requête ``SELECT COUNT(*)`` sur la tables ``node``.

  .. code-block:: php

    // Counts activities with database api dynamic query.
    return $this->database->select('node', 'n')
      ->condition('n.type', 'activity', '=')
      ->countQuery()
      ->execute()
      ->fetchField();

      // Counts users with entity field query.
    return $this->entityTypeManager()
      ->getStorage('node')
      ->getQuery()
      ->condition('type', 'activity')
      ->count()
      ->execute();

:underline:`Pour le nombre d'activités sans inscrits :`

  Deux requêtes sont nécessaires :

  * Une première pour récupérer tous les ``nid`` des activités →  directement sur
    les entités.
  * Une seconde pour récupérer tous les ``nid``, distinct, référencés par les
    utilisateurs →  sur la table ``field_activities_target_id``
    les entités.

  Reste à faire la différence entre ces deux tableaux.

  .. code-block:: php

    // Get nids of activities
    $activity_nids = $this->entityTypeManager()
      ->getStorage('node')
      ->getQuery()
      ->condition('type', 'activity')
      ->execute();
    $activity_nids = \array_values($activity_nids);

    // Get distinct nids of referenced activities in user__field_activities table
    $referenced_activity_nids = $this->database->select('user__field_activities', 'ufa')
      ->fields('ufa', ['field_activities_target_id'])
      ->distinct()
      ->execute()
      ->fetchAll();
    $referenced_activity_nids = \array_column($referenced_activity_nids, 'field_activities_target_id');
    // Make difference.
    $emty_activity_nids = array_diff($activity_nids, $referenced_activity_nids);

Données articles
----------------

:underline:`Pour le nombre d'articles :`

  De la même que pour les utilisateurs ou les activités, il s'obtient de deux
  manières différentes :

  * Avec une requête sur les entités ``node`` - avec une condition sur le type.
  * Avec  une requête ``SELECT COUNT(*)`` sur la tables ``node``.

  C'est la même logique que les précédents exemples.

:underline:`Pour le nombre de nouveaux articles :`

  Avec une requête sur les entités, avec une condition sur la date de création -
  opérateur ``BETWEEN`` sur un tableau simple de deux timestamps, correspondant
  au premier et dernier jour du mois.

  .. code-block:: php

    // Get count new articles.
    $month = [
       \strtotime(\date("Y-m-d", \time()). 'first day of this month'),
       \strtotime(\date("Y-m-d", \time()). 'last day of this month'),
    ];
    return $this->entityTypeManager()
      ->getStorage('node')
      ->getQuery()
      ->condition('type', 'article')
      ->condition('created', $month, 'BETWEEN')
      ->count()
      ->execute();

:underline:`Pour le nombre d'auteurs :`

  À ce niveau, une requête ``SELECT DISTINCT(uid)`` sur la table ``node_field_data``,
  permet d'obtenir de résultat.

  Il pourrait également s'obtenir avec une requête sur les entités articles, avec
  une agrégation sur le champ ``uid``.

  .. code-block:: php

    // Get count distinct authors.
    $query = $this->database->select('node_field_data', 'nd')
      ->fields('nd', array('uid'))
      ->condition('nd.type', 'article', '=');

    return $query->distinct()
      ->countQuery()
      ->execute()
      ->fetchField();


Rendre les différents tableaux
------------------------------

Utiliser le service ``renderer`` qui permet de convertir en html des render arrays.

.. code-block:: php

  \Drupal::service('renderer')

    /** @var \Drupal\Core\Render\RendererInterface **/
    protected $renderer;
    $container->get('renderer'),
    $this->renderer = $renderer;

    // Theme table render array.
    $render_array = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ]

    $this->renderer->render($render_array);


Niveau 2 : Statistiques sur les données des activités sportives
===============================================================

Initialiser une requête
-----------------------

Les requêtes que vous avez à construire nécessite de faire des agrégations. il
faudra utiliser la `QueryAggregateInterface`_ accessible dans le
controller avec le gestionnaire d'entité ``$this->entityTypeManager()`` ;

.. _QueryAggregateInterface: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Entity%21Query%21QueryAggregateInterface.php/interface/QueryAggregateInterface

.. code-block:: php

  $this->entityTypeManager()->getStorage($entity_machine_name)->->getAggregateQuery()


Données utilisateurs
--------------------

La requête doit prendre en compte :

* Une requête faite sur les entités ``user``.
* La vérification d'une inscription à au moins une activité sportive - ``field_activities``.
* Une agrégation - ``COUNT`` - du nombre d'inscrits - ``uid``.
* Un regroupement par activité - ``field_level``.

.. code-block:: php

  // Get distribution of users by level.
  $distribution = $this->entityTypeManager()->getStorage('user')->getAggregateQuery()
    ->aggregate('uid', 'COUNT')
    ->exists('field_activities')
    ->groupBy('field_level')
    ->execute();

Données activités
-----------------

La requête doit prendre en compte :

* Une requête faite sur les entités ``node``.
* Une condition sur le type de node - ``type``.
* Une agrégation - ``COUNT`` - du nombre d'activités - ``nid``.
* Un regroupement sur le sport - ``field_sport``.

.. code-block:: php

  // Get distribution of activities by level.
  $distribution = $this->entityTypeManager->getStorage('node')->getAggregateQuery()
    ->aggregate('nid', 'COUNT')
    ->condition('type', 'activity')
    ->groupBy('field_sport')
    ->execute();

Données association
-------------------

La requête doit prendre en compte :

* Une requête faite sur les entités ``user``.
* Une agrégation - ``SUM`` - le montant de l'association - ``field_subscription``.

.. code-block:: php

  // Total number of places by activity, by level.
  $amount = $this->entityTypeManager->getStorage('user')->getAggregateQuery()
    ->aggregate('field_subscription', 'SUM')
    ->execute();
