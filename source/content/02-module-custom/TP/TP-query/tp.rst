.. role:: underline

****************************
📈 - En quête de performance
****************************

⏱ | ~ 45 min. / 60 min.

!!!!

Dans ce TP, nous allons nous focaliser sur les notions relatives aux requêtes
faites sur les données du site,

Le propos, créer des interfaces permettant de consulter quelques statistiques sur
les données du site.


Niveau 1 : Statistiques sur les données du sites
================================================

  Créer une interface, accessible dans le menu administrateur - ``admin/config/system/statistics`` -
  dans un onglet, permettant d'afficher différentes statistiques sur les données
  du site :

  :underline:`Sur les utilisateurs :`

  * Nombre total d'utilisateurs.
  * Nombre et pourcentage d'inscription à la newsletter.
  * Nombre et pourcentage d'inscription aux activités sportives.

  :underline:`Sur les activités :`

  * Nombre d'activité.
  * Nombre d'activité sans inscrit / pourcentage.

  :underline:`Sur les articles :`

  * Nombre d'articles.
  * Nombre d'articles écrit dans le mois / pourcentage.
  * Nombre distinct d'auteur / pourcentage.

  Rendre ces trois tableaux dans un template personnalisé.


Niveau 2 : Statistiques sur les données des activités sportives
===============================================================

  Créer une interface, accessible dans le menu administrateur - ``admin/config/system/...`` -
  dans un second onglet, permettant d'afficher différentes statistiques sur les
  activités sportives  :

  :underline:`Sur les utilisateurs :`

  * Répartition des inscrits aux activités sportives par tranche d'âges.

  :underline:`Sur les activités :`

  * Répartition des activités par sport.

  :underline:`Sur la gestion de l'association :`

  * Le montant total des cotisations perçues.
