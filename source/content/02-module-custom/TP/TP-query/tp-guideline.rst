.. role:: underline


**********************
🗣️ - Pour vous guider
**********************


Injection de service
====================

Vous allez devoir faire des requêtes sur les entités mais également directement
sur les tables. Il faudra injecter le service ``database``.

Vous allez devoir rendre plusieurs tableaux pour construire votre page. Vous allez
devoir utiliser le service ``renderer``


Le rendu HTML
=============

Pour rendre plusieurs éléments formatés - liste à puces, tableaux - vous devrez
utiliser service ``renderer`` - méthode ``render()`` - pour afficher un render
array et l'intégrer dans votre template.

`Consultez la documentation`_.

.. _Consultez la documentation: https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Render!RendererInterface.php/interface/RendererInterface


Niveau 1 : Statistiques sur les données du sites
================================================

:underline:`Pour le traitement des données :`

  Pour obtenir toutes les statistiques demandées, vous allez devoir faire :

  * Des requêtes sur les entités, avec la `QueryInterface`_ - accessible dans le
    controller avec le gestionnaire d'entité ``$this->entityTypeManager()``.
  * Des requêtes sur les tables de la base de données - `Database API`_ -
    accessible avec le service ``\Drupal::database()``.

  Bien souvent, la statistique demandée peut s'obtenir avec les deux méthodes.

.. _QueryInterface: https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Entity!Query!QueryInterface.php/function/QueryInterface%3A%3Acondition
.. _Database API: https://www.drupal.org/docs/8/api/database-api

:underline:`La base de données :`

  C'est le moment de se familiariser avec la structure de la base de données.

  * Appréhender la structure générale de la base de données.
  * Appréhender la structure des tables d'une entité : données de l'entité, données des champs...
  * Regarder le détail d'une table d'un champ.

:underline:`Pour le rendu des données :`

  Vous devez rendre trois tableaux, structurés de la même manière.

  * Sujet : utilisateurs, activités, articles.
  * Header : sujet, total, pourcentage.

  Essayer, autant que possible de faire une fonction générique qui permette de rendre
  tous ces tableaux.

:underline:`Pour le routing :`

  * Définir un path pointant vers ``/admin/config/system/`` pour bénéficier du
    thème d'administration.
  * Définir un lien pointant vers pour créer une entrée dans le menu d'administration
    ``/admin/config/system/``.
  * Définir un premier onglet sur cette page vers notre interface.

  `Un peu de documentation sur l'API Menu`_.

.. _Un peu de documentation sur l'API Menu: https://www.drupal.org/docs/drupal-apis/menu-api


Niveau 2 : Statistiques sur les données des activités sportives
===============================================================

:underline:`Pour le traitement des données :`

  Pour obtenir toutes les statistiques demandées, vous allez devoir faire des requêtes
  sur les entités, avec la `QueryAggregateInterface`_ - accessible dans le  controller avec
  le gestionnaire d'entité ``$this->entityTypeManager()``.

  Prenez le temps de consulter la documentation.

.. _QueryAggregateInterface: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Entity%21Query%21QueryAggregateInterface.php/interface/QueryAggregateInterface

:underline:`Pour le routing :`

  Même conseil que le niveau 1.
