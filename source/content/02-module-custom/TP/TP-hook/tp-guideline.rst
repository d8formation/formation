.. |br| raw:: html

  <br />

.. role:: underline


**********************
🗣️ - Pour vous guider
**********************


D'une manière générale
======================

* Vous pouvez vous référer à l'API des `hooks`_.
  pour connaître la signature de la fonction à implémenter.
* Vous pouvez utiliser un ``dump()`` pour inspecter la structure des données.
* Vous avez le droit de faire des recherches sur internet 🔎.

.. _hooks: https://api.drupal.org/api/drupal/core!core.api.php/group/hooks/8


Niveau 1 : Gestion des formulaires
==================================

:underline:`L'altération du formulaire :`

  * Il faut implémenter un ``hook_form_alter()`` - `documentation`_.
  * Un champ, c'est un ensemble de 3 éléments : container, un widget, un input ;
  * `Tous les éléments d'un formulaire`_.
  * Tous ces éléments sont des renders array.
  * Les clés de configuration d'un render array  sont marquées d'une clé ``#``.
  * Pour connaître toutes les clés configurations possibles, vous pouvez vous référer
    à la documentation de la `Form API`_.
  * Faite un dump() du seul champ que vous souhaitez altérer pour me comprendre sa
    structure.

.. _documentation: https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Form!form.api.php/function/hook_form_alter
.. _Tous les éléments d'un formulaire: https://api.drupal.org/api/drupal/elements
.. _Form API: https://api.drupal.org/api/drupal/developer%21topics%21forms_api_reference.html/7

:underline:`La fonction de validation :`

  * Pour définir une action de validation, regarder dans la configuration du
    formulaire, la propriété ``#validate``.
  * La signature de votre fonction de validation custom reprend les mêmes arguments
    que votre ``hook_form_alter()``.
  * Pour récupérer des informations du formulaire :
    ``$value = $form_state->getValue('field_name')``.
  * Pour renvoyer une erreur : ``$form_state->setErrorByName('field_name', $message);``.

:underline:`Traitements des données :`

  * ``$entity = \Drupal::entityTypeManager()->getStorage('node')->load($nid)``.
  * ``load($nid)`` pour une seule entité, ``loadMultiple(array $nids)`` pour
    plusieurs entités.
  * ``$entity->get('[field_name]')->isEmpty()`` pour vérifier si un champ est rempli.
  * ``$entity->get('[field_name]')->getValue()`` pour accéder à la valeur d'un champ.
  * ``$terms = $entity->get('[field_term_reference]')->referencedEntities();`` pour
    charger les entités référencées.


Niveau 2 : Gestion des entités
==============================

:underline:`Les hooks utilisables :`

  * Regardez du côté du ``hook_ENTITY_TYPE_xxxxx()``.
  * Ils existent différents hooks, fonction des actions à réaliser.
  * Documentation sur les `hooks spécifiques à la gestion des entités`_.

.. _hooks spécifiques à la gestion des entités: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Entity%21entity.api.php/9.x

:underline:`Traitements des données :`

  * ``$entity = \Drupal::entityTypeManager()->getStorage('node')->load($nid)``.
  * ``load($nid)`` pour une seule entité, ``loadMultiple(array $nids)`` pour
    plusieurs entités.
  * ``$entity->get('[field_name]')->getValue()`` pour accéder à la valeur d'un champ.
  * ``$entity->get('[field_name]')->isEmpty()`` pour vérifier si un champ est rempli.
  * ``$entity->set('[field_name]', $value)`` pour affecter une valeur.
  * ``$terms = $entity->get('[field_term_reference]')->referencedEntities();`` pour
    charger les entités référencées.


Niveau 3 : Gestion de l'affichage
=================================

:underline:`Implémentation de hook :`

  Regardez du côté du ``hook_entity_view_XXXX()`` et choisissez le bon 🤔. |br|

  * `liste des hooks`_

.. _liste des hooks: https://api.drupal.org/api/drupal/core!core.api.php/group/hooks/9.x

:underline:`Configuration de l'affichage :`

  La configuration des ``formatters``, utilisés pour rendre l'entité, se fait en
  `back-office`_ - mais elle est également accessible dans les fichiers
  de configuration. |br| 
  Ex. : ``core.entity_view_display.node.article.default.yml`` .

  Dans le hook, vous pouvez accéder à la configuration d'un champ à l'aide de la
  fonction ``$display->getComponent('field_name')``.

.. _back-office: ../../../01-concepts-drupal/entites-configurations.html#les-differents-modes-d-affichage


Niveau 4 : Administration
=========================

* Il vous faudra correctement configurer le dossier des fichiers privés.
* Vous devrez utiliser deux hooks :

    * un premier hook pour ajouter un élément de menu à la toolbar,
    * un deuxième hook pour contrôler l'accès au dossier des fichiers privés,
    * pour construire une url, vous pouvez utilisez la classe ``Url`` et la
      fonction ``Url::fromUri(file_create_url('private://documentation/index.html'))``.

* Vous pouvez consulter `un exemple d'implémentation`_ proposé par le `module examples`_.

.. _un exemple d'implémentation: https://api.drupal.org/api/examples/examples.module/function/examples_toolbar/8.x-1.x
.. _module examples: https://www.drupal.org/project/examples
