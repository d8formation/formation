.. role:: underline


************************************
🥊 - Le crochet, une arme redoutable
************************************

⏱ | 45 min. / 60 min.

!!!!

Nous allons continuer de travailler sur notre site Internet et implémenter
quelques fonctionnalités spécifiques. Pour cela, nous allons utiliser différents
hooks pour adapter le fonctionnement de Drupal à nos besoins.

Dans ce TP, nous allons travailler sur :

* l'altération de formulaire,
* la gestion des entités,
* l'affichage des contenus,
* l'adaptation du back-office.

Avec un prérequis .... Créer son premier module 🙂 !


Niveau 1 : Gestion des formulaires
==================================

  :underline:`Dans le formulaire de création des utilisateurs :`

  * Rendre le champ 'Cotisation' ``disabled``.
  * Mettre en place une validation pour vérifier que l'inscription est possible
    pour les activités sélectionnées → Classe d'âge.


Niveau 2 : Gestion des entités
==============================

  Adapter le mécanisme de gestion des comptes de telle sorte que, pour chaque
  compte créé / modifié, vous calculerez le montant de la cotisation à acquitter.


Niveau 3 : Gestion de l'affichage
=================================

  Modifier l'affichage des articles de manière à ce que :

  * Les utilisateurs anonymes n'aient accès qu'à une version tronquée de l'article.
  * Les utilisateurs connectés puissent consulter l'intégralité de l'article.


Niveau 4 : Administration
=========================

  Rendre la présente documentation accessible en back-office, en créant un lien
  dans la toolbar du menu d'administration.
