.. |br| raw:: html

  <br />

.. role:: underline


***************************
🏅 - Éléments de correction
***************************


Implémentation d'un hook
========================

✅ J'ai renommé mon hook avec le nom de mon module. |br|
✅ J'ai vérifié la signature de la fonction. |br|
✅ Je vide mon cache à chaque nouvelle implémentation. |br|


Niveau 1 : Gestion des formulaires
==================================


:underline:`Implémentation du hook_form_alter() :`

  Deux formulaires à altérer : le **formulaire de création** et le **formulaire
  d'édition** ;

  Deux manières de procéder :

  * Vous pouvez utiliser un ``hook_form_alter()`` générique et filtrer sur l'identifiant
    du formulaire ``$form_id``.
  * Vous pouvez implémenter deux ``hook_form_FORM_ID_alter()`` spécifiques à
    chaque formulaire.

  .. code-block:: php

    use Drupal\Core\Form\FormStateInterface;

    function sam_form_alter(&$form, FormStateInterface $form_state, $form_id) {
      if ($form_id == 'user_register_form' || 'user_form') {
        // Process.
      }
    }

    /**
     * Implements hook_form_BASE_FORM_ID_alter().
     */
    function sam_form_user_form_alter(&$form, FormStateInterface $form_state, $form_id){
      // Process.
    }

:underline:`Altération du formulaire :`

  La variable ``$form`` représente le formulaire en tant que tel, avec la description
  de tous les champs. Vous pouvez obtenir un champ spécifique grâce à son nom
  machine - ``$form['field_name']``. Vous aurez accès à aux éléments de configuration
  de son widget - cf. `configuration back-office`_ - et aux éléments de configuration
  de l'input.

.. _configuration back-office: ../../../01-concepts-drupal/entites-configurations.html#le-formulaire-de-contribution

  Aussi bien le champ, que le widget ou l'input sont des render array. 3 manières
  de désactiver le champ dans le formulaire :

  * configuration du champ,
  * configuration du widget,
  * configuration de l'input.

  .. code-block:: php

    $form['field_subscription']['#disabled'] = TRUE;
    $form['field_subscription']['widget']['#disabled'] = TRUE;
    $form['field_subscription']['widget'][0]['#disabled'] = TRUE;
    $form['field_subscription']['widget'][0]['value']['#disabled'] = TRUE;
    $form['field_subscription']['widget'][0]['value']['#attributes']['disabled'] = TRUE;

:underline:`Implémenter une fonction de validation :`

  * Cette fonction doit se déclencher à la création ou à l’édition d'un utilisateur.
    On peut donc utiliser un ``hook_form_BASE_FORM_ID_alter()``.
  * La clé ``$form['#validate']`` liste les fonctions de validation auxquelles
    seront soumises les données du formulaire.
  * Pour récupérer les données du formulaire : ``$form_state->getValue('field_name')``
    ou ``$form_state->getValues()``.

  .. code-block:: php

    function sam_form_user_form_alter(&$form, FormStateInterface $form_state, $form_id){
      $form['#validate'][] = '_check_activity_registration';
    }
    function _check_activity_registration(&$form, FormStateInterface $form_state) {
      // Process.
      if($error) {
        $form_state->setErrorByName('field_name', $message);
      }
    }

:underline:`Traitements des données :`

* ``$entity = \Drupal::entityTypeManager()->getStorage('node')->load($nid)``.
* ``load($nid)`` pour une seule entité, ``loadMultiple(array $nids)`` pour
  plusieurs entités .
* ``$entity->get('[field_name]')->getValue()`` pour accéder à la valeur d'un champ.
* ``$terms = $entity->get('[field_term_reference]')->referencedEntities();`` pour
  charger les entités référencées.

!!!!

Niveau 2 : Gestion des entités
==============================

:underline:`Implémentation des hooks :`

  Deux hooks à implémenter :

  * `hook_ENTITY_TYPE_presave()`_ pour la création ou la modification de l'entité ;

.. _hook_ENTITY_TYPE_presave(): https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Entity!entity.api.php/function/hook_ENTITY_TYPE_presave

  Dans ce hook, vous avez accès à :

  * ``$entity`` : l'entité en cours de sauvegarde,
  * ``$entity->original`` : l'état de l'entité avant modification.

  .. code-block:: php

    use Drupal\Core\Entity\EntityInterface;

    function sam_user_presave(EntityInterface $entity) {
      dump($entity);
      dump($entity->original);
    }

:underline:`Traitements des données :`

  * ``$entity = \Drupal::entityTypeManager()->getStorage('node')->load($nid)``.
  * ``load($nid)`` pour une seule entité, ``loadMultiple(array $nids)`` pour
    plusieurs entités .
  * ``$entity->get('[field_name]')->isEmpty()`` pour vérifier si un champ est rempli.
  * ``$entity->get('[field_name]')->getValue()`` pour accéder à la valeur d'un champ.
  * ``$entity->set('[field_name]', $value)`` pour affecter une valeur.


  .. code-block:: php

    $activities = $entity->get('field_activities')->referencedEntities();
    $amount = 0;
    foreach ($activities as $activity) {
      if (!$activity->get('field_price')->isEmpty()) {
        $price = $activity->get('field_price')->first()->getValue();
        $amount += $price['value'];
      }
    }
    $entity->set('field_subscription', ['value' => $amount]);

!!!!

Niveau 3 : Gestion de l'affichage
=================================

:underline:`Implémentation du hook :`

  Il existe plusieurs hooks concernant les entités. Pour parvenir à modifier la
  configuration du widget, il faut utiliser le ``hook_entity_view_display_alter()``.
  Il vous donne accès au contexte et à la configuration de l'entité.

  .. code-block:: php

    use Drupal\Core\Entity\Display\EntityViewDisplayInterface;

    function sam_entity_view_display_alter(EntityViewDisplayInterface $display, array $context) {
      // Process.
    }

:underline:`L'utilisateur courant :`

  Pour faire varier l'affichage du contenu de l'article, vous devez interroger le
  statut de l'utilisateur courant. Vous pouvez utiliser la classe statique
  ``\Drupal`` et sa fonction ``$current_user = \Drupal::currentUser()``.

  .. code-block:: php

    $anonymous = \Drupal::currentUser()->isAnonymous();

:underline:`La configuration du widget :`

  Le formatter est `défini et configuré en back-office`_. |br|
  Vous pouvez vous familiariser avec les possibilités de configuration du formatter
  de deux manières différentes :

  * en faisant un dump que composant que vous souhaitez inspecter,
  * dans les fichiers de configuration yml.

.. _défini et configuré en back-office: .../../../01-concepts-drupal/entites-configurations.html#les-differents-modes-d-affichage

  .. code-block:: php

    if ($context['bundle'] == 'article' && $context['view_mode'] == 'full') {
      $options = [
        "type" => "text_trimmed",
        "weight" => 0,
        "region" => "content",
        "settings" => [
          "trim_length" => 200
        ],
        "third_party_settings" => [],
        "label" => "hidden",
      ];
      $display->setComponent('body', $options);
    }

!!!!

Niveau 4 : Administration
=========================

:underline:`Configuration du répertoire des fichiers privés :`

  Ce répertoire doit être inaccessible depuis l'extérieur - *comprendre, en dehors
  du webroot*. |br|
  Il faut procéder de manière identique à la configuration du dossier de synchronisation
  des configurations.

  `Un peu de documentation`_.

.. _Un peu de documentation: https://www.drupal.org/docs/8/modules/skilling/installation/set-up-a-private-file-path

  .. code-block:: php

    /** @see sites/default/settings.php` **/
    $settings['file_private_path'] = '../private';

  .. ATTENTION::
    Il faut un fichier .htaccess dans votre dossier. |br|
    Drupal le génère automatiquement... Mais il peut arriver - *pour des raisons
    de configuration de droit d'écriture* - que le système n'y arrive pas ! |br|
    Penser à consulter vos logs !

:underline:`Implémentation du hook_toolbar() :`

  Ce hook permet de rajouter un lien dans la toolbar du menu d'administration. |br|
  Dans la documentation, vous pouvez prendre exemple sur le lien de menu "home"
  ``$items['home']`` pour l'implémentation d'un lien simple.

  .. code-block:: php

    use Drupal\Core\Url;

    function sam_toolbar() {
      $items['documentation'] = [
        '#type' => 'toolbar_item',
        'tab' => [
          '#type' => 'link',
          '#title' => t('Documentation'),
          '#url' => Url::fromUri(file_create_url('private://documentation/index.html')),
          '#options' => [
            'attributes' => [
              'target' => '_blank',
            ],
          ],
        ],
        '#weight' => 500,
      ];
      return $items;
    }

:underline:`Implémentation du hook_file_download() :`


  Ce hook permet de contrôler l'accès aux fichiers privés. |br|
  Il vous faudra interroger le statut de l'utilisateur courant - *permissions,
  rôles ...*  en utilisant la classe statique ``\Drupal`` et sa méthode
  ``$current_user = \Drupal::currentUser()``. |br|
  Les vérifications faites, vous renvoyez un tableau contenant les informations
  appropriées pour construire le header de la réponse.

  .. code-block:: php

    function sam_file_download($uri) {
      // $uri = 'private://documentation/index.html';
      if (in_array('documentation', explode('/', $uri))) {
        if (in_array('administrator', \Drupal::currentUser()->getRoles())) {
          return [
            'Content-Type: text/html; charset=utf-8'
          ];
        }
      }
      return -1;
    }
