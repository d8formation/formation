.. # define break line
.. |br| raw:: html

  <br />


***************************
🏅 - Éléments de correction
***************************

Niveau 1 - Souscrire à un Event
===============================

Le choix de l'évènement
-----------------------

Un peu de documentation sur le composant
`httpKernel de Symfony <https://symfony.com/doc/current/components/http_kernel.html>`_

.. figure:: ../../_static/httpKernel_workflow.png
    :align: right
    :figwidth: 450px

Chronologie de déclenchement des événements :

* KernelEvents::REQUEST
* KernelEvents::CONTROLLER
* KernelEvents::CONTROLLER_ARGUMENTS
* KernelEvents::VIEW
* KernelEvents::RESPONSE
* KernelEvents::FINISH_REQUEST
* KernelEvents::TERMINATE
* KernelEvents::EXCEPTION

Vous devez vous placer avant la génération de la réponse, à savoir au moment de
la request ou de la résolution du Controller.


.. code-block:: php

  use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

  public static function getSubscribedEvents() {
    $events[KernelEvents::CONTROLLER][] = ['onController'];
    return $events;
  }

  public function onController(FilterControllerEvent $event) {
    $request = $event->getRequest();
  }


Le traitement de la requête
---------------------------

Vous récupérez l'objet Request avec la methode de l'Event ``getRequest()``.

Vous devez opérer les vérification suivante :

* Il faut que la route corresponde à un node ;
* Il faut le node consulté soit de type article ;
* Il faut s'assurer qu'un tag soit renseigné ;

Vous pouvez alors déclarer votre variable globale Twig !

.. code-block:: php

  $request = $event->getRequest();
    if ($request->attributes->get('_route') == 'entity.node.canonical') {
      $node = $request->attributes->get('node');
      if ($node->bundle() == 'article') {
        if ($node->hasField('field_sport')) {
          $field_sport = $node->get('field_sport')->getValue();
          if (!empty($field_sport)) {
            $field_sport = reset($field_sport);
            $sport = $this->entityTypeManager->getStorage('taxonomy_term')->load($field_sport['target_id']);
            $name = mb_strtolower($sport->getName());
            $this->twig->addGlobal('sport_content', $name);
          }
        }
      }
    }


La déclaration de l'EventSubscriber
-----------------------------------

Vous aurez besoin de deux services :

  * ``entity_type.manager``, pour charger et récupérer les informations du tag ;
  * ``twig`` pour initialiser la variable globale ;

.. code-block:: yaml

  sam.request_subscriber:
    class: Drupal\sam\EventSubscriber\RequestSubscriber
    arguments: ['@entity_type.manager', '@twig']
    tags:
      - { name: 'event_subscriber' }


.. Note::
  Vous ne pouvez passer en argument que des services et si vous souhaitez utiliser
  une classe dans votre service, vous aurez une erreur ... |br|
  À moins d'utiliser la propriété d'autowiring de Symfony qui permet de résoudre
  directement les dépendances à partir des arguments passés dans le constructeur.
  Pour en savoir plus sur `l'autowiring <https://www.yuseferi.com/en/blog/What-Autowiring-how-use-Autowire-Drupal>`_


Theming & utilisation de la variable globale
---------------------------------------------

`Un peu de documentation sur la gestion des assets dans un module <https://www.drupal.org/docs/creating-custom-modules/adding-stylesheets-css-and-javascript-js-to-a-drupal-module>`_

* Vous déclarez une librairie css ;
* Vous définissez un style pour votre classe / votre id ;
* Vous injectez votre librairie dans votre template, soit dans un render array
  soit directement dans votre template ;

.. code-block:: HTML

  // Dans votre EventSubscriber
  $this->twig->addGlobal('sport_content', $name);

  // Dans le fichier sam/libraries
  global:
  css:
    theme:
      css/styles.css: {}

  // Dans votre template
  {{ sport_content }}

!!!!

Niveau 2 - Créer votre Event
============================


Implémenter un hook_mail()
--------------------------

Deux étapes principales :

  * Implémenter un hook mail ;
  * Utiliser le MailManager pour déclencher des envois de mails ;

Le hook_mail() vous permet de définir plusieurs gabarits de mail, distingués par
une clé. Vous pouvez ainsi configurer différemment :

  * l'expéditeur ;
  * l'entête de votre mail ;
  * le sujet ou le message du texte ;

.. code-block:: php

  /**
   * Implements hook_mail().
 */
  function sam_mail($key, &$message, $params) {
    switch ($key) {
      case 'my_cstil_key':
        $message['from'] = \Drupal::config('system.site')->get('mail');
        $message['headers']['Content-Type'] = 'text/html; charset=UTF-8; format=flowed; delsp=yes';
        $message['subject'] = $params['subject'];
        $message['body'][] = $params['message'];
        $message['options'] = [];
        if (isset($params['options']) && !empty($params['options'])) {
          foreach ($params['options'] as $option => $value) {
            $message['options'][$option] = $value;
          }
        }
        break;
    }
  }


Implémenter votre collection d'événements
-----------------------------------------

Deux aspects à réfléchir pour concevoir sa classe Events :

  * Le constructeur, à savoir les données mises à disposition de l'EventSubscriber
    qui traitera les Event ;
  * Les Event à définir, à savoir les différents types de traitement ;

Dans notre cas, nous aurons besoin :

  Pour le constructeur :

    * L'entité User traité ;
    * Une adresse mail - si l'utilisateur est anonyme ;

  Pour les événements :

    * USER_REGISTRATION pour l'enregistrement d'un utilisateur ;
    * NEWSLETTER_SUBSCRIBE pour l'inscription à la newsletter ;

  Et bien les getters 🙂 !

.. code-block:: php

  namespace Drupal\sam\Event;

  use Symfony\Component\EventDispatcher\Event;
  use Drupal\Core\Entity\EntityInterface;

  class RegistrationEvents extends Event {

    const USER_CREATE = 'sam.user_create';
    const NEWSLETTER_SUBSCRIBE = 'sam.newsletter_subscribe';

    protected $entity;
    protected $mail;

    /**
     * {@inheritdoc}
     */
    public function __construct(EntityInterface $entity = NULL, string $mail = '') {
      $this->entity = $entity;
      $this->mail = $mail;
    }

    public function getEntity() {
      return $this->entity;
    }

    public function getEmail() {
      return $this->mail;
    }
  }


Implémenter votre EventSubscriber
---------------------------------

Votre EventSubscriber portera les traitements à réaliser à l'interception des
événements auxquels il doit réagir.

Deux aspects à réfléchir :

  * le constructeur, à savoir les classes mises à disposition pour réaliser les
    traitements ;
  * les événements auxquels réagir ;

Dans notre cas,

  * Notre EventSubscriber ne réagira qu'à notre collection d'événements
    définis précedemment.
  * Nous aurons besoin des services à minima de ``entity_type.manager`` et de
    ``plugin.manager.mail`` ... |br|
    voire ``string_translation`` si votre site est multilingue !

Considéré comme un service, taggé event_subscriber, nous résolvons l'injection dans
le fichier de configuration yml ;

.. code-block:: yaml

  sam.user_registration_subscriber:
    class: Drupal\sam\EventSubscriber\RegistrationEventSubscriber
    arguments: ['@entity_type.manager', '@plugin.manager.mail']
    tags:
      - { name: 'event_subscriber' }

.. code-block:: php

  namespace Drupal\sam\EventSubscriber;

  use Drupal\Core\Entity\EntityTypeManagerInterface;
  use Drupal\sam\Event\RegistrationEvents;
  use Drupal\Core\Mail\MailManagerInterface;
  use Symfony\Component\EventDispatcher\EventSubscriberInterface;

  class RegistrationEventSubscriber implements EventSubscriberInterface {

    protected $entityTypeManager;
    protected $mailManager;

    public function __construct(EntityTypeManagerInterface $entity_manager,
      MailManagerInterface $mail_manager
    ) {
      $this->mailManager = $mail_manager;
      $this->entityTypeManager = $entity_manager;
    }

    public static function getSubscribedEvents() {
      $events[RegistrationEvents::USER_CREATE][] = ['sendMailToCoach'];
      $events[RegistrationEvent::NEWSLETTER_SUBSCRIBE][] = ['sendConfirmationMail'];
      return $events;
    }

    public function sendMailToCoach(RegistrationEvents $event) {
      $user = $event->getEntity();
      // get coach entity object in the taxonomy term field_sport of user account
      // get the coach mail and define the mail massage
      $this->mailManager->mail($module, $key, $to, $langcode, $params, $reply, $send);

    }

    public function sendConfirmationMail(RegistrationEvents $event) {
      $user = $event->getEntity();
      $mail = $event->getEmail();
      // define the mail message and send the mail :
      // $to = $user->getEmail() is user exist in the drupal database
      // $to = $mail if user is anonymous
      $this->mailManager->mail($module, $key, $to, $langcode, $params, $reply, $send);

    }
  }


Déclencher votre Event dans vos traitements
-------------------------------------------

Pour la création d'un utilisateur, nous déclencherons l'Event dans un
hook_ENTITY_TYPE_create($entity) et fournir l'entité passé en paramètre à son
constructeur - avec un mail vide ;

Pour l'inscription à la newsletter, nous déclencherons l'Event dans la fonction
submitForm de notre formulaire en fournissant à son constructeur une entité nulle
et le mail renseigné ;

.. tabs::

  .. tab:: Création d'utilsateur

    .. code-block:: php

      use Drupal\sam\Event\RegistrationEvents;

      function sam_user_create(EntityInterface $entity) {
        \Drupal::service('event_dispatcher')->dispatch(RegistrationEvent::USER_CREATE, new RegistrationEvent($entity, ''));
      }

  .. tab:: Inscription formulaire

    .. code-block:: php

      use Drupal\sam\Event\RegistrationEvents;

      public function submitForm(array &$form, FormStateInterface $form_state) {
        $mail = $form_state->getValue('mail');
        $this->eventDispatcher->dispatch(RegistrationEvents::NEWSLETTER_SUBSCRIBE, new RegistrationEvents(NULL,$mail));
      }
