.. # define break line
.. |br| raw:: html

  <br />

**********************
🗣️ - Pour vous guider
**********************

Pour le niveau 1
================

  EventSubscriber :

    * Il faudra souscire à un `événement provenant de Symfony <https://api.drupal.org/api/drupal/vendor!symfony!http-kernel!KernelEvents.php/class/KernelEvents/8.8.x>`_ |br|
      *Je vous laisse choisir celui qui sera le plus pertinent*
    * Soyez attentif au code source de la ``final class KernelEvents`` pour avoir
      un peu plus d'informations sur les objets manipulés par les différents
      événements ;
    * Pour interagir avec l'environnement Twig, vous pouvez utiliser le service
      ``twig`` fourni par le core de Drupal |br|
      **OU** |br|
      charger la classe ``Twig\Environment`` à la construction de votre Subscriber ;
    * Penser bien à déclarer votre EventSubscriber dans le fichier de déclaration
      des services ;
    * ... Avec une petite subtilité si vous choisissez de déclarer directement la
      classe ``\Twig\Environement`` ! |br|
      En savoir plus sur `l'autowiring <https://www.yuseferi.com/en/blog/What-Autowiring-how-use-Autowire-Drupal>`_

  Gestion du thème :

    * Deux manières d'utiliser une librairie css dans un composant :

      * Dans le render array du composant ``$build[]['#attached']['library'][] =
        'your_module/library_name'`` ;
      * Dans le template ``{{ attach_library('your_module/library_name') }}`` ;

  question subsidiaire

    Il y a une différence subtile dans la gestion des assets entre thème et module.
    Je vous renvoie à
    `ce point spécifique de l'API <https://www.drupal.org/docs/creating-custom-modules/adding-stylesheets-css-and-javascript-js-to-a-drupal-module#process>`_.
    Pour utiliser le thème d'un module sur toutes les pages, il faudra utiliser
    un autre hook !


Pour le niveau 2
================

  Quelques préparatifs :

    Il vous faudra imlémenter un
    `hook_mail() <https://api.drupal.org/api/drupal/core!core.api.php/function/hook_mail/8.8.x>`_
    pour définir vos mails personnalisés. Je vous mets un lien vers
    `une serie d'articles <https://code.tutsplus.com/series/using-and-extending-the-drupal-8-mail-api--cms-783>`_
    très complet sur le sujet.

  Event :

    * Votre classe devra comporter à minima deux évènements, pour la création de
      compte et pour l'inscription à la newsletter ;
    * Réfléchissez aux données dont vous aurez besoin pour réaliser vos traitements :
      il faudra les passer à votre constructeur ;

  EventSubscriber

    * Pour la gestion des mails, il existe un service, ``plugin.manager.mail`` et une
      méthode ``mail()`` ! |br|
      Le lien vers `la documentation <https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Mail%21MailManager.php/class/MailManager/8.8.x>`_
    * Il vous faudra à minima deux méthodes, une pour chaque événement suivi.

  Déclenchement de l'évènement

    Vous déclencherez l'évènement à deux endroits différents :

    * Pour la mise à jour d'un utilisateur, vous le ferez dans un``hook_user_update()``
      - un hook spécifique des ``hook_ENTITY_TYPE_update`` ;
    * Pour la newsletter, vous le ferez dans la fonction ``submitForm`` du formulaire ;
