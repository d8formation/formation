.. # define break line
.. |br| raw:: html

  <br />

*************************
🏆 - Le grand rendez-vous
*************************

Nous allons, dans ce TP, traiter les deux facettes des événements

* Souscrire à un événement natif de Drupal ;
* Créer notre propre événement que nous allons déclencher dans nos traitements ;

Niveau 1
--------

  Créer un EventSuscriber qui réagit à la requête. |br|
  Pour un article consulté, déclarer une variable globale Twig que vous utiliserez
  ensuite dans un template pour adapter la css en fonction du sport concerné.

  Pour mettre en place le test :

    * Si vous avez dans un précédent TP réalisé un thème custom... Parfait 🤗 ! |br|
      Utilisez le !
    * Si vous n'avez pas de template custom disponible ... C'est pas grave ! |br|
      Reprenez un des composants réalisés en TP - *Controller, block* - et implémenté
      un hook_theme(). |br|
      Cf TP .

  Question subsidiaire :

    Vous avez chargé spécifiquement votre librairie. |br|
    **Comment étendre ce comportement à toutes les pages ?**

Niveau 2
--------

  Créer un Event custom pour interagir avec les inscriptions réalisées sur votre
  site :

    * Pour la création d'un utilisateur ;
    * Pour l'inscription à la newsletter ;

  Créer ensuite un EventSuscriber qui régira à votre Event :

    * pour la création d'un utilisateur, envoyer au coach un email d'alerte ;
    * pour la newsletter, un email de confirmation.

  Prérequis : |br|

    Il faudra enrichir la taxonomie dédiée au sport en lui ajoutant un champs qui
    référence un utilisateur.
