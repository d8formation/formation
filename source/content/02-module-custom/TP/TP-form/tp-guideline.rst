.. role:: underline


**********************
🗣️ - Pour vous guider
**********************


Niveau 1 : Inscription à la newsletter
======================================

:underline:`Le formulaire :`

  * Vous pouvez vous référer à la classe ``InputDemo.php`` pour des exemples
    d'implémentation de champs de formulaire.
  * Pour avoir une vue complète sur les possibilités de configuration, vous pouvez
    vous référer à `l'API des formulaires`_.
  * Pour vous aider à implémenter un champs de formulaire ``autocomplete``, vous pouvez
    regarder cette `documentation`_.

.. _l'API des formulaires: https://api.drupal.org/api/drupal/developer%21topics%21forms_api_reference.html/7.x
.. _documentation : https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Entity%21Element%21EntityAutocomplete.php/class/EntityAutocomplete/

:underline:`Validation des données :`

  * Certaines vérifications de données peuvent se faire avec une bonne configuration
    du formulaire, d'autres nécessiteront une fonction spécifique.
  * Pour récupérer des informations du formulaire :
    ``$value = $form_state->getValue('field_name')``.
  * Pour renvoyer une erreur : ``$form_state->setErrorByName('field_name', $message);``.

:underline:`Message de validation :`

  Penser à regarder ce que la classe de base ``FormBase`` vous met à disposition pour
  réaliser vos traitements.


Niveau 2 : Workflow du formulaire
=================================

:underline:`Traitement de l'utilisateur connecté :`

  * Pour réaliser une redirection, vous pouvez consulter la documentation sur la
    `structure des routes relatives au user`_.
  * Le formulaire doit pouvoir être accessible à n'importe quel utilisateur,
    qu'il soit connecté ou non !
  * Pour mettre à jour un compte, vous pouvez adapter le code suivant, en étant
    bien attentif à la structure des données attendue.

.. _structure des routes relatives au user: https://api.drupal.org/api/drupal/core%21modules%21user%21user.routing.yml/

  .. code-block:: php

    $user = \Drupal::entityTypeManager()->getStorage('user')->load($uid);
    $user->set('field_name', ['value' => $value]);
    $user->save();

:underline:`Le champ select :`

  Pour adapter le champ ``select``, vous pouvez obtenir tous les termes de
  taxonomies d'un vocabulaire, en utilisant la méthode `loadTree()`_ de la
  `TermStorageInterface`_:

.. _loadTree(): https://api.drupal.org/api/drupal/core%21modules%21taxonomy%21src%21TermStorage.php/class/TermStorage/8.2.x
.. _TermStorageInterface: https://api.drupal.org/api/drupal/core%21modules%21taxonomy%21src%21TermStorageInterface.php/interface/TermStorageInterface/8.2.x

  .. code-block:: php

    $entity_type = 'entity_type';
    $vid = 'vocabulary_machine_name';
    $terms = \Drupal::entityTypeManager()->getStorage($entity_type)->loadTree($vid);
      if(!empty($terms)) {
      foreach($terms as $term) {
        // Process.
      }
    }

:underline:`L'api state :`

  Consulter `la documentation Drupal`_ et `cet article`_, proposant un exemple
  d'implémentation.

.. _la documentation Drupal: https://www.drupal.org/docs/8/api/form-api/conditional-form-fields
.. _cet article: https://www.lullabot.com/articles/form-api-states

:underline:`Traitement du formulaire :`

  * La redirection à lieu à la soumission du formulaire - à la différence de la
    précédente redirection - qui a lieu avant la construction du formulaire.
  * L'implémentation se fait dans la fonction ``submitForm()``.
  * La redirection se fait avec la FormStateInterface ``$form_state->setRedirect()``.
  * Créer un controller pour afficher les informations du formulaire.


Niveau 3 : Configuration back-office
====================================

  Pas de stress 😬, ce n'est pas très compliqué ! Faite une petite recherche sur
  internet, vous trouverez facilement un exemple d'utilisation, issu de l'API Drupal ...

  Je vous laisse rechercher tout seul ... Niveau 3 oblige !
