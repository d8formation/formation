.. role:: red
.. role:: underline


**********************
🗞 - Demander l'équipe
**********************

⏱ | ~ 45 min. / 60 min.

!!!!

La création de formulaire ne représente pas en soi une grande difficulté, le plus
compliqué étant l'implémentation de toutes les règles de gestion qui vont autour.

Nous allons créer dans un premier temps un formulaire simple d'inscription à une
newsletter, ce qui nous permettra d'aborder différents points clés :

* la création de champs,
* l'utilisation des fonctions de validation et de soumission,
* des implémentations classiques dans des workflows de formulaires : pré-remplissage
  de données, redirection, affichage de message de confirmation...

Nous ne traiterons pas encore les informations de ce formulaire, ce sera l'objet
d'un prochain exercice consacré à la gestion de la base de données.


Niveau 1 : Inscription à la newsletter
======================================

  Créer un formulaire d'inscription à la newsletter de l'association, accessible
  via un lien dans le menu de votre choix - footer, menu principal ... - en respectant
  les contraintes suivantes :

  :underline:`Le formulaire`

  * Nom :red:`*`,
  * Mail :red:`*`,
  * Centre d’intérêt sportif, avec un champ de type ``autocomplete`` :red:`*`,
  * Code postal,
  * Ville.

  :underline:`Validation des données`

  * Nom et mail obligatoire.
  * Format du code postal : 5 chiffres.
  * Si un code postal est renseigné, alors la ville doit l'être également.

  :underline:`Traitement du formulaire`

  * Afficher un message de validation.


Niveau 2 : Workflow du formulaire
=================================

  Continuer à travailler ce formulaire, en implémentant quelques fonctionnalités
  supplémentaires :

  :underline:`Traitement de l'utilisateur connecté :`

  * Si l'utilisateur connecté est déjà inscrit, mettre en place une redirection
    vers le formulaire d'édition de son compte.
  * Si l'utilisateur connecté n'est pas inscrit :

    * préremplir le formulaire avec le nom et le courriel,
    * à la soumission du formulaire, mettre à jour les informations du compte.

  :underline:`Autour de la gestion des champs :`

  * Transformer le champ ``autocomplete`` de sélection des sports en champ de type ``select``.
  * Utiliser l'API state sur le champ code postal pour faire apparaître le champ
    ville si un code postal est renseigné.

  :underline:`Traitement du formulaire :`

  * À la soumission du formulaire, renvoyer vers une page spécifique, résumant
    les informations renseignées.

  :underline:`Question subsidiaire :`

  Comment faire une redirection sans que les paramètres n'apparaissent dans l'url ?


Niveau 3 : Configuration back-office
====================================

  Construire un formulaire, de type configuration, pour gérer en back-office les
  informations relatives aux réseaux sociaux de l'association : lien Facebook, Twitter
  Instagram, LinkedIn...

  * Enregistrer ces liens dans les fichiers de configuration du site.
  * Créer un lien de menu dans l'administration du site - ``admin/config/system/...``.
