.. |br| raw:: html

  <br />

.. role:: underline


***************************
🏅 - Éléments de correction
***************************


Vous ne trouverez que des éléments de réponse concernant les principales difficultés
de ce TP. Pour une correction complète, je vous renvoie au module sam rattaché
à la formation, dans lequel vous retrouverez l'implémentation complète.


Niveau 1 : Inscription à la newsletter
======================================

:underline:`Créer un lien dans le menu principal :`

  .. code-block:: yaml

    # sam.routing.yml
    sam.newsletter_form:
      path: '/newsletter/subscribe'
      defaults:
        _form:  '\Drupal\sam\Form\NewsletterSubscribeForm'
        _title: 'Subscibe to the newsletter'
      requirements:
        _permission: 'access content'

    # sam.links.menu.yml
    sam.newsletter_form_link:
      menu_name: main
      title: 'Subscribe to the newsletter'
      route_name: sam.newsletter_form

:underline:`Input autocomplete :`

  L'input ``autocomplete`` fait référence à un type d'entité. Nous l'utilisons pour
  les termes de taxonomy, mais il peut être également utilisé sur d'autres types
  d'entités : node, user ...

  .. code-block:: php

    $form['taxonomy'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'taxonomy_term',
      '#selection_settings' => [
        'target_bundles' => [
          '[vocabulary_machine_name]',
        ],
      ],
    ];

:underline:`Validation des données du formulaire :`

  **Côté formulaire**, en jouant sur la configuration des inputs :

  * ``#required`` sur le champ sport,
  * ``#minlength``, ``#maxlength`` sur le champ code postal.

  **Avec une fonction spécifique**, pour contrôler la cohérence des donnée :

  * format du code postal : longueur, type de caractère,
  * format de la ville : existence, type de caractère.

  .. code-block:: php

    public function validateForm(array &$form, FormStateInterface $form_state) {
      $postal_code = $form_state->getValue('postal_code');
      if (!preg_match('/^[0-9]*$/', $postal_code)) {
        $form_state->setErrorByName('postal_code', $this->t('Only number are allowed !'));
      }
    }

:underline:`Afficher un message de confirmation :`

  La classe de base ``FormBase`` utilise le ``MessengerTrait``, ce qui, grâce à la
  fonction ``messenger()`` d'afficher des message flash ;

  .. code-block:: php

    public function submitForm(array &$form, FormStateInterface $form_state) {
      $this->messenger()->addStatus($this->t('Your subscription have been submitted'));
    }

!!!!

Niveau 2 : Workflow du formulaire
=================================

Traitement de l'utilisateur connecté
------------------------------------

  :underline:`Précision sur les redirections :`

    Deux types de redirection possible :

    **Dans le formulaire**

      Les redirections dans le ``buildForm`` se font à l'aide de la fonction ``redirect()``
      mise à disposition par la classe de base ``FormBase`` ;

      C'est ce type de redirection que l'on utilisera pour travailler autour de
      l'utilisateur courant :

      * L'utilisateur courant est-il connecté ?
      * L'utilisateur courant - connecté - est-il déjà inscrit à la newsletter ?

      .. code-block:: php

        public function buildForm(array $form, FormStateInterface $form_state) {
          if (!$this->currentUser()->isAnonymous()) {
            $account = $this->entityTypeManager->getStorage('user')->load($this->currentUser()->id());
            $field_newsletter = $account->get('field_newsletter')->first()->getValue();
            if (!empty($field_newsletter)) {
              if ($field_newsletter['value']) {
                return $this->redirect('entity.user.edit_form', ['user' => $this->currentUser()->id()]);
              }
            }
          }
        }

    **Dans les fonctions de traitement**

      Les redirections à la soumission du formulaire se font en utilisant la fonction
      ``setRedirect()`` de `l'interface FormStateInterface`_.

      .. code-block:: php

        public function submitForm(array &$form, FormStateInterface $form_state) {
          $values = $form_state->getValues();
          $parameters = [
            'name' => $values['name'],
            'mail' => $values['mail'],
            'sport' => $term_sport->getName(),
          ];
          $form_state->setRedirect('sam.newsletter_form_redirection', $parameters);
        }

  .. _l'interface FormStateInterface: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Form%21FormStateInterface.php/interface/FormStateInterface/

  :underline:`Mise à jour des données de l'utilisateur :`

    On utilise l'``entityTypeManger`` qui doit être injecté à la construction du
    formulaire.

    Il faut également penser à vérifier le statut de l'utilisateur courant, afin de
    ne réserver ce traitement qu'aux utilisateurs connectés.

    .. code-block:: php

      public function submitForm(array &$form, FormStateInterface $form_state) {
        if (!$this->currentUser()->isAnonymous()) {
          $account = $this->entityTypeManager->getStorage('user')->load($this->currentUser()->id());
          $account->set('field_newsletter', ['value' => 1]);
          $account->save();
        }
      }

Gestion des champs
------------------

  :underline:`Gestion du champ select :`

    On utilise toujours l'``entityTypeManger`` déjà utilisé précédemment pour récupérer
    toutes les données d'un vocabulaire - methode ``loadTree()``. Il suffit ensuite
    de formater correctement les données pour hydrater le champ.

    .. code-block:: php

      $options = [];
      $terms = $this->entityTypeManager->getStorage('taxonomy_term')->loadTree('sport');
      if (!empty($terms)) {
        foreach ($terms as $term) {
          $term_name = $term->name;
          $options[$term->tid] = $term_name;
        }
      }
      $form['sport'] = [
        '#type' => 'select',
        '#title' => $this->t('Center of interest'),
        '#required' => TRUE,
        '#options' => $options,
        '#empty_option' => $this->t('-- Select a sport --'),
      ];

  :underline:`La state Api :`

    Vous avez la possibilité de vérifier plusieurs configurations d'un champ du
    formulaire :

    * ``empty``
    * ``filled``
    * ``checked``
    * ``unchecked``
    * ``expanded``
    * ``collapsed``
    * ``value``

    Et, de la même manière, vous pouvez appliquer plusieurs configurations à un
    champs de formulaire :

    * ``enabled``
    * ``disabled``
    * ``required``
    * ``optional``
    * ``visible``
    * ``invisible``
    * ``checked``
    * ``unchecked``
    * ``expanded``
    * ``collapsed``

    Je vous renvoie à `la documentation`_.

.. _la documentation: https://www.drupal.org/docs/drupal-apis/form-api/conditional-form-fields#s-the-form-api-states-property

    Pour notre exercice, ll faut vérifier que le champs code postal est rempli
    pour faire apparaître le champs ville.

    .. code-block:: php

      $form['locality'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Your locality'),
        '#states' => [
          'invisible' => [
            ':input[name="postal_code"]' => ['filled' => FALSE],
          ],
        ],
      ];

Traitement des données du formulaire
------------------------------------

  * Si l'on veut afficher les informations renseignées, il nous faut un controller !
  * Le service ``request_stack`` permet d’accéder aux paramètres de la requête.
  * On passe en argument de la méthode ``setRedirect()`` un tableau avec les
    données du formulaire.

  .. code-block:: php

    public function redirectionNewsletterSubscriptionForm() {

      $params = $this->requestManager->getCurrentRequest()->query->all();
      $name = array_key_exists('name', $params) ? $params['name'] : '';
      $mail  = array_key_exists('mail', $params) ? $params['mail'] : '';
      $sport = array_key_exists('sport', $params) ? $params['sport'] : '';
      $list = [
        $this->t('name') . ': ' . $name,
        $this->t('Email') . ': ' . $mail,
        $this->t('Sport') . ': ' . $sport,
      ];
      return [
        '#theme' => 'item_list',
        '#items' => $list,
      ];
    }

Question subsidiaire
--------------------

  Les redirections se font avec un code réponse HTTP 302 (redirection non permanente)
  et se fait sur la base d'une méthode GET. Tous les paramètres sont donc accessibles
  dans l'url. |br|
  Une astuce pour envoyer "discrètement" des données d'une page à l'autre passe
  par l'utilisation d'un stockage temporaire à l'aide de la ``tempStoreFactory``,
  avec la logique d'implémentation que pour l'exemple de formulaire multistep.

  Je vous renvoie à la correction pour avoir plus de détail sur l'implémentation.

!!!!

Niveau 3 : Configuration back-office
====================================

Pour vous aider, vous pouvez vous appuyer sur ce `tutoriel`_ issu de l'API Drupal.

.. _tutoriel: https://www.drupal.org/docs/8/api/configuration-api/working-with-configuration-forms

Je vous renvoie à la correction pour l'implémentation complète.

.. note::
  Cette configuration sera sauvegardée dans un fichier yml, portant le nom que
  vous avez déclaré dans votre formulaire. |br|
  Exportez la configuration avec Drush avec la commande ``drush cex`` pour vous
  en rendre compte !
