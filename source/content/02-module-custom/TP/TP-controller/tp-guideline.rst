.. role:: underline


**********************
🗣️ - Pour vous guider
**********************


D'une manière générale
======================

* Vous pouvez utiliser un ``dump()`` pour inspecter la structure des données.
* Vous avez le droit de faire des recherches sur internet 🔎.


Niveau 1 - Page "S'inscrire aux activités sportives"
====================================================

:underline:`La classe ControllerBase :`

  Pensez à utiliser les fonctions mises à disposition par le ``ControllerBase``
  pour :

  * accéder à l'utilisateur courant,
  * manipuler les entités,
  * faire des redirections.

:underline:`Manipuler les données :`

  Pour obtenir les informations demandées, vous devrez utiliser différentes
  interfaces :

  * `UserInterface`_,
  * `NodeInterface`_,
  * `TermInterface`_,

.. _UserInterface: https://api.drupal.org/api/drupal/core%21modules%21user%21src%21UserInterface.php/interface/UserInterface
.. _NodeInterface: https://api.drupal.org/api/drupal/core%21modules%21node%21src%21NodeInterface.php/interface/NodeInterface
.. _TermInterface: https://api.drupal.org/api/drupal/core%21modules%21taxonomy%21src%21TermInterface.php/interface/TermInterface

  Quelques implémentations pour vous aider :

  .. code-block:: php

    // Load on entity.
    $entity = $this->entityTypeManager()
      ->getStorage($entity_type)
      ->load($id);

    // Load entities by properties.
    $entities = $this->entityTypeManager()
      ->getStorage('node')
      ->loadByProperties(['type' => 'activity']);

    // Get all the referenced entities.
    $referenced_entities = $entity->get('field_reference')->referencedEntities();

:underline:`Redirection :`

  * Vous utiliserez la méthode ``$this->redirect()`` mise à disposition par la classe
    ``ControllerBase``.
  * Consultez la classe pour accéder à la documentation et ainsi implémenter
    correctement la méthode.
  * Un petit rappel :

  .. code-block:: bash

    entity.[entity_type].canonical     // argument → ['entity_type' => $id]
    entity.[entity_type].edit-form     // argument → ['entity_type' => $id]
    entity.[entity_type].delete-form   // argument → ['entity_type' => $id]


:underline:`Construire un tableau :`

  * Vous pouvez utiliser le thème ``table`` mis à disposition par Drupal.
  * Jetez un petit coup d'oeil à la `documentation sur les thèmes communs`_
    implémentés nativement.
  * Utilisez les propriétés du `render array`_.

.. _documentation sur les thèmes communs: https://api.drupal.org/api/drupal/core%21includes%21theme.inc/function/drupal_common_theme
.. _render array: https://www.drupal.org/docs/8/api/render-api/render-arrays


Niveau 2 - Réponse Json
=======================

:underline:`Tous les conseils du niveau 1 :`

  Même si le format de réponse est différent, vous serez amenés à manipuler les
  différentes entités.

:underline:`Sérialisation des données :`

  * Vous pouvez utiliser le service de sérialisation mis à disposition par Drupal.
  * `Documentation du service`_.
  * Pensez au principe d'injection de service.

  .. code-block:: php

    $service = \Drupal::service('serialization.json')

.. _Documentation du service: https://api.drupal.org/api/drupal/core%21core.services.yml/service/serialization.json

:underline:`Réponse du controller :`

  Consulter la documentation sur `les différents types de réponses`_.

.. _les différents types de réponses: https://www.drupal.org/docs/drupal-apis/responses/responses-overview


Niveau 3 - Utiliser une API
===========================

:underline:`Se connecter à l'api :`

  * Drupal fournit un service - HTTP Client - pour manipuler des requêtes http.
  * Consultez la `documentation sur le service`_.
  * Pensez à l'injection de service.

  .. code-block:: php

    $service = \Drupal::httpClient();
    $service = \Drupal::service('http_client');

.. _documentation sur le service: https://www.drupal.org/docs/8/modules/http-client-manager/introduction

:underline:`Construire une liste à puce :`

  * Vous pouvez utiliser le thème ``items_list`` mis à disposition par Drupal.
  * Jetez un petit coup d'oeil à la `documentation sur les thèmes communs`_
    implémentés nativement.

:underline:`Pour vous amuser avec d'autres apis gratuites - sans licence :`

  * `NBA statistic`_

.. _NBA statistic: https://www.balldontlie.io/#introduction
