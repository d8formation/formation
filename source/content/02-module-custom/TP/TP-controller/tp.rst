.. role:: underline


********************************
🏄 - Contrôler sa trajectoire !️
********************************

⏱ | ~ 45 min. / 60 min.

!!!!

Ce TP va vous permettre de vous familiariser avec la notion de controller et de
routing. Il va nous permettre d'aborder :

* l'implémentation d'un controller et la configuration d'une route,
* la structure du render_array(),
* l'api drupal.


Niveau 1 - Page "S'inscrire aux activités sportives"
====================================================

  Créer une page, :underline:`accessible dans le menu principal`, qui liste toutes les
  activités proposées par l'association.

  * Les informations doivent être rendues dans un tableau.
  * Afficher à minima les informations suivantes : titre de l'activité, catégorie
    d'âges, horaire, tarif.

  :underline:`Pour aller un peu plus loin :`

  Affiner cet affichage en imposant quelques contraintes :

  * Les utilisateurs anonymes seront redirigés vers une page institutionnelle -
    "Le fonctionnement de l'association" - préalablement créée.
  * Ne pas afficher les activités auxquelles l'utilisateur est déjà inscrit.

  :underline:`Et toujours plus loin :`

  * Ajouter un lien "S'inscrire" menant vers la page d'édition de l'utilisateur.


Niveau 2 - Réponse Json
=======================

  Créer un controller qui renvoie - format json - des informations sur les activités :

  * titre de l'activité,
  * sport,
  * tranche d'âge,
  * nom du coach,
  * tarif,
  * timestamp de dernière modification.


Niveau 3 - Utiliser une API
===========================

  Créer un controller qui se connecte à une api remontant des résultats sportifs
  - `SportDB`_ - et afficher les derniers résultats des matchs d'une ligue sportive
  de votre choix. Vous remontrez ces résultats sous forme de liste.

  `Documentation de l'api`_

  :underline:`Pour aller un peu plus loin :`

  * Utiliser un thème personnalisé pour rendre les résultats.

.. _SportDB: https://www.thesportsdb.com
.. _Documentation de l'api: https://www.thesportsdb.com/api.php
