.. |br| raw:: html

  <br />

.. role:: underline


***************************
🏅 - Éléments de correction
***************************


Implémentation d'un Controller
==============================

✅ J'ai implémenté ma classe. |br|
✅ J'ai fait toutes les injections de services nécessaires. |br|
✅ J'ai bien configuré ma route. |br|
✅ Je vide mon cache à chaque nouvelle modification de la route. |br|

!!!!

Niveau 1 - Page "S'inscrire aux activités sportives"
====================================================

:underline:`Route :`

  * Une configuration pour la route du controller - ``sam.routing.yml``.
  * Une configuration pour le lien de menu - ``sam.links.menu.yml``.

  .. code-block:: yaml

    # sam.routing.yml
    sam.subscription:
      path: '/activities/subscription'
      defaults:
        _controller: \Drupal\sam\Controller\TpController\Lv1SubscriptionController::render
        _title: 'Subscribe to activities'
      requirements:
        _permission: 'access content'

    # sam.links.menu.yml
    sam.subscription_link:
      menu_name: main
      title: 'Subscription'
      route_name: sam.subscription
      weight: 1


:underline:`L'utilisateur courant :`

  * Vous accédez à l'utilisateur courant grâce à la méthode``$this->currentUser()``
    mise à disposition par la classe.
  * Cette méthode vous renvoie l'`interface AccountInterface`_ qui vous permet
    d'accéder aux informations génériques relatives à l'utilisateur courant.
  * Pour obtenir toutes les informations relatives à l'utilisateur, il faut charger
    l'entité avec le gestionnaire d'entités.

  .. code-block:: php

    // Current user account.
    $current_user = $this->currentUser();
    $anonymous = $this->currentUser()->isAnonymous();
    // Current user entity.
    $current_account = $this->entityTypeManager()
      ->getStorage('user')
      ->load($this->currentUser()->id());

.. _interface AccountInterface: https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Session!AccountInterface.php/interface/AccountInterface

:underline:`La liste de toutes les entités :`

  .. code-block:: php

    // load on entity -> return full entities
    $activities = $this->entityTypeManager()
      ->getStorage('node')
      ->loadByProperties(['type' => 'activity']);

:underline:`Accéder à la valeur d'un champs :`

  * À l'aide des méthodes magiques ou des méthodes de l'interface.
  * Soyer attentif à la structure de vos données.
  * Pensez à faire les vérifications nécessaires : existence de la données, donnnées
    non vides...

  .. code-block:: php

    // Cardinality 0..1
    if ($entity->get('field_name')->isEmpty()) {
      $field_name = $entity->get('field_name')->first()->getValue();
      $value = $field_name['value'];            // Common field.
      $target_id = $field_name['target_id'];    // Entity reference field.
    }

    // Cardinality 0..*
    if ($entity->get('field_names')->isEmpty()) {
      $field_names = $entity->get('field_names')->getValue();
      foreach($field_names as $value) {
        // Process.
      }
    }

    // Field reference.
    if ($entity->get('field_reference')->isEmpty()) {
      $referenced_entities = $entity->get('field_reference')->referencedEntities();
      foreach($referenced_entities as $referenced_entity) {
        // Process.
      }
    }

:underline:`Thème table :`

  * Un header pour l'entête du tableau.
  * Des lignes - un simple tableau comportant autant d'entrées que de colonnes
    déclarées dans l'entête.
  * Utilisez la clé ``#theme`` pour appeler le thème tableau.
  * Respectez l'implémentation : ``#header``, ``#rows``.

  .. code-block:: php

    // Loop on activities.
    foreach ($activities as $activity) {
      // Create row.
      $row[] = $activity->getTitle();
      $row[] = $level;
      $row[] = $schedule;
      $row[] = $price;
      $row[] = $link;
      $rows[] = $row;
    }
    // Display theme table.
    $header = [
      $this->t('Name'),
      $this->t('Level'),
      $this->t('Schedule'),
      $this->t('Price'),
      $this->t('Subscribe'),
    ];
    return [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ];

:underline:`Créer un lien vers le formulaire d'édition du compte utilisateur :`

  * Utilisez la classe ``Url`` pour construire l'adresse du lien - appelez la classe
    en statique.
  * Utilisez le service ``link_generator`` - retourne l'interface `LinkGeneratorInterface`_.
  * Procéder bien sur à un injection de service.
  * Utiliser la méthode ``generate()``.

.. _LinkGeneratorInterface: https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Utility!LinkGeneratorInterface.php/interface/LinkGeneratorInterface

  .. code-block:: php

    // Classes
    use Drupal\Core\Url;
    use Drupal\Core\Utility\LinkGeneratorInterface;

    // service injection
    protected $linkGenerator;

    public static function create(ContainerInterface $container) {
      return new static(
          $container->get('link_generator'),
      );
    }
    public function __construct(LinkGeneratorInterface $link_generator) {
      $this->linkGenerator = $link_generator;
    }

    // Generate link.
    $url = Url::fromRoute('entity.user.edit_form', ['user' => $this->currentUser()->id()]);
    $label = $this->t('Suscribe');
    $link = $this->linkGenerator->generate($label, $url);

!!!!

Niveau 2 - Réponse Json
=======================

:underline:`Manipulation des données :`

  Je vous renvoie aux éléments de correction précédents pour tout ce qui concerne
  la gestion des entités :

  * Le chargement des termes de taxonomie.
  * Récupérer toutes les entités "Activités sportives".
  * Récupérer la valeur des champs.

:underline:`Injection de services :`

  * Implémentez le service ``serialization`` ou ``serialization.json`` pour mettre
    au format json les données récupérées.
  * Utilisez la méthode ``encode(array $datas)``.

  .. code-block:: php

    $json = $this->serializer->encode($results);

:underline:`Injection de services :`

  * Retournez une réponse ``CacheableJsonResponse`` - permet de définir une
    configuration de cache.
  * Vous pouvez également retourner une réponse
    ``Symfony\Component\HttpFoundation\JsonResponse``.

    .. code-block:: php

      // Use Drupal\Core\Cache\CacheableJsonResponse.
      return new CacheableJsonResponse($json);
      // Use Symfony\Component\HttpFoundation\JsonResponse.
      return new JsonResponse($json);

!!!!

Niveau 3 - Utiliser une API
===========================

:underline:`Injection de services :`

  * Implémentez le service ``http_client`` pour construire votre requête et
    récupérer les résultats.
  * Implémentez le service ``serialization.json`` pour sérialiser les données
    récupérées.

  .. code-block:: php

    /** @var GuzzleHttp\Client **/
    protected $httpClient;

    /** @var \Drupal\Component\Serialization\Json **/
    protected $serializer;

    public static function create(ContainerInterface $container) {
      return new static(
        $container->get('http_client'),
        $container->get('serialization.json'),
      );
    }
    public function __construct(Client $http_client, Json $serializer) {
      $this->httpClient = $http_client;
      $this->serializer = $serializer;
    }

:underline:`Construire sa requête :`

  * API : `https://www.thesportsdb.com/api/v1/json/1/eventspastleague.php?id=4334`_
  * Utilisez la methode ``request()`` - `documentation`_.
  * Passez en argument l'identifiant de la ligue.
  * La méthode vous retourne une `interface ResponseInterface`_.
  * Vous interrogez le code retour de votre requête avec la méthode ``getStatusCode()``.
  * Vous récupérez le résultat de votre requête avec la méthode ``getBody()``.

.. _https://www.thesportsdb.com/api/v1/json/1/eventspastleague.php?id=4334: https://www.thesportsdb.com/api/v1/json/1/eventspastleague.php?id=4334
.. _documentation: https://api.drupal.org/api/drupal/vendor!guzzlehttp!guzzle!src!Client.php/class/Client
.. _interface ResponseInterface: https://api.drupal.org/api/drupal/vendor%21psr%21http-message%21src%21ResponseInterface.php/interface/ResponseInterface

  .. code-block:: php

    $response = $this->httpClient
      ->request('GET', 'https://www.thesportsdb.com/api/v1/json/1/eventsnextleague.php', [
       'query' => [
          'id' => 4334,
        ]
    ]);
    // Verify response.
    if ($response->getStatusCode() != 200) { } // Process
    // Get datas.
    $events = $this->serializer->decode($response->getBody());

:underline:`Items list :`

  * Un tableau simple avec autant d'entrées que d'éléments de la liste.
  * Utilisez la clé ``#theme`` pour appeler le thème tableau.
  * Respectez l'implémentation : ``#items``.
