**********************
🗣️ - Pour vous guider
**********************

Désactiver le cache
===================

Deux manières de procéder :

1. en commentant, dans le fichier de configuration ``settings.php``, les lignes
   permettant inclure le fichier de configuration local,
2. en utilisant la drupal console.

.. code-block:: bash

    # Root of the Drupal project
    vendor/bin/drupal site:mode prod


Configuration du cache
======================

Plusieurs points de vigilance :

* il faut que ce bloc affiche les bonnes informations d'une activité à l'autre,
* il faut veiller à ce que ce bloc s'actualise à chaque modification des données.
