.. role:: underline


***************************
🏅 - Éléments de correction
***************************


Déclaration de la configuration du cache
========================================

Le plus simple de l'implémenter dans le render array, en utilisant les données
utilisées pour afficher les données du bloc.

.. code-block:: php

  // Using render array
  return [
    '#theme' => 'item_list',
    '#items' => $list,
    '#cache' => [
      'tags' => [],
      'contexts' => [],
    ]
  ];


Dépendance aux données : cache tags
===================================

Les informations de notre bloc dépendent de plusieurs paramètres :

* les données de l'animateur sportif : son nom, son mail,
* les données du terme sport : animateur sportif référencé,
* les données de l'activité : le sport référencé.


.. code-block:: php

  'tags' => ['user:' . $coach->id(), 'node:' . $node->id(), 'taxonomy_term:' . $sport->id()],


Dépendance au contexte
======================

Le bloc est lié à une activité. Il est donc différent d'une page à l'autre. Il
dépend donc d'un contexte, le chemin de l'activité.

.. code-block:: php

  'contexts' => ['url.path'],
