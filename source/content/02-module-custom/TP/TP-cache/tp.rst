.. role:: underline


********************************
👻 - Une partie de cache - cache
********************************

⏱ | ~ 45 min. / 60 min.

!!!!

Nous venons de créer des blocs, mais, mode développement oblige, nous n'avons pas
tester le cache ! Et pourtant, il faudrait .... vraiment 🤗 !

C'est à garder en tête, il faut toujours tester le cache des blocs - dont la
configuration par défaut est ``Cache::PERMANENT`` - **et particulièrement** quand
les blocs ont un comportement dynamique.

Dans ce TP, nous allons nous servir des blocs précédemment créés pour :

* prendre le temps de tester l'incidence du cache sur notre le traitement des
  données,
* utiliser la `Cache API`_ pour configurer la mise en cache de nos blocs.

.. _Cache API: https://www.drupal.org/docs/8/api/cache-api/cache-api


Mise en place du TP
===================

:underline:`Création d'un bloc :`

  Implémenter un bloc, affiché sur les contenus de type activité, dans la région
  ``Sidebar 2``, qui affiche le nom et le courriel de l'animateur sportif qui
  encadre l'activité consultée.

  Configurer son cache.

:underline:`Configuration de l'environnement :`

  * Désactiver les caches pendant la phase de développement.
  * Activer les caches pendant la phase de test.
  * Dupliquer éventuellement votre bloc dans vos sources, pour implémenter un
    bloc sans gestion de cache et le bloc sur lequel vous testerez votre configuration.

:underline:`Préparation des tests :`

  * Il vous faudra deux fenêtres de navigation :

    * pour la navigation privé : dans une fenêtre de navigation privé, dans un
      autre navigateur...
    * pour la navigation en mode administrateur.

  * Dans votre fenêtre de navigation privé, ouvrez 3/4 onglet sur des activités
    idéalement dans plusieurs sports.
