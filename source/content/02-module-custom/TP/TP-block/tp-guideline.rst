.. role:: underline


**********************
🗣️ - Pour vous guider
**********************


Niveau 1 - Bloc articles associés
=================================

:underline:`Récupérer le contexte de la page :`

  * Pour avoir accès à des informations sur la page consulté, vous pouvez utiliser
    le service ``\Drupal::routeMatch()``.
  * Filtrer sur le nom de la route.
  * Vous pourrez accéder au node consulté et donc au tag sport.
  * `Un peu de documentation`_.

  .. code-block:: php

    $route_name = $this->routeMatch->getRouteName();
    $node = $this->routeMatch->getParameter('node');

.. _Un peu de documentation: https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Routing!RouteMatchInterface.php/interface/RouteMatchInterface/9.x

:underline:`Manipuler les données :`

  * Vous aurez besoin du gestionnaire d'entités pour manipuler les données du site
    ``\Drupal::entiyTypeManager()``.
  * Vous devez récupérer le tag de l'article consulté.
  * Le requête pour faire remonter les trois derniers articles :

  .. code-block:: php

    $nids = $this->entityTypeManager
      ->getStorage('node')
      ->getQuery()
      ->condition('type', 'article', '=')
      ->condition('field_sport', $tid, '=')
      ->sort('created')
      ->range(0, 3)
      ->execute();

:underline:`Générer un lien :`

  * Il vous faudra utiliser la classe ``\Drupal\Core\Url`` pour construire l’adresse
    du lien. N'hésitez pas à consulter la  `documentation sur la classe`_.
  * Pour créer des liens, vous pouvez utiliser le service ``\Drupal::linkGenerator()``.
    N'hésitez pas à consulter la `documentation sur le service`_.
  * Rappel sur la structure des routes :

  .. code-block:: console

    entity.[entity_type].canonical     // argument → ['entity_type' => $id] ;
    entity.[entity_type].edit-form     // argument → ['entity_type' => $id] ;
    entity.[entity_type].delete-form   // argument → ['entity_type' => $id] ;

.. _documentation sur la classe: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Url.php/class/Url/8.8.x
.. _documentation sur le service: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Utility%21LinkGeneratorInterface.php/interface/LinkGeneratorInterface/8.8.x

:underline:`Formulaire de configuration :`

  * Deux fonctions à implémenter : ``blockForm()`` et ``blockSubmit()``.
  * Accéder à la configuration du plugin : ``$config = $this->getConfiguration();``
    fournie par la classe parente ``BlockBase`` - ``BlockPluginTrait``.
  * Un seul champ - type number - pour le nombre d'articles à afficher.
  * Vous pouvez consultez `cette documentation`_.

.. _cette documentation: https://www.drupal.org/docs/drupal-apis/block-api/block-api-overview#s-add-custom-configuration-options-to-your-block


Niveau 2 - Bloc newsletter
==========================

:underline:`Gestion de l'utilisateur courant :`

   Il faudra implémenter deux services :

  * ``\Drupal::currentUser()`` pour accéder à l'utilisateur courant,
  * ``\Drupal::entityTypeManager()`` pour charger l'utilisateur courant et accéder
    à ces données.

:underline:`Affichage du formulaire :`

  Il faudra utiliser un autre service, ``\Drupal::formBuilder()``.
  Nous avons évoqué son existence dans `la partie sur les controllers`_, dans les
  services mis à disposition par la classe parente ``ControllerBase``.

.. _la partie sur les controllers: ../../controller.html#la-classe-controllerbase


Niveau 3 - Bloc réseaux sociaux
===============================

Cet exercice n'est pas vraiment difficile. Il nous permet de d'utiliser un
nouveau service que nous n'avions pas encore utilisé jusque là :
``\Drupal::config()``.

Il vous faudra également implémenter un thème personnalisé pour ce bloc, à l'aide
d'un ``hook_theme()`` - *Eh oui, un nouveau hook()** 🤗 *!*

Vous pouvez regarder `la documentation du hook`_.

.. _la documentation du hook: https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Render!theme.api.php/function/hook_theme/9.x
