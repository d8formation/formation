.. role:: underline


**************************
‍🧗 - À bloc sur les blocs
**************************

⏱ | 45 min. / 60 min.

!!!!

Les blocs sont des éléments très utiles pour afficher des informations de manière
contextuelle : par page, par type de contenus, par rôle ...

Dans ce TP, nous allons nous en servir pour agrémenter nos pages de contenus
supplémentaires, toujours en lien avec la taxonomie.

Au travers de ces exercices, nous allons nous familiariser avec :

* l'injection de services dans les blocs,
* l'utilisation du formulaire de configuration,
* l'utilisation de template spécifique.


Niveau 1 - Bloc articles associés
=================================

  Implémenter un bloc, affiché sur les contenus de type articles, dans la région
  ``Sidebar 2``, en respectant les contraintes suivantes :

  * Il affiche la liste des 3 derniers articles - lien cliquable.
  * Les articles doivent avoir le même tag sport que l'article consulté.

  :underline:`Question subsidiaire :`

  Implémenter un formulaire de configuration pour déterminer le nombre d'articles
  associés à remonter.


Niveau 2 - Bloc newsletter
==========================

  Implémenter un bloc, affiché sur toutes les pages, dans une région footer, dans
  lequel vous afficherez le formulaire d'inscription à la newsletter précédemment
  créé.

  Afficher le bloc si seulement l'utilisateur courant n'est pas déjà inscrit
  à la newsletter.


Niveau 3 - Bloc réseaux sociaux
===============================

  Implémenter un bloc, affiché sur toutes les pages, dans une région footer, dans
  lequel vous afficherez les liens vers les réseaux sociaux de l'association,
  que l'on a configuré en back-office - voir TP 3 Formulaire, niveau 3.

  Utiliser un thème personnalisé.
