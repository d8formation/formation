.. role:: underline


***************************
🏅 - Éléments de correction
***************************


Vous ne trouverez que des éléments de réponse concernant les principales difficultés
de ce TP. Pour une correction complète, je vous renvoie au module sam rattaché
à la formation, dans lequel vous retrouverez l'implémentation complète.


Niveau 1 - Bloc articles associés
=================================

:underline:`Injection de services :`

  Trois services à injecter :

  * ``\Drupal::routeMatch()`` ;
  * ``\Drupal::entiyTypeManager()`` ;
  * ``\Drupal::linkGenerator()`` ;

  .. code-block:: php

    \Drupal::entityTypeManager()
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface **/
    protected $entityTypeManager;
    $container->get('entity_type.manager'),
    $this->entityTypeManager = $entity_type_manager;

    \Drupal::routeMatch()
    /** @var \Drupal\Core\Routing\RouteMatchInterface **/
    protected $routeMatch;
    $container->get('current_route_match'),
    $this->routeMatch = $route_match;

    \Drupal::linkGenerator()
    /** @var \Drupal\Core\Utility\LinkGeneratorInterface **/
    protected $linkGenerator;
    $container->get('link_generator'),
    $this->linkGenerator = $link_generator;

:underline:`Contexte de la page :`

  On utilise le service ``current_route_match`` pour obtenir le nom de la route
  et récupérer l'entité consultée.

  .. code-block:: php

    if ($this->routeMatch->getRouteName() === 'entity.node.canonical') {
      $node = $this->routeMatch->getParameter('node');
      $field_sport = $node->get('field_sport')->getValue();
    }

:underline:`Création des liens :`

  * Injectez le service ``link_generator`` - retourne l'interface
    `LinkGeneratorInterface`_.
  * Utilisez la classe ``use Drupal\Core\Url`` pour construire l'adresse du lien.
  * Route : ``'entity.[entity_type].canonical'``    // argument → ``['entity_type' => $id]``
  * Utiliser la méthode ``generate()``.

.. _LinkGeneratorInterface: https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Utility!LinkGeneratorInterface.php/interface/LinkGeneratorInterface/9.x

  .. code-block:: php

    use Drupal\Core\Url;
    $label = $article->getTitle();
    $url = Url::fromRoute('entity.node.canonical', ['node' => $article->id()]);
    $list[] = $this->linkGenerator->generate($label, $url);


:underline:`Formulaire de configuration :`

  * Accéder à la configuration du plugin : ``$config = $this->getConfiguration();``.
    fournie par la classe parente ``BlockBase`` - ``BlockPluginTrait``.
  * Un seul champ - type number - pour le nombre d'articles à afficher.
  * Deux fonctions à implémenter : ``blockForm()`` et ``blockSubmit()``.
  * On utilise la configuration pour construire le formulaire.

  .. code-block:: php

    use Drupal\Core\Form\FormStateInterface;

    public function blockForm($form, FormStateInterface $form_state) {
      $form = parent::blockForm($form, $form_state);
      $form['range'] = [
        '#type' => 'number',
        '#default_value' => isset($config['range']) ? $config['range'] : 3,
      ];
      return $form;
    }

    public function blockSubmit($form, FormStateInterface $form_state) {
      parent::blockSubmit($form, $form_state);
      $this->configuration['range'] = $form_state->getValue('range');
    }

    public function build() {
      $config = $this->getConfiguration();
      $range = !empty($config['range']) ? $config['range'] : 3;
       // Process.
    }

!!!!

Niveau 2 - Bloc newsletter
==========================


:underline:`Injection de services :`

  Trois services à injecter :

  * ``\Drupal::currentUser()`` ;
  * ``\Drupal::entiyTypeManager()`` ;
  * ``\Drupal::formBuilder()`` ;

  .. code-block:: php

    \Drupal::currentUser()
    /** @var \Drupal\Core\Session\AccountProxy **/
    protected $currentUser;
    $container->get('current_user'),
    $this->currentUser = $current_user;

    \Drupal::entityTypeManager()
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface **/
    protected $entityTypeManager;
    $container->get('entity_type.manager'),
    $this->entityTypeManager = $entity_type_manager;

    \Drupal::formBuilder()
    /** @var \Drupal\Core\Form\FormBuilderInterface **/
    protected $formBuilder;
    $container->get('form_builder'),
    $this->formBuilder = $form_builder;


:underline:`Gestion de l'utilisateur courant :`

  On utilise les services ``\Drupal::currentUser()`` et ``\Drupal::entityTypeManager()``
  pour vérifier :

  * si l'utilisateur courant est anonyme,
  * si l'utilisateur, connecté, est déjà inscrit à la newsletter.

  .. code-block:: php

    if (!$this->currentUser->isAnonymous()) {
      $account = $this->entityTypeManager->getStorage('user')->load($this->currentUser->id());
      $field_newsletter = $account->get('field_newsletter')->first()->getValue();
      if (!empty($field_newsletter)) {
        if ($field_newsletter['value']) {
        return;
        }
      }
    }

:underline:`Le rendu du formulaire :`

  On utilise le service ``\Drupal::formBuilder()``  et on charge le formulaire
  avec la méthode ``getForm()``.

  .. code-block:: php

    // Construction du bloc
    public function build() {
      $form = $this->formBuilder
        ->getForm('Drupal\sam\Form\TpForm\Lv1NewsletterSubscribeForm');
      return $form;
    }

!!!!

Niveau 3 - Bloc réseaux sociaux
===============================

:underline:`Récupération de la configuration :`

  On utilise le service ``\Drupal::config()`` pour accéder à la configuration du
  système.

  .. code-block:: php

    $social_network_config = $this->config->get('sam.settings');
    $config = $social_network_config->getRawData();

  .. Note::
    Je vous renvoie `aux éléments de correction proposés pour le TP formulaire`_ -
    niveau 3 - pour avoir plus d'information sur la gestion de la configuration.

.. _aux éléments de correction proposés pour le TP formulaire: ../TP3/tp-answer.html

:underline:`Implémentation du hook_theme() :`

  Trois implémentations pour utiliser un template personnalisé :

    * implémenter le ``hook_theme()`` dans le fichier sam.module,
    * implémenter le thème dans un dossier templates,
    * déclarer votre thème dans le render array.

  .. code-block:: php

    // sam.module.
    function sam_theme($existing, $type, $theme, $path){
      $themes['block_socialnetwork'] = [
        'template' => 'block/block-socialnetwork',
        'variables' => [
          'title' => NULL,
          'config' => NULL,
        ],
      ];
      return $themes;
    }

  .. code-block:: html

    {# block-socialnetwork.html.twig #}
    <ul>
      {% for label, link in config %}
        <li>
          <a href="{{link }}" target="_blank">{{ label }}</a>
        </li>
      {% endfor %}
    </ul>

  .. code-block:: php

    // Class block.
    public function build() {
      return [
        '#theme' => 'block_socialnetwork',
        '#title' => $this->t('Social network'),
        '#config' => $config,
      ];
    }
