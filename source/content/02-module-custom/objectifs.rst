.. |br| raw:: html

  <br />

*********
Objectifs
*********

À partir des notions abordées, vous serez capable de :

* décrire l'architecture de Drupal, la structure d'un module,
* expliquer l’intérêt de créer un module personnel,
* décrire le principe de fonctionnement des hooks,
* implémenter des hooks,
* décrire différents composants : Controller, Formulaire, Block ...,
* implémenter des composants,
* manipuler les données au travers l'api Drupal.

!!!!!

:underline:`Ressource de la formation :`

* `Training module`_ 
* `Correction des exercices`_ 

.. _Training module: https://gitlab.com/zewebmaster1/training-drupal
.. _Correction des exercices: https://gitlab.com/zewebmaster1/sam

:underline:`Documentation Drupal :`

  `La documentation de référence`_ est assez complète pour acquérir les bases.
  N'hésitez pas à la consulter pour aller plus loin dans votre compréhension de
  ce framework.

.. _La documentation de référence: https://www.drupal.org/docs/8/creating-custom-modules

:underline:`Module examples :`

  C'est un `module communautaire`_ qui vous aidera dans l'implémentation de certaines
  fonctionnalités. C'est un paquet composé d'une série de modules individuels qui
  propose un focus sur de nombreuses fonctionnalités et les bonnes manières pour
  les implémenter.

.. _module communautaire: https://www.drupal.org/project/examples

  .. code-block:: bash

    # Root of the Drupal project
    composer require drupal/examples

:underline:`Coding Standard :`

  Prenez connaissance des `standards de code`_ pour commencer directement avec les
  bonnes pratiques.

..  _standards de code: https://www.drupal.org/docs/develop/standards/coding-standards
