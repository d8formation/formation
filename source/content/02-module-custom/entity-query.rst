.. |br| raw:: html

  <br />

.. role:: underline


************************
Requêtes sur les entités
************************


.. admonition:: Documentation
  :class: Note

  *  `QueryInterface`_
  *  `QueryAggregateInterface`_

.. _QueryInterface: https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Entity!Query!QueryInterface.php/function/QueryInterface%3A%3Acondition/
.. _QueryAggregateInterface: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Entity%21Query%21QueryAggregateInterface.php/interface/QueryAggregateInterface/

Le gestionnaire d'entité - ``entityTypeManager`` - nous permet de manipuler les entités,
mais également de construire des requêtes.

* sur toutes les entités gérées par le framework : node, blocs, utilisateurs, termes...,
* des conditions émises sur les champs qui structurent l'entité,
* la possibilité d'agréger les résultats,
* des possibilités de raffinement : limit, sort, pager...

Ces requêtes nous renvoient un tableau d'identifiants des entités correspondantes
à nos critères de recherche.


Requête simple sur les entités
==============================

La construction d'une requête simple se fait avec `une interface spécifique`_
- ``QueryInterface``.

.. _une interface spécifique: https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Entity!Query!QueryInterface.php/interface/QueryInterface/

.. code-block:: php

  $query = \Drupal::entityQuery('entity_type_name');
  $query = $this->entityTypeManager->getStorage('entity_type_name')->getQuery()

:underline:`Les conditions :`

`Les conditions`_ sont posées sur les éléments qui structurent une entité :

* des champs généraux : type, date de création, auteur...,
* les champs spécifiques de l'entité,
* la possibilité de parcourir les propriétés d'une entité référencée,
* la langue.

.. _Les conditions: https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Entity!Query!QueryInterface.php/function/QueryInterface%3A%3Acondition/

.. code-block:: php

  public function condition($field, $value = NULL, $operator = NULL, $langcode = NULL)

  // Example.
  ->condition('type', 'article', '=')
  ->condition('created', '1601678170', '>')
  ->condition('field_date', '2019-09-19', '<')
  ->condition('field_tags', 1)
  ->condition('field_tags.entity:taxonomy_term.name', 'a', 'STARTS_WITH')

:underline:`Les mécanismes de raffinement :`

Les résultats de vos recherches peuvent être affinés :

* nombre de résultats remontés : ``range()``,
* critère de tri : ``sort()``,
* nombre de résultats : ``count()``,
* ...

Parcourez la documentation de l'interface - `QueryInterface`_ - pour appréhender
toutes les méthodes à votre disposition.

.. code-block:: php

  public function range($start = NULL, $length = NULL);
  public function sort($field, $direction = 'ASC', $langcode = NULL);
  public function count();

  // Example.
  ->range(0, 20)
  ->sort('username', 'DESC')
  ->count()

:underline:`Un exemple d'implémentation :`

.. code-block:: php

  // $range last articles.
  $nids = $this->entityTypeManager
    ->getStorage('node')
    ->getQuery()
    ->condition('type', 'article', '=')
    ->sort('created')
    ->range(0, $range)
    ->execute();


Requête avec agrégation de données
==================================

La construction d'une requête, permettant d'agréger se fait avec `une autre interface`_
- ``QueryAggregateInterface``, qui étend la``QueryInterface``.

.. _une autre interface: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Entity%21Query%21QueryAggregateInterface.php/interface/QueryAggregateInterface/

.. code-block:: php

  $aggregate_query = \Drupal::entityQueryAggregate('entity_type_name');
  $aggregate_query = $this->entityTypeManager->getStorage('entity_type_name')->getAggregateQuery()

:underline:`Les mécanismes d'agrégation :`

* une agrégation simple, sur un champ avec une fonction d'agrégation à préciser :
  SUM, AVG, MIN, MAX, COUNT,
* une agrégation, qui nécessite de satisfaire une condition particulière.

.. code-block:: php

  public function aggregate($field, $function, $langcode = NULL, &$alias = NULL);
  public function conditionAggregate($field, $function = NULL, $value = NULL, $operator = '=', $langcode = NULL);

:underline:`Les méthodes de raffinement :`

* regrouper les résultats : ``groupBy()`` ;
* critère de tri : ``sortAggregate()`` ;

.. code-block:: php

  public function groupBy($field);
  public function sortAggregate($field, $function, $direction = 'ASC', $langcode = NULL);

:underline:`Un exemple d'implémentation :`

  .. code-block:: php

    // Number of articles by author.
    $nids = $this->entityTypeManager->getStorage('node')->getAggregateQuery()
      ->aggregate('nid', 'COUNT')
      ->condition('type', 'article')
      ->groupBy('uid')
      ->execute();


Parcourir les résultats
=======================

La requête renvoie un tableau d'identifiant correspondant aux entités recherchées.
Vous pouvez ensuite parcourir ce résultats dans une structure itérative. Question
performance, préférer charger toutes les entités d'une seul coup avec la méthode
``loadMultiple()`` plutôt que d'effectuer plusieurs ``load()``.

.. code-block:: php

  $entities = $this->entityTypeManager->getStorage($entity_type_name)->loadMultiple(array $entity_ids)
  foreach ($entities as $entity) {
    // Process
  }
