.. |br| raw:: html

  <br />

.. role:: underline


*********************
Structure d'un module
*********************


Les modules permettent d'étendre les fonctionnalités de base de Drupal. Les
`modules communautaires`_ répondent à des besoins très largement partagés
- géolocalisation, référencement, sécurité, mais il s'avère vite nécessaire de
créer ses propres modules pour répondre à deux grands besoins que sont :

* altérer le comportement natif de Drupal grâce aux mécanismes des hooks,
* la création de composants spécifiques.

.. _modules communautaires: https://www.drupal.org/project/project_module


Rappel sur l'autoloading
========================

.. admonition:: Documentation
  :class: Note

  `Les namespaces et l'autoloading dans Drupal`_ ;

L'`autoloading`_ est un mécanisme qui permet de charger automatiquement les
dépendances nécessaire à notre application, c'est à dire sans avoir à les inclure
explicitement. Mécanisme normé, Drupal implémente la `recommandation PSR-4`_,
avec un autoloading reposant sur les namespaces [1]_.

.. _Les namespaces et l'autoloading dans Drupal: https://www.drupal.org/docs/develop/standards/psr-4-namespaces-and-autoloading-in-drupal-8
.. _autoloading: https://getcomposer.org/doc/01-basic-usage.md#autoloading
.. _recommandation PSR-4: https://www.php-fig.org/psr/psr-4/

1. Chaque module a son propre namespace, correspondant au nom du module.
2. Le namespace du module est mappé sur le dossier ``src`` du module [2]_.
3. À partir du namespace du module, tout est mappé sur les dossiers et les fichiers
   du module.

.. code-block:: bash

 Drupal\training\                              → training/src/
 Drupal\training\Controller\TrainingController → training/src/Controller/TrainingController.php
 Drupal\training\Folder\CustomController       → training/src/Forlder/CustomController.php


Fort de ces considérations, il est possible de structurer un module librement, à
l’`exception des plugins`_ - entités ou tout autre composant utilisant des annotations.
Néanmoins, le respect de certaines conventions participe à la lisibilité et à la
maintenabilité du module.

.. _exception des plugins: https://www.drupal.org/docs/8/api/plugin-api/annotations-based-plugins#s-registering-a-plugin

.. figure:: _static/module_structure.png
    :align: right
    :width: 100%


Structure d'un module
=====================

On trouve dans un module :

* des fichiers de configuration : ``.yml`` ;
* des scripts de traitement : ``.module`` , ``.install`` , ``.php`` ... ;
* le dossier des librairies css / js ;
* le dossier des sources pour les composants ;


Déclarer un module
==================

Tous les modules personnalisés sont placés dans le dossier ``/modules/custom``. |br|
Un seul fichier est nécessaire pour déclarer un module : ``[module_name].info.yml``

.. code-block:: yaml

  # training.info.yml
  name: 'Training Drupal Development'
  package: 'Formation'
  type: module
  core_version_requirement: ^8 || ^9
  description: 'The basics of Drupal development'


Les composants du module
========================

Tous les principaux composants d'un site sont programmables dans un module :

* `des Controllers <controller.html>`_,
* `des formulaires <form.html>`_,
* `des blocks <block.html>`_,
* différents plugins : field, widget..,
* `des services <services.html>`_,
* `des Event / EventSubscriber <events.html>`_.

Cette liste n'est pas exhaustive. Elle ne reprend que les composants qui seront
abordés tout au long de cette formation.

.. code-block:: bash

  /src
    /Controller
      TrainingController.php
    /Form
      TrainingForm.php
    /Plugin/Block
      TrainingBlock.php
    /Service
      TrainingService.php


Les autres fichiers
===================

Outre les composants, un module s'accompagne de deux autres types de fichiers :

* des fichiers de script comme le ``.module`` ou le ``.install``,
* des fichiers ``yaml`` de configuration.

:underline:`Exemple de fichiers courants`

+--------------------------------+---------------------------------------------------------+
| Fichier                        | Configuration                                           |
+================================+=========================================================+
| [module_name].module           | Déclaration des hooks et autres fonctions de traitement |
+--------------------------------+---------------------------------------------------------+
| [module_name].services.yml     | `Configuration des services`_                           |
+--------------------------------+---------------------------------------------------------+
| [module_name].routing.yml      | `Configuration des routes`_                             |
+--------------------------------+---------------------------------------------------------+
| [module_name].links.menu.yml   | `Configuration des liens de menu`_                      |
+--------------------------------+---------------------------------------------------------+
| [module_name].permissions.yml  | Configuration des permissions                           |
+--------------------------------+---------------------------------------------------------+
| [module_name].librairies.yml   | Librairies utilisées                                    |
+--------------------------------+---------------------------------------------------------+
| [module_name].install          | `Traitements à réaliser à l'installation du module`_    |
+--------------------------------+---------------------------------------------------------+

.. _Configuration des services: services.html
.. _Configuration des routes: routing.html
.. _Configuration des liens de menu: routing.html#les-fichiers-de-configuration-des-menus
.. _Traitements à réaliser à l'installation du module: database.html


Créer un premier module
=======================

Deux méthodes :

* à la main,
* en utilisant l'outil CLI Drupal console.

.. code-block:: bash

  # Root of the Drupal project
  vendor/bin/drupal gm

.. figure:: _static/module_drupal_console_gm.png
    :align: center

!!!!

.. [1] Cf. ``index.php`` & ``vendor/composer``
.. [2] Cf. `DrupalKernel::getModuleNamespacesPsr4`_

.. _DrupalKernel::getModuleNamespacesPsr4: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21DrupalKernel.php/function/DrupalKernel%3A%3AgetModuleNamespacesPsr4
