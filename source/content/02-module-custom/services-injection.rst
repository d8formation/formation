.. |br| raw:: html

  <br />

.. role:: underline


*********************************
Services et injection de services
*********************************


.. admonition:: Documentation
  :class: Note

  * `La documentation Drupal sur l'injection de services`_
  * `Liste des services natifs`_

.. _La documentation Drupal sur l'injection de services: https://www.drupal.org/docs/8/api/services-and-dependency-injection/services-and-dependency-injection-in-drupal-8
.. _Liste des services natifs: https://api.drupal.org/api/drupal/services/


Quelques notions sur les services
=================================

Un service, c'est une classe regroupant un ensemble de fonctionnalités dédié à une
tâche spécifique : accéder à la base de données, logger des messages, envoyer un
mail, manipuler une entité...

Le propre d'un service, c'est d'être réutilisable et facilement accessible.

La plupart des fonctionnalités des modules du core de Drupal sont implémentées
sont formes de `services`_. |br|
Quelques exemples des services courants :

.. _services: https://api.drupal.org/api/drupal/services/

* `current_user`_,
* `entity_type.manager`_,
* `current_route_match`_,
* `request_stack`_,
* `database`_,
* ...

.. _current_user: https://api.drupal.org/api/drupal/core%21core.services.yml/service/current_user
.. _entity_type.manager: https://api.drupal.org/api/drupal/core%21core.services.yml/service/entity_type.manager
.. _current_route_match: https://api.drupal.org/api/drupal/core%21core.services.yml/service/current_route_match
.. _request_stack: https://api.drupal.org/api/drupal/core%21core.services.yml/service/request_stack
.. _database: https://api.drupal.org/api/drupal/core%21core.services.yml/service/database


Sans compter sur les services fournies par les modules communautaires ou les modules
personnels.


Utilisation des services
========================

:underline:`Dans un script :`

  *Ex : dans un hook...* |br|
  À l'aide de la classe statique [1]_ `Drupal`_ qui vous permet d'utiliser un
  certain nombres de services courants ou de charger au besoin n'importe quel service.

.. _Drupal: https://api.drupal.org/api/drupal/core!lib!Drupal.php/class/Drupal

  .. code-block:: php

      \Drupal::currentUser();
      \Drupal::logger('My channel')->warning('My message');
      \Drupal::service('[service_name]')->method();


:underline:`Dans une classe :`

  *Ex : dans un controller, un formulaire, un block...* |br|
  `Bonne pratique`_, il faut avoir recours aux mécanismes d'injection de dépendance
  et utiliser le conteneur de service qui les rendra disponibles.

.. _Bonne pratique: https://www.drupal.org/docs/8/api/services-and-dependency-injection/services-and-dependency-injection-in-drupal-8#s-accessing-services

Injection de service dans une classe
====================================

Deux méthodes à surcharger :

* la fonction ``create()`` pour construire le container et instancier les services,
* la fonction ``__construct()`` pour hydrater les variables dédiées.

  .. tabs::

    .. tab:: Controller

      .. code-block:: php

        use Drupal\Core\Controller\ControllerBase;
        use Drupal\Core\Mail\MailManagerInterface;
        use Symfony\Component\DependencyInjection\ContainerInterface;

        class TrainingController extends ControllerBase {

          protected $mailManager;

          public static function create(ContainerInterface $container) {
            return new static(
              $container->get('plugin.manager.mail')
            ) ;
          }
          public function __construct(MailManagerInterface $mail_manager) {
            $this->mailManager = $mail_manager;
          }
        }

    .. tab:: Formulaire

      .. code-block:: php

          use Drupal\Core\Form\FormBase;
          use Drupal\Core\Form\FormStateInterface;
          use Drupal\Core\Mail\MailManagerInterface;
          use Symfony\Component\DependencyInjection\ContainerInterface;

          class TrainingForm extends FormBase {

            protected $mailManager;

            public static function create(ContainerInterface $container) {
              return new static(
                $container->get('plugin.manager.mail')
              ) ;
            }
            public function __construct(MailManagerInterface $mail_manager) {
              $this->mailManager = $mail_manager;
            }
          }

    .. tab:: Block

      La signature des fonctions ``create()`` et ``_construct()`` est différente,
      car un bloc est un plugin. Néanmoins, le principe reste le même.

      .. code-block:: php

        use Drupal\Core\Block\BlockBase;
        use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
        use Drupal\Core\Mail\MailManagerInterface;
        use Symfony\Component\DependencyInjection\ContainerInterface;

        class TrainingBlock extends BlockBase implements ContainerFactoryPluginInterface {

          protected $mailManager;

          public static function create(ContainerInterface $container,
            array $configuration,
            $plugin_id,
            $plugin_definition
          ) {
              return new static(
                $configuration,
                $plugin_id,
                $plugin_definition,
                $container->get('plugin.manager.mail'),
            );
          }

          public function __construct(array $configuration,
            $plugin_id,
            $plugin_definition,
            MailManagerInterface $mail_manager
          ) {
              parent::__construct($configuration, $plugin_id, $plugin_definition);
              $this->mailManager = $mail_manager;
          }
        }

!!!!

.. [1] Le fait de déclarer des propriétés ou des méthodes comme statiques vous permet
   d'y accéder sans avoir besoin d'instancier la classe. On ne peut accéder à une
   propriété déclarée comme statique avec l'objet instancié d'une classe (bien que
   ce soit possible pour une méthode statique).
