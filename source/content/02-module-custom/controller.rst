.. |br| raw:: html

  <br />

.. role:: underline


***************
Les controllers
***************


Un controller est une classe qui réalise des traitements :

* afficher des données,
* effectuer des redirections,
* opérer des contrôles d'autorisation,
* interagir avec des webservices, des apis,
* ...

Un controller étend la classe ``ControllerBase`` qui implémente quelques services
courants pour les fonctionnalités de base, qu'il est possible d'étendre par
`injection de services`_.

.. _injection de services: injection-services.html

Un premier controller
=====================

1. Créer la classe **[ControllerName]Controller.php** dans le dossier ``/src/Controller/`` ;
2. Implémenter une fonction qui retourne un simple hello world ;
3. Déclarer une route dans le fichier de configuration ``[module_name].routing.yml`` ;

.. code-block:: php

  namespace Drupal\training\Controller;

  use Drupal\Core\Controller\ControllerBase;

  class TrainingController extends ControllerBase {
    public function render() {
      return [
        '#markup' => 'Hello world \O/',
      ];
    }
  }

.. code-block:: yaml

  training.page:
    path: '/controller'
    defaults:
      _controller: \Drupal\training\Controller\TrainingController::render
      _title: 'A simple page'
    requirements:
      _permission: 'access content'


La gestion des arguments
========================

Les méthodes d'un controller peuvent prendre, en argument, les paramètres des routes
qui leurs sont associées.

* les paramètres de la route et les arguments du controller doivent être déclarés
  dans le même ordre, avec la même nomenclature ;
* les paramètres tel que ``{node}`` ou ``{user}`` peuvent être chargés dans le
  controller en les typant des bonnes classes ;

.. code-block:: bash

  path: '/page/{foo}'              # Route configuration.
  public function render($foo) {}  # Controller configuration.


  path: '/page/{node}'                                         # Route configuration.
  options:
    parameters:
      node1:
        type: entity:node
  public function render(\Drupal\node\NodeInterface $node) {}  # Controller configuration.


  path: '/page/{user}'                                                    # Route configuration.
  options:
  parameters:
    user:
      type: entity:user
  public function render(\Drupal\Core\Account\AccountInterface $user) {}   # Controller configuration.

La classe ControllerBase
========================

`Cette classe de base`_ donne accès à un certain nombre de méthodes utilitaires
courantes et au conteneur d'injection de services, permettant de réduire considérablement
toute la partie de code relative à la gestion de dépendance.

.. _Cette classe de base: https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Controller!ControllerBase.php/class/ControllerBase

.. code-block:: php

  // Current user entity.
  $current_user = $this->currentUser();

  // EntityTypeManager.
  // Entity management : load, query, render ...
  $this->entityTypeManager()->getStorage('[entity_machine_name]');

  $node = $this->entityTypeManager()->getStorage('node')->load(1);
  $uid = $this->entityTypeManager()->getStorage('user')->getQuery()
    ->condition('uid', 1, '=')
    ->execute();

  // FormBuiler & entityFormBuilder.
  $this->formBuilder();
  $this->entityFormBuilder();

  // Manage redirection.
  $this->redirect();

  // Set message flash.
  $this->messenger()->addStatus($this->t('My message'));

  // Translation.
  $this->t('String to translate')

  // Logger.
  $this->getLogger('My channel')->notice($this->t('My message'));

  // Manage configuration files.
  $config = $this->config('configuration_file.name');


La gestion du render
====================

Un controller peut renvoyer plusieurs types de réponses :

* une données,
* du code html,
* une redirection,
* `une réponse json`_,
* ...

.. _une réponse json: https://www.drupal.org/docs/drupal-apis/responses/responses-overview

Le `render array`_ sont les éléments constitutifs du composant à rendre - les
controllers, mais également les blocks, les formulaires... C'est un tableau
associatif, conforme aux normes et la structure de données attendus pour le
traitement du rendu.

.. _render array: https://www.drupal.org/docs/drupal-apis/render-api/render-arrays

:underline:`Les principales clés du render array`

+----------------------------+------------------------------------------------------------------------+
| Clé                        | Traitement                                                             |
+============================+========================================================================+
| #markup                    | HTML simple                                                            |
+----------------------------+------------------------------------------------------------------------+
| #theme                     | Thème à utiliser pour mettre en forme les données qui lui sont passées |
+----------------------------+------------------------------------------------------------------------+
| #type                      | Valeur correspondant à un RenderElement (cf. formulaire)               |
+----------------------------+------------------------------------------------------------------------+
| #prefix / #suffix          | Markup HTML à afficher avant / après le rendu des données              |
+----------------------------+------------------------------------------------------------------------+
| #cache                     | Configuration du cache                                                 |
+----------------------------+------------------------------------------------------------------------+
| #pre_render / #post_render | Tableaux de fonctions exécutée avant / après le rendu des données      |
+----------------------------+------------------------------------------------------------------------+

Drupal permet d'utiliser plusieurs `thèmes utilitaires`_ comme des tableaux ou
des listes.

.. _thèmes utilitaires: https://api.drupal.org/api/drupal/core%21includes%21theme.inc/function/drupal_common_theme

Si vous souhaitez aller plus loin dans vos connaissances sur les paramètres du
render array, vous pouvez consulter les deux documentations suivantes :

* `Render API`_
* `Liste des paramètres du render array`_

.. _Render API: https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Render!theme.api.php/group/theme_render/
.. _Liste des paramètres du render array: https://www.drupal.org/docs/8/api/render-api/render-arrays#properties


:underline:`Exemple d'implémentation :`

.. tabs::

  .. tab:: Render array dans un composant

    Exemple de retour, dans un controller ou un block.

    .. code-block:: php

      return [
        '#markup' => 'Hello world \O/',
        '#prefix' => '',
        '#suffix' => '',
        '#cache' => '',
        '#pre_render' => '',
        '#post_render' => '',
      ];

  .. tab:: Pour un champs de formulaire

    Les champs de formulaires ont une structure en render_array(), les clés
    représentant les propriétés de l'input.

    .. code-block:: php

      $form['input_text'] = [
        '#type' => 'textfield',
        '#title' => $this->t('My object'),
        '#description' => $this->t('The description of the input'),
        '#size' => 60,
        '#maxlength' => 128,
      ];

  .. tab:: Rendre un thème

    Il faut renseigner dans notre render_array les clés, définie dans le ``hook_theme()``
    et attendu par le template.

    .. code-block:: php

       return [
        '#theme' => 'item_list',
        '#items' => [],
        '#title' => '',
        '#list_type' => 'ol',
      ];
