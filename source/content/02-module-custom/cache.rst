.. |br| raw:: html

  <br />

.. role:: red
.. role:: underline


*****************************
Focus sur la gestion du cache
*****************************


.. admonition:: Documentation
  :class: Note

  `Cache API`_

.. _Cache API: https://www.drupal.org/docs/8/api/cache-api/cache-api

Drupal dispose d’un système de cache très puissant qui permet d’améliorer de manière
significative les performances générales de votre système. Contrepartie, pendant
la période de développement, cela peut s’avérer particulièrement gênant.

La configuration en mode développement - désactivation du cache - permet d'apprécier
plus rapidement les résultats de nos développements. Contrepartie, la gestion
du cache n'est jamais testée... Et c'est parfois un peu fastidieux de tester le
cache 🤯.

La gestion du cache repose sur trois types de métadonnées :

* `Cache tags`_, relatif à la dépendance à la données,
* `Cache contexts`_, relatif à la dépendance à un contexte,
* `Cache max-age`_, relatif à la dépendance au temps ;

.. _Cache tags: https://www.drupal.org/docs/drupal-apis/cache-api/cache-tags
.. _Cache contexts: https://www.drupal.org/docs/drupal-apis/cache-api/cache-contexts
.. _Cache max-age: https://www.drupal.org/docs/drupal-apis/cache-api/cache-max-age


Les différents niveaux de cache
===============================

:underline:`Au niveau de l'application :`

  C'est la gestion du cache qu'opère le core de Drupal - que les modules spécifiques
  au cache soient activés ou non. Il inclut :

  * les informations liés aux thèmes - render cache : templates utilisés, les fichiers css & js,
  * des informations structurelles : entités, plugin, implémentation de hook...

  .. admonition:: Noter
    :class: Tip

    Même en mode développement, il est nécessaire de vider les caches à chaque
    nouveau template ou à chaque nouveau hook implémenté.

  Les seuls raffinements que l'on peut apporter à cette gestion sont la mise en
  place de systèmes tels que `MemCache`_ ou `Redis`_ pour déporter ce stockage en
  mémoire.

.. _MemCache: http://memcached.org/
.. _Redis: https://redis.io/

:underline:`Au niveau des composants :`

  C'est la gestion du cache opérée sur des composants tels que des blocs, les vues...
  et ce quelque soit le statut - anonyme, connecté - de l'utilisateur. Les éléments
  d'une page dont le cache est personnalisé sont exclus du cache global de la page
  et peuvent être réutilisés tel quel ou recalculés.

:underline:`Au niveau de la page :`

  C'est le cache principal de Drupal, capable de servir, aux utilisateurs anonymes,
  une copie HTML de la page à rendre. Ce niveau de cache n'est plus accessible
  lorsque l'utilisateur est connecté.

:underline:`La mise en cache HTTP :`

  Elle est configurable en back-office.

  .. figure:: _static/cache_conf_BO.png
    :align: center
    :width: 100%


Anonymous vs authenticated
==========================

* utilisateur anonyme : un cache complet des pages,
* utilisateur authentifié : un gestion du cache par composant.

.. figure:: _static/cache_behaviour.png
  :align: center
  :width: 80%


Les modules du core
===================

Nativement, deux modules implémentent les fonctionnalités liées au cache :

* `Internal Page Cache`_ : la gestion du cache des pages pour les utilisateurs anonymes,
* `Dynamic Page Cache`_ : la gestion du cache de composants spécifiques, pour
  tous les types d'utilisateurs.

.. _Internal Page Cache: https://www.drupal.org/docs/administering-a-drupal-site/internal-page-cache
.. _Dynamic Page Cache: https://www.drupal.org/docs/8/core/modules/dynamic-page-cache/overview

Les données de cache sont fractionnées et stockées dans plusieurs tables ``cache_*``.
Ces données sont recalculables indépendamment les unes des autres et ce fonction
du contexte.

.. figure:: _static/cache_tables.png
  :align: center
  :width: 70%


La cache API
============

La `cache API`_ permet de configurer le cache des composants personnalisés du site.
Elle repose sur trois types de métadonnées qui permettent de définir les conditions
pour lesquelles le cache doit être invalidé.

:underline:`Cache Tags :`

  `Les tags de cache`_  permettent de définir un niveau de cache en fonction de la
  dépendance entre les données du composant et une ou plusieurs entités. Le cache
  sera alors invalidé si l'une des entités référencées est modifiée.
  Les tags de cache sont utilisés par le module ``Internal Page Cache`` pour
  déterminer les contenus dont le cache doit être recalculé.

  Un tag se déclare à l'aide d'une ``string`` faisant référence à une ou
  plusieurs entités dont dépendent les données du composant - ``tag`` ou ``tag:id`` . |br|

  Exemple de tags : user, node, taxonomy_term, node_list, {entity_type}_list, file...

  Ces tags peuvent être affinés avec un identifiant pour accéder à un niveau plus
  fin de configuration.

  **Quelques exemples concrets**

  .. code-block:: console

    user
    node:4
    article_list
    ['user:1', 'node:5']

.. _Les tags de cache: https://www.drupal.org/docs/drupal-apis/cache-api/cache-tags

:underline:`Cache Contexts :`

  `Les contextes de cache`_ permettent de définir un niveau de cache en fonction
  d'un contexte de page : une url, une lague, un utilisateur... Ils sont utilisés
  entre autre par le module ``Dynamic Page Cache`` pour configurer le cache des
  composants du site.

  Un contexte se déclare à l'aide d'une ``string`` faisant référence au contexte
  dans lequel les données du composant sont traitées. |br|

  Exemple de contexte : user, url, route, languages, theme...

  Certains contextes peuvent être affinés pour accéder à un niveau plus fin de
  configuration.

  **Quelques exemples concrets**

  .. code-block:: console

    user.roles
    user.roles:anonymous
    languages
    url.query_args
    ['user.roles:anonymous', 'route']

  .. tip:: À noter !

    La Drupal console permet d'accéder à tous les contextes disponibles :

    .. code-block:: console

      # Root of the Drupal project
      vendor/bin/drupal debug:cache:context

.. _Les contextes de cache: https://www.drupal.org/docs/drupal-apis/cache-api/cache-contexts

:underline:`Cache max-age :`

  `Le cache d’expiration`_ a un fonctionnement analogue à la mise en cache HTTP -
  directive du header ``Cache-Control:max-age``. Il permet de créer un dépendance
  au temps du cache.

  :red:`Avec une limite :` |br|
  Cette configuration de cache n'est pas compatible avec la gestion du cache, opéré
  par le module ``Internal Page Cache`` pour les utilisateurs anonymes.

  Un temps d'expiration se déclare à l'aide d'un entier ``int`` positif, exprimant
  le temps en seconde.

  **Quelques exemples concrets**

  .. code-block:: console

    0
    3600
    Cache::PERMANENT  // Class const.

.. _Le cache d’expiration: https://www.drupal.org/docs/drupal-apis/cache-api/cache-max-age


Définir le cache d'un composant
===============================

:underline:`Dans le render array :`

  .. code-block:: php

    $build['#cache'] = [
      'contexts' => ['languages'],
      'tags' => ['node:' . $nid, 'user:' . $uid],
      'max-age' => 3600,
    ],

:underline:`Dans une classe :`,

  La classe abstraite ``ContextAwarePluginBase``, étendue par la classe `BlockBase`_
  implémente trois fonctions pour déclarer les métadonnées du cache.

.. _BlockBase: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Block%21BlockBase.php/class/BlockBase/

  .. code-block:: php

    public function getCacheContexts();   // set $tags = [];
    public function getCacheTags();       // set $cache_contexts = [];
    public function getCacheMaxAge();     // set $max_age = Cache::PERMANENT;

  Ces méthodes peuvent être surchargées avec vos propres configurations. Utiliser
  les méthodes statiques de la `classe Cache`_ pour ajouter vos métadonnées aux
  valeurs prédéfinies.

  **Exemples d'implémentation**

.. _classe Cache: https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Cache%21Cache.php/class/Cache/

  .. code-block:: php

    use Drupal\Core\Cache\Cache;

    public function getCacheTags() {
      return Cache::mergeTags(parent::getCacheTags(), ['node:' . $nid, 'user:' . $uid]);
    }
    public function getCacheContexts() {
      return Cache::mergeContexts(parent::getCacheContexts(), ['languages']);
    }
    public function getCacheMaxAge() {
      return Cache::mergeMaxAges(parent::getCacheMaxAge(), 3600);
    }


Désactiver le cache
===================

C'est un point abordé dans `la configuration de son environnement de développement`_.
Les caches se paramètrent dans un fichier de configuration local ``settings.local.php``.

.. _la configuration de son environnement de développement: ../00-introduction/dev-mode-configuration.html

.. code-block:: php

  # sites/default/settings.local.php
  $settings['cache']['bins']['render'] = 'cache.backend.null';              // Disable the render cache.
  $settings['cache']['bins']['page'] = 'cache.backend.null';                // Disable Internal Page Cache.
  $settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.null';  // Disable Dynamic Page Cache.

On peut également utiliser la Drupal console.

.. code-block:: bash

    # Root of the Drupal project
    vendor/bin/drupal site:mode dev
    vendor/bin/drupal site:mode prod
