.. # define break line
.. |br| raw:: html

  <br />


*******************************
4 - Les profiles d'installation
*******************************

Concevoir une distribution ?
============================

C'est un profil d'installation Drupal qui installe et pré-configure un système.
On obtient après l'installation, un site prêt à l'emploi. |br|
Cela permet de créer différentes configurations de systèmes.


Comprendre les bases
====================

Lorsque vous installez Drupal pour la première fois, vous devez choisir un profil
d'installation :

* standard - *le profil que nous avons installé* ;
* minimum ;
* `Demo Umami <https://www.drupal.org/docs/8/umami-drupal-8-demonstration-installation-profile>`_ ;

.. figure:: _static/profiles_installations.png
    :align: center

Dans le core, ces profils sont déclarés dans le dossier ``/core/profiles``. |br|
Pour chaque profil, il y a un ``profil.install`` qui définit les tâches à réaliser
à l'installation du système.

Déclarer un profil d'installation
==================================

.. NOTE::
  Il faut se représenter la création d'un profil installation comme étant identique
  à la création d'un module.

Vous déclarez votre profil d'installation dans le dossier profiles.

.. TIP::
  **Bonne pratique** |br|
  Ranger vos profils d'installation dans un dossier custom !


Nous avons donc deux possibilités pour fournir des configurations initiales :

* via les fichiers de configuration placés sous le répertoire config/install ou config/optionnal ;
* via l'implémentation de hook_install() ;
