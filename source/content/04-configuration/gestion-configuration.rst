.. # define break line
.. |br| raw:: html

  <br />


*******************************
1 - Gestion de la configuration
*******************************

1.1 - Documentation
===================

Configuration API |br|
`https://www.drupal.org/docs/8/api/configuration-api <https://www.drupal.org/docs/8/api/configuration-api>`_

Gérer la configuration |br|
`https://www.drupal.org/docs/8/configuration-management/managing-your-sites-configuration <https://www.drupal.org/docs/8/configuration-management/managing-your-sites-configuration>`_


1.2 - Principe de base
======================

La configuration des entités, faites en back-office peut être longue et fastidieuse
et il n'est pas question de le refaire à la main à chaque nouveau déploiement. |br|
La configuration de votre système peut s'extraire sous la forme d'un ensemble de
fichiers yml... et qui peut se réimporter dans un autre système. Vous pouvez ainsi
synchroniser la configuration entre deux instances de votre système.

.. NOTE::
  La synchronisation de configuration ne peut se faire que sur des sites identiques,
  c'est à dire des sites clonés qui possède le même uuid. |br|
  Pour connaître l'uuid de son site : ``drush cget system.site uuid``.

Cette information est stockée dans un fichier de configuration ``system.site``
et stocker en base de données, dans la table ``config``.

.. WARNING::
  Vous ne pourrez pas gérer les configurations entre deux sites qui ne sont pas
  des clones... |br|
  À moins de réécrire l'uuid de l'instance.


1.3 - Configurer son export
===========================

La déclaration du chemin du dossier  d'export se fait de le fichier
``/sites/default/settings.php`` grâce à la variable ``$settings['config_sync_directory']``.

.. NOTE::
  Pour en savoir plus, prenez le temps de lire les commentaires dans le fichiers de settings !

Il est préférable de configurer le dossier de configuration, en dehors du webroot. |br|

.. code-block:: php

  $settings['config_sync_directory'] = '../config/sync';


Pour en savoir plus |br|
`https://www.drupal.org/docs/8/configuration-management/changing-the-storage-location-of-the-sync-directory <https://www.drupal.org/docs/8/configuration-management/changing-the-storage-location-of-the-sync-directory>`_.


1.4 - Exporter sa configuration
===============================

Deux possibilités pour opérer :

* Vous pouvez le faire en back-office, dans le menu d'administration
  ``/admin/config/development/configuration`` |br|
  --> vous téléchargez vos sources ;
* Vous pouvez le faire avec une commande drush ``drush cex -y`` |br|
  --> vos sources sont exportées dans le dossier déclaré en paramètre ;

.. TIP::
  Soyez vigilent lorsque vous travaillez sur une configuration d'entité. |br|
  Penser à tous les aspects de l'entité (champs, mode d'affichage, formulaire contrib,
  image ...) pour ne pas avoir à y revenir.

.. NOTE::
  Gitter le dossier de synchronisation des configurations. |br|
  Il vous permettra d'historiser vos configurations.


1.5 - Importer sa configuration
===============================

De la même manière que pour l'exportation de la configuration, deux possibilité :

* Vous pouvez le faire en back-office ;
* Vous pouvez le faire avec un commande drush ``drush cim -y`` ;


1.6 -  Des outils complémentaires
=================================

drush cim tools |br|
`https://www.drupal.org/project/drush_cmi_tools <https://www.drupal.org/project/drush_cmi_tools>`_
