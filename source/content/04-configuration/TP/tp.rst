**********************************
TP 1 - Gestion de la configuration
**********************************

Consigne
========

* Faire un premier déploiement de votre site sur un serveur de stagin ;
* Faire des modifications en local :
  * création d'un type de node ;
  * création d'un block custom que vous aurez positionné ;
  * implémenter un template ;
  * etc ;
* Répercuter ces modifications dans votre serveur de stagin ;

Guide
=====

* Utiliser au maximum la console... c'est plus rapide et cela va vite devenir un automatisme ;
* N'hésitez pas à vous appuyer sur les exemples de protocole fournis.
