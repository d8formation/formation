**************************
2 - Un exemple de workflow
**************************

2.1 - Initialisation du projet
===============================

Installer Drupal
----------------

* Installer Drupal 8 ;
* Installer les quelques modules dont vous êtes déjà sûr qu'ils vont vous servir : pathauto, metatag, sitemap, ...
* Lancer l'installeur ;
* Créer le dossier pour la synchronisation des configurations ;
* Exporter votre configurations ;
* Exporter la base de données ;

Initialiser le projet
---------------------

* Vérifier la configuration de votre .gitignore ;
* Faites un dump de la base de données ;
* Initialiser votre repo local git ;
* Connecter le à votre remote ;
* Gitter et pousser votre projet ;


Script
------

::

  // Installation de drupal
  cd www
  composer create-project --prefer-dist drupal/recommended-project drupal_project
  cd drupal_project
  composer require drupal/module_name
  composer require drupal/module_name
  mkdir config && cd config && mkdir sync && cd ..

  // après avoir lancer l'installeur
  drush cex -y

  // Dump de la base
  cd www/drupal_project
  drush sql-dump > config/drupal_project_db_bare.sql

  // Workflow git
  cd www/drupal_project
  git init
  git add origin remote add origin https://path/to/my/remote/drupal_project.git
  git status
  git add .
  git commit -m "Init project"
  git push --set-upstream origin master


2.2 - Déploiement du projet sur un serveur
==========================================

Pour un premier déploiement
---------------------------

1. Cloner le projet depuis votre remote ;
2. Récupérer le dossier des files ;
3. Créer / vérifier les informations de ``settings.php`` : base de données, dossier de synchronisation, gestion du ``local.settings.php`` ;
4. Importer votre base de données ou installer la base de données bare.
5. Lancer le composer install ;
6. Vérifier les mises à jour de la base de données ;


Pour appliquer des mises à jour
-------------------------------
1. Faire un git pull ;
2. Si il y a des modifications sur le composer, faire un install ;
3. Si il y des modifications dans la configuration, les importer ;
4. Vérifier les mises à jour de la base de données ;


Script
------

::

  // Premier déploiement
  cd www/
  git clone https://path/to/my/remote/drupal_project.git project_stagin
  cd web/sites/default
  mv default.settings.php settings.php
  // renseigner la base de données, le dossier de synchronisation
  vim settings.php
  cd ../..
  composer install
  drush sqlc < ../config/drupal_project_bare.sql
  drush cr

  //Mise à jour
  cd www/project_stagin
  git pull
  cd web
  composer install
  drush cim
  drush updb
  drush cr
