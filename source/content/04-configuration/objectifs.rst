*********
Objectifs
*********

* Comprendre la gestion de la configuration ;
* Se familiariser avec un workflow de développement ;
* Appréhender les mises à jour de Drupal et des modules communautaires ;
* Comprendre la notion de profil d'installation ;
* TP ;
