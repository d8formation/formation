**************
Drupal console
**************

.. admonition:: Documentation
  :class: Note

  `Toutes les commandes`_

.. _Toutes les commandes: https://drupalconsole.com/docs/en/commands/


Installation
============

Installer la console avec Composer, au lancement de votre projet.

.. code-block:: bash

  # Root of the Drupal project
  composer require drupal/console

  # Accéder à l'ensemble des commandes
  vendor/bin/drupal


Lignes de commande utiles
=========================

.. code-block:: bash

  # Documentation d'une commande particulière
  vendor/bin/drupal [command_name] -h

  # Mettre le site en mode développement / production
  vendor/bin/drupal site:mode dev
  vendor/bin/drupal site:mode prod

  # Création de contenus de test
  vendor/bin/drupal create:nodes --limit=10 page
  vendor/bin/drupal create:users --limit=10 --password="password" [<roles>...]
