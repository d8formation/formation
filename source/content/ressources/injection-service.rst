*****************************
Snippet injection de services
*****************************

.. code-block:: php

  \Drupal::currentUser()
  /** @var \Drupal\Core\Session\AccountProxy **/
  protected $currentUser;
  $container->get('current_user'),
  $this->currentUser = $current_user;


  \Drupal::entityTypeManager()
  /** @var \Drupal\Core\Entity\EntityTypeManagerInterface **/
  protected $entityTypeManager;
  $container->get('entity_type.manager'),
  $this->entityTypeManager = $entity_type_manager;


  \Drupal::formBuilder()
  /** @var \Drupal\Core\Form\FormBuilderInterface **/
  protected $formBuilder;
  $container->get('form_builder'),
  $this->formBuilder = $form_builder;


  \Drupal::routeMatch()
  /** @var \Drupal\Core\Routing\RouteMatchInterface **/
  protected $routeMatch;
  $container->get('current_route_match'),
  $this->routeMatch = $route_match;


  \Drupal::request()
  /** @var \Symfony\Component\HttpFoundation\RequestStack **/
  protected $requestManager;
  $container->get('request_stack'),
  $this->requestManager = $request_manager;


  \Drupal::urlGenerator()
  /** @var \Drupal\Core\Routing\UrlGeneratorInterface **/
  protected $urlGenerator;
  $container->get('url_generator'),
  $this->urlGenerator = $url_generator;


  \Drupal::linkGenerator()
  /** @var \Drupal\Core\Utility\LinkGeneratorInterface **/
  protected $linkGenerator;
  $container->get('link_generator'),
  $this->linkGenerator = $link_generator;


  \Drupal::configFactory()
  /** @var \Drupal\Core\Config\ConfigFactory **/
  protected $config;
  $container->get('config.factory'),
  $this->config = $config;


  \Drupal::database()
  /** @var \Drupal\Core\Database\Driver\mysql\Connection **/
  protected $database;
  $container->get('database'),
  $this->database = $database;


  /** @var \Drupal\Core\TempStore\PrivateTempStoreFactory; **/
  protected $tempStoreFactory;
  /**  @var \Drupal\user\PrivateTempStore **/
  protected $store;
  $container->get('tempstore.private'),
  $this->tempStoreFactory = $temp_store_factory;
  $this->store = $this->tempStoreFactory->get('[key]');


  \Drupal::translation()
  /** @var \Drupal\Core\StringTranslation\TranslationInterface **/
  protected $stringTranslation;
  $container->get('string_translation'),
  $this->stringTranslation = $string_translation;
  use StringTranslationTrait;


  \Drupal::languageManager()
  /** @var \Drupal\Core\Language\LanguageManager **/
  protected $languageManager;
  $container->get('language_manager'),
  $this->languageManager = $language_manager;


  \Drupal::service('date.formatter')
  /** @var \Drupal\Core\Datetime\DateFormatter **/
  protected $dateFormatter;
  $container->get('date.formatter'),
  $this->dateFormatter = $date_formatter;


  \Drupal::service('plugin.manager.mail')
  /** @var \Drupal\Core\Mail\MailManagerInterface **/
  protected $mailManager;
  $container->get('plugin.manager.mail'),
  $this->mailManager = $mail_manager;


  \Drupal::service('event_dispatcher')
  /** @var \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher **/
  protected $eventDispatcher;
  $container->get('event_dispatcher'),
  $this->eventDispatcher = $event_dispatcher;


  \Drupal::service('renderer')
  /** @var \Drupal\Core\Render\RendererInterface **/
  protected $renderer;
  $container->get('renderer'),
  $this->renderer = $renderer;


  \Drupal::service('twig')
  /** @var \Drupal\Core\Template\TwigEnvironment **/
  protected $twig;
  $container->get('twig'),
  $this->twig = $twig;


  \Drupal::service('http_client_factory')
  /** @var \Drupal\Core\Http\ClientFactory **/
  protected $httpClientFactory;
  $container->get('http_client_factory'),
  $this->httpClientFactory = $http_client_factory;
  $client = $this->httpClientFactory->fromOptions([
    'base_uri' => 'https://www.thesportsdb.com/api/v1/json/1/',
  ]);
  $response = $client->get('eventsnextleague.php', [
    'query' => [
      'id' => 4334,
    ]
  ]);


  \Drupal::httpClient();
  /** @var \GuzzleHttp\Client **/
  protected $httpClient;
  $container->get('http_client')
  $this->httpClient = $http_client;
  $response = $this->httpClient
    ->request('GET', 'https://www.thesportsdb.com/api/v1/json/1/eventsnextleague.php', [
        'query' => ['id' => 4334]
      ]);
