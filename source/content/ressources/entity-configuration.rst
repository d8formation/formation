.. |br| raw:: html

  <br />

.. role:: underline


**************************
Configuration d'une entité
**************************

:underline:`Configuration des champs`

✅ Être attentif à la nomenclature des champs utilisés. |br|
✅ Être attentif à la configuration des champs utilisés : champs requis, cardinalité. |br|

Propre les champs images :

✅ J'ai correctement défini la source des téléchargements. |br|
✅ J'ai correctement configuré les conditions d'upload. |br|
✅ J'ai configuré le widget de rendu dans les modes d'affichage de mes entités. |br|
✅ J'ai défini mes styles d'images. |br|


:underline:`Configuration du formulaire de contribution`

✅ Être attentif à la logique de saisie du formulaire. |br|
✅ Pour les utilisateurs - j'ai configuré mon formulaire de création de compte. |br|
✅ Être attentif à la configuration des widgets utilisé pour saisir les champs. |br|


:underline:`Configuration des modes d'affichages`

✅ J'ai configuré tous mes modes d'affichages. |br|
✅ J'ai correctement configuré la gestion du label des champs. |br|
✅ J'ai correctement configuré les widgets qui vont rendre les champs. |br|

Propre aux champs de type Référence :

✅ J'ai configuré le mode de rendu de l'entité référencée. |br|


:underline:`Configuration de l'entité`

✅ Définir un pathauto - si besoin. |br|
✅ Définir un positionnement de bloc - si besoin. |br|
✅ Ajuster les permissions pour les utilisateurs - si besoin. |br|


:underline:`Workflow git`

✅ J'exporte mes configurations. |br|
✅ Je gitte la configuration dans un commit dédié. |br|
