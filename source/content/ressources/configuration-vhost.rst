*************************************
Linux : configurer des virtuals hosts
*************************************

Les étapes de la configuration
==============================

1. Déclarer votre serveur dans le fichier hosts ``/etc/hosts``
2. Configurer votre serveur virtuel en créant le fichier ``/etc/apache2/sites-available/training.conf`` ;
3. Activer le site et démarrer apache ;

.. NOTE::
  `Une ressource pour configurer un serveur Wamp`_

.. _Une ressource pour configurer un serveur Wamp: http://www.nicolas-verhoye.com/comment-configurer-virtual-hosts-wamp.html

Script
======

.. code-block:: bash

  # Place source here
  sudo mkdir /var/www/training

  # Déclaration du serveur local
  vim /etc/hosts

  # Virtualhost declaration
  127.0.0.100 training.local

  # Virtualhost configuration
  cd /etc/apache2/sites-available/
  sudo touch training.conf
  sudo vim training.conf

  # Report this config
  <VirtualHost 127.0.0.100:80>
      ServerAdmin webmaster@training.local
      ServerName training.local
      ServerAlias training.local
      DocumentRoot /var/www/training/web
      ErrorLog ${APACHE_LOG_DIR}/training.local.error.log
      CustomLog ${APACHE_LOG_DIR}/training.local.access.log combined
  </VirtualHost>
  <Directory /var/www/training/web>
    RewriteEngine on
    RewriteBase /
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteRule ^(.*)$ index.php?q=$1 [L,QSA]
  </Directory>
  <Directory /var/www/training>
   AllowOverride All
  </Directory>

  # Allumer les serveurs
  sudo systemctl start apache2
  sudo systemctl start mysql


Lignes de commande utiles
=========================

Apache
------

* Pour lancer apache : ``sudo systemctl start apache2`` ;
* Pour arrêter apache : ``sudo systemctl stop apache2`` ;
* Pour relancer apache : ``sudo systemctl restart apache2`` ;
* Pour recharger la configuration d'apache : ``sudo systemctl reload apache2`` ;
* Pour voir la version d'apache utilisée : ``sudo apache2ctl -v`` ;
* Pour tester l'ensemble de la configuration d'apache : ``sudo apache2ctl -t`` ;
* Pour tester la configuration des hôtes virtuels : ``sudo apache2ctl -t -D DUMP_VHOSTS`` ;
* Pour voir les modules d'apache chargés : ``sudo apache2ctl -M`` ;
* Pour activer un site  : ``sudo a2ensite [training.conf]``
* Pour désactiver un site  : ``sudo a2dissite [training.conf]``

Mysql
-----

* Démarrage : ``sudo systemctl start mysql`` ;
* Redémarrage : ``sudo systemctl restart mysql`` ;
* Arrêt : ``sudo systemctl stop mysql`` ;
* Rechargement de la configuration : ``sudo systemctl reload mysql`` ;
* Connaître la version : ``mysql --version`` ;
* Se connecter : ``mysql -u root -p`` ;
* Pour exporter la base de donnée [database] : ``sudo mysqldump -u root -p database > database_backup.sql`` ;
* Pour exporter une table [table] appartenant à [database] : ``sudo mysqldump -u root -p database table > table.sql`` ;
* Pour importer une base de données dans [database]  : ``sudo mysql -u root -p database < backup.sql`` ;

php
---

* Connaître la version de php : ``php -v`` ;
* Changer de configuration : ``sudo update-alternatives --config php`` ;


Documentation
=============

* `Apache 2`_ ;
* `MySql`_ ;

.. _Apache 2: https://doc.ubuntu-fr.org/apache2
.. _MySql: https://doc.ubuntu-fr.org/mysql
