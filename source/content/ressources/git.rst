**************
Gestion du git
**************

Workflow git
============
Le repo git est à initier à la racine du projet.

:underline:`Gitter`

* les modules, thèmes et profiles personnalisés,
* le dossier de synchronisation des configuration.

:underline:`Ne pas gitter`

* ne pas gitter le vendor,
* ne pas gitter le core,
* ne pas gitter les modules, thèmes et profiles communautaires,
* ne pas gitter les fichiers de configuration : ``settings.php`` ou ``.env``,
* ne pas gitter le dossiers des média,s aussi bien publics$ que privés.

.. code-block:: bash

  # Root of the Drupal project
  git init
  git add .
  git commit -m "Initial commit."
  git remote add origin [my_repositoy_address.git]
  git push -u origin master


Template gitignore
==================

.. code-block:: bash

  # Ignore core and community's extension
  web/modules/contrib/
  web/themes/contrib/
  web/profiles/contrib/
  web/libraries/

  # Ignore dependencies that are managed with Composer.
  vendor/

  # Ignore configuration files that may contain sensitive information.
  web/sites/*/settings*.php
  web/sites/*/services*.yml

  # Ignore paths that contain user-generated content.
  web/sites/*/files/
  web/sites/*/private/

  # Ignore SimpleTest multi-site environment.
  web/sites/simpletest
