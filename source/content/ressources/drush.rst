*****
Drush
*****

.. admonition:: Documentation
  :class: Note

  `Toutes les commandes`_

.. _Toutes les commandes: https://drushcommands.com/


Installation
============

Installer Drush avec Composer, au lancement de votre projet.

.. code-block:: bash

  # Root of the Drupal project
  composer require drush/drush

  # Accéder à l'ensemble des commandes
  vendor/bin/drush


Lignes de commande utiles
=========================

.. code-block:: bash

  # Documentation générale
  vendor/bin/drush

  # Documentation d'une commande particulière
  vendor/bin/drush [command_name] -h

  # Vider les caches
  vendor/bin/drush cr

  # Activer un module
  vendor/bin/drush en -y [module_name]

  # Activer un theme
  vendor/bin/drush then -y [theme_name]

  # Désinstaller un module
  vendor/bin/drush pmu [module_name]

  # Mettre à jour la base de données
  vendor/bin/drush updb

  # Lancer les crons
  vendor/bin/drush cron

  # Se connecter avec un lien de connexion unique
  vendor/bin/drush uli [uid]

  # Accèder au log
  vendor/bin/drush ws

  # Faire un dump de la base de données
  vendor/bin/drush sql:dump --gzip --result-file=[filename]

  # Exporter / Importer les configurations
  vendor/bin/drush cex
  vendor/bin/drush cim
