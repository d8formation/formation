.. |br| raw:: html

  <br />

.. role:: underline


************
La taxonomie
************


La taxonomie permet de référencer les contenus du site. Ce seront les tags d'un
billet de blog, la catégorie d'un produit, la thématique d'une formation...

Elle s'articule autour de deux grands concepts :

* les vocabulaires : les catégories de tags,
* les termes : les tags en eux-même.

.. figure:: _static/taxo_administration.png
  :align: center
  :width: 100%

Notions fondamentales
=====================

La taxonomie se structure autour de deux grands concepts :

:underline:`Vocabulaires`

  Un vocabulaire est une collection d'éléments qui constitue une catégorie utilisée
  pour référencer les contenus. Conceptuellement, un vocabulaire est une entité
  et il en possède toutes les caractéristiques de configuration :

  * des champs, pour définir ses propriétés,
  * un formulaire de contribution à configurer,
  * des modes d'affichage pour le rendu front.

  .. figure:: _static/taxo_entite.png
    :align: center
    :width: 100%

:underline:`Termes` :

  Ce sont les éléments d'un vocabulaire. Conceptuellement, un terme est une
  contribution pour l'entité vocabulaire correspondante.

  Chaque terme de taxonomie possède déjà sa propre vue, agrégeant tous les contenus
  qui y font référence.  Elle est accessible par l'url générique ``terms/{tid}``,
  qui peut être personnalisée par un alias.


Utiliser la taxonomie
=====================

La taxonomie s'utilise pour référencer les contenus - les entités - du site.
Le lien entre l'entité et le / les vocabulaires se fait par le biais d'un champ -
de type Reference - qui sera renseigné par le contributeur.

.. figure:: _static/taxo_field_ref.png
  :align: center
  :width: 100%
