.. role:: underline
.. role:: red


***********************************
🥤 - Site est sport, t'es sirosport
***********************************

⏱️ | 1 heure.

!!!!

Nous allons construire un site internet pour une association sportive. Ce sera
le fil rouge de tous les exercices qui seront proposés tout au long de cette
formation.

Nous allons réaliser ce TP en deux temps :

* Un temps d'analyse ou vous réfléchirez à la conception du site ;
* Après une mise en commun de vos réflexions, nous passerons à la réalisation.

!!!!

Pour la réalisation de ce site, plusieurs spécifications seront à considérer :

:underline:`Le contenu éditorial :`

  * page institutionnelles : Présentation de l'association, mentions légales, rgpd ...,
  * des articles : En lien avec les sports proposés par l'association,
  * des activités sportives.


:underline:`Le contenu spécifique`

  **Les sports**

    Les sports seront décrits avec les informations suivantes :

    * une photo,
    * une description,
    * un coach :red:`*`,
    * une adresse - gymnase.


  **Les activités sportives**

    L'association propose des activités variées dans les différents sports qu'elles
    animent, pour les différentes catégories d'âges accueillis. Chaque activité
    sera décrite par quelques informations, comme :

    * une photo,
    * un sport de référence :red:`*`,
    * une tranche d'âges :red:`*`,
    * une description :red:`*`,
    * un horaire :red:`*`,
    * un tarif :red:`*`,
    * les formalités nécessaires : certificat médicale, licence, autorisation
      parentale...


:underline:`Les comptes utilisateurs`

  Outre les gestionnaires et les bénévoles de l'association, ce seront surtout
  les personnes inscrites aux activités sportives proposées. On considérera
  quelques informations importantes comme :

  * une catégorie de compte : coach, gestionnaire, usager :red:`*` ;
  * un téléphone ;
  * une adresse ;
  * l'inscription à la newsletter :red:`*` ;
  * la classe d'âges : poussin, benjamin, cadet...  :red:`*` ;
  * une / plusieurs activité.s sportive.s :red:`*` ;
  * une cotisation à acquitter :red:`*` ;

  Une partie de ces informations pourront être modifiées par l'utilisateur
  lui-même.
