.. |br| raw:: html

  <br />

.. role:: underline
.. role:: red


***************************
🏅 - Éléments de correction
***************************


La création des types de contenus / entités
===========================================

✅ Être attentif à la nomenclature des champs utilisés. |br|
✅ Être attentif à la configuration du formulaire de contribution. |br|
✅ Être attentif à la configuration des modes d'affichage. |br|
✅ Définir un pathauto - si besoin. |br|
✅ Ajuster les permissions pour les utilisateurs. |br|


La taxonomie
============

:underline:`Vocabulaire sport :`

  Un vocabulaire pour les sports, enrichi par les champs qui doivent décrire une
  activité sportive :

  * un champ adresse - gymnase,
  * un champ image,
  * un champ coach, de type référence ``User`` :red:`*`.

:underline:`Vocabulaire classe d'âge :`

  La configuration du vocabulaire par défaut suffit.


Les types de contenus
=====================

:underline:`Les pages de bases :`

  La configuration par défaut du type ``Page`` est suffisant. Vous pouvez éventuellement
  rajouter un champ, type image, type image à la une, à utiliser dans votre thème.

:underline:`Les articles de blog :`

  Il faut enrichir le type de contenus ``Article`` avec, à minima un champ référence
  à la taxonomie sport - *Préférer supprimer le champ implémenté par défaut, et
  créer un nouveau champ avec un nom signifiant.*

  Vous pouvez également ajouter un champ, type image, type image à la une, à
  utiliser dans votre thème.

:underline:`Les activités sportives :`

  Créer un nouveau type de contenus spécifique et ajouter les champs nécessaires :

  * un champ description - Body, créé automatiquement,
  * un champ image,
  * un champ date range,
  * un champs number pour le prix de l'activité :red:`*`,
  * deux champs reference taxonomies : sport, classe d'âge :red:`*`.

  Le champ **Date range** - module natif du core à activer - permet d'éviter
  d'implémenter deux champs date !


Le compte utilisateur
=====================

:underline:`Les rôles :`

  Créer deux rôles ``Coach`` pour les animateurs sportifs et ``Manager``
  pour les responsables administratifs de l'association.
  Nous utiliserons le rôle ``Authenticated`` pour les usagers des activités.

:underline:`Configuration du compte :`

  Ajouter les champs nécessaire à la gestion des utilisateur :

  * un champ image,
  * un champ date,
  * un champ adresse,
  * un champ téléphone,
  * un champ taxonomie classe d’âges :red:`*`,
  * un champ activités sportives :red:`*`, de type référence ``Content → activity``,
  * un champ number pour la cotisation à acquitter :red:`*`,
  * un champ booléen pour l'inscription à la newsletter :red:`*`.

Permissions
===========

:underline:`Sur la gestion des types de contenus :`

.. figure:: ../_static/TP_permissions_node.png
  :align: center
  :width: 100%


:underline:`Sur la gestion des taxonomies :`

.. figure:: ../_static/TP_permissions_taxo.png
  :align: center
  :width: 100%

:underline:`Sur la gestion du thème d'administration :`

.. figure:: ../_static/TP_permissions_system.png
  :align: center
  :width: 100%

:underline:`Sur la gestion de la toolbar d’administration :`

.. figure:: ../_static/TP_permissions_toolbar.png
  :align: center
  :width: 100%

Utilisation du module devel generate
====================================

Ce module permet de générer du contenu factice pour tous vos types de contenus :
node, utilisateurs, vocabulaires, termes de taxonomie.

* pour créer 12 articles : ``drush genc 12 --types article``,
* pour créer 50 utilisateurs : ``drush genu 50``.

Si vous modifiez la configuration de vos entités, le contenu de test peut être
supprimé et régénéré avec l'option ``--kill``

:underline:`En savoir plus :` `Drush devel generate <https://drushcommands.com/drush-8x/devel-generate/>`_
