*********
Objectifs
*********

À partir des notions abordées, vous serez capable de :

* utiliser le back-office de Drupal,
* décrire les grands principes de fonctionnement des entités,
* construire votre structure de données,
* configurer vos types de contenus,
* créer vos types de contenus,
* afficher des blocs dans les régions de votre site,
