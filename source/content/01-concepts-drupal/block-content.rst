.. |br| raw:: html

  <br />


*********
Les blocs
*********


À la différence des nodes, les blocs sont des *plugins*, c'est à dire des petits
éléments indépendants, interchangeables, positionables dans le layout des régions
du thème. Leur principal objet des blocs est d'enrichir la mise en page du site
avec des informations en lien avec à la page consultée.

.. figure:: _static/block_adm.png
    :width: 100%
    :align: center


Différents types de blocs
=========================

Les blocs sont des éléments à disposer dans le layout des régions de vos thèmes.
Ils sont configurables pour conditionner leur affichage. Il faut distinguer plusieurs
types de blocs :

* les blocs spécifiques fournis par des modules : bloc aide, bloc de connexion...,
* les blocs de menu,
* les blocs de vue,
* les blocs personnalisés, à savoir des blocs de type entité,
* les blocs provenant d'un module personnel.


Les blocs personnalisés
=======================

Un bloc personnalisé est un type d'entité à part entière. Il est possible de
créer ses propres gabarits, avec les éléments de configuration communs à toutes
les entités :

* les champs, pour définir les propriétés du bloc,
* le formulaire de contribution à configurer,
* des modes de vues.

Il est possible d'en instancier autant que nécessaire et de les positionner dans
le layout des régions d'un thème.

.. figure:: _static/block_entite.png
    :align: center
    :width: 100%


Le layout du blocs
==================

C'est le découpage de votre page et il est propre à chaque thème. Un aperçu de
ce layout est disponible dans une interface qui affiche le rendu de la structure
des régions dans une page.

Il y a autant de layouts différents qu'il n'y a de thèmes installés. Une illustration
très concrète : le découpage des régions et la composition des blocs sont différentes
entre le layout du front-office et le layout du back-office.

.. figure:: _static/theme_block_layout.png
  :align: center
  :width: 100%


Configuration des blocs
=======================

Cette configuration se fait à la déclaration d'un bloc. Plusieurs paramètres
permettent de conditionner son affichage :

* par page : on déclare une ou plusieurs url(s) sur lesquelles afficher ou non le bloc,
* par type de contenu : le bloc n’apparaîtra que sur les pages spécifiques au
  type de contenu choisi,
* par rôle : celui de l'utilisateur courant, qu'il soit connecté ou non.

.. figure:: _static/block_configuration.png
    :align: center
    :width: 75%
