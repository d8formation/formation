.. |br| raw:: html

  <br />


*********
Les nodes
*********


Les nodes sont des entités spécifiques qui portent le contenu principal du site.
Dans une installation standard, Drupal implémente deux types de nodes : les pages
et les articles.

Ces deux types de contenus différent par leurs configurations et les propriétés
qui les définissent.

.. figure:: _static/node_content_type.png
  :align: center
  :width: 100%

Créer ses types de contenus
===========================

Il est possible de créer ses propres structures de données pour les informations
spécifiques que le site expose. |br|
*Ex :  événement, offre promotionnelle, formation, activité...*.

Les principaux paramètres de configuration sont les mêmes que pour toutes les
autres entités du système :

* manage fields : les champs qui vont décrire l'entité,
* manage form display : la configuration du formulaire de contribution,
* manage display : les différents modes de rendu.

D'autres paramètres permettent de configurer plus finement votre node :

* les paramètres de contribution : libellé du titre, option de prévisualisation,
* les options de publication : statut, révisions...,
* les paramètres de langue : pour les sites multilingues,
* les paramètres d'affichage des informations de l'auteur,
* les paramètres de menu.

.. figure:: _static/node_configuration.png
  :align: center
  :width: 100%

Contribuer
==========

Deux aspects à prendre en compte lors de la contribution :

* le contenu : en saisissant les informations attendues,
* les options de publication : révision, menu, alias url, promotion...

.. figure:: _static/node_create_content.png
  :align: center
  :width: 100%

Gestion des contenus
====================

Les contenus du site sont administrables, en back-office, au travers d'une interface
dédiée qui permet de :

* filtrer les contenus avec à un moteur de recherche,
* modifier / supprimer les contenus,
* réaliser des actions en lot : dépublication, suppression...,
* traduire les contenus,
* gérer les révisions et les modérations.

Cette interface est une vue. Elle est donc paramètrables.

.. figure:: _static/node_content_administration.png
  :align: center
  :width: 100%
