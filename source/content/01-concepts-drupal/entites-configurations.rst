.. |br| raw:: html

  <br />

.. role:: red
.. role:: underline


****************************
La configuration des entités
****************************


Une logique commune de configuration
====================================

Les entités sont entièrement configurables en back-office. Quelque que soit le type
d'entité considéré, la logique de configuration est la même.

:underline:`Trois niveaux de configuration :`

* manage fields : les champs qui vont décrire l'entité,
* manage form display : la configuration du formulaire de contribution,
* manage display : les différents modes de rendu.

.. figure:: _static/entite_configuration.png
  :align: center
  :width: 100%

Les fields
==========

Ce sont les types de données qui vont décrire votre entité. Beaucoup de champs
sont disponibles nativement et il est possible de créer ses propres champs.

* champs génériques : boolean, date, mail...,
* champs textes : texte brute, texte formaté, liste de textes...,
* champs nombres : entier, réel, liste de nombres...,
* champs références : utilisateur, contenu, taxonomie...

Dans la configuration d'une entité, un field possède :

* un libellé personnalisable,
* un nom machine,
* un type,
* une configuration pour la contribution et l'affichage.

.. figure:: _static/entite_manage_field.png
  :align: center
  :width: 100%

Un field est défini pour un type d'entité et peut être réutilisé. Il est important
de ne pas multiplier les fields et de les réutiliser au maximum, ce qui implique
d'être attentif à son nom machine et à sa configuration.

.. figure:: _static/entite_database_field_management.png
  :align: center
  :width: 100%

:red:`Avec une limite :` |br|
Si la configuration d'un field existant doit être modifiée pour une autre entité,
il faudra alors créer un second field - avec une nouvelle configuration.

* la présence de données renseignées empêche la modification de la configuration,
* sans données, la modification de configuration du field se propagerait à toutes
  les entités l'utilisant.

:underline:`Quelques bonnes pratiques :`

Le nom machine du champs est identique dans toutes les entités l'implémentant.
Le libellé lui peut se configurer de manière différente d'une entité à l'autre.

Un nom machine doit être simple et signifiant.

  ::

    field_edition_released_date vs field_released

Définir le nom machine de vos champs en fonction de l'information qu'il porte et
non à son contexte. Ils seront plus facilement réutilisables.

  ::

    field_event_date vs field_date

Si le champs accepte plusieurs valeurs, utiliser le pluriel.

  ::

    field_colors


Le formulaire de contribution
=============================

C'est le formulaire à remplir à la création d'un contenu. Il est important de bien
configurer ce formulaire pour les utilisateurs finaux qui s'en serviront au quotidien.
Plusieurs paramètres sont à prendre en compte :

* la visibilité du champ,
* l'ordre de saisie des champs,
* le widget utilisé,
* la configuration du dit widget.

.. figure:: _static/entite_manage_form_display.png
  :align: center
  :width: 100%

Les différents modes d'affichage
================================

Un mode d'affichage correspond à une manière de rendre le contenu de l'entité.
Plusieurs modes d'affichage peuvent être définis, fonction des mécanismes qui
vont rendre le contenu de l'entité : page, vue, bloc...

Un mode d'affichage se définit par :

* les champs à rendre,
* le rendu du label,
* le format de rendu et sa configuration.

.. figure:: _static/entite_manage_display.png
  :align: center
  :width: 100%

Le stockage de la configuration
===============================

La configuration d'une entité est répartie entre plusieurs fichiers yaml, chacun
contenant une partie des caractéristiques de celle-ci. Prenons l'exemple d'un
node de type ``article`` :

  :underline:`Configuration du type d'entité`

  * node.type.article.yml

  :underline:`Configuration des champs`

  * field.field.node.article.body.yml
  * field.field.node.article.comment.yml
  * field.field.node.article.field_image.yml
  * field.field.node.article.field_sport.yml
  * field.storage.node.body.yml
  * field.storage.node.comment.yml
  * field.storage.node.field_image.yml
  * field.storage.node.field_sport.yml

  :underline:`Configuration du formulaire de saisie`

  * core.entity_form_display.node.article.default.yml

  :underline:`Configuration des modes d'affichage`

  * core.entity_view_display.node.article.default.yml
  * core.entity_view_display.node.article.teaser.yml
  * core.entity_view_display.node.article.rss.yml
