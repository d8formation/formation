.. |br| raw:: html

  <br />


****************
Les utilisateurs
****************


Configuration
=============

Un utilisateur est représenté par une entité, configurée par défaut avec un certain
nombre de champs :

* username - nom d'utilisateur,
* mail,
* mot de passe,
* rôle - ensemble des permissions qui seront données pour interagir avec le système,
* statut du compte.

Et comme toutes les entités abordées précédemment, un utilisateur possède les mêmes
propriétés de configuration :

* des champs, pour l'enrichir,
* un formulaire de contribution à configurer,
* des modes d'affichage à paramétrer,
* une interface pour gérer les contenus - ici les comptes des utilisateurs.


Rôle et permissions
===================

Les possibilités d'interaction entre un utilisateur - connecté ou non - et le
système sont régies par un mécanisme de rôles - attribués à un utilisateur - et
un ensemble de permissions associées.

* la création d'un rôle se fait par simple déclaration,
* le poids ordonne les rôles par ordre de permissivité croissante,
* les permissions sont déclarées par les modules activés,
* les permissions peuvent se créer sur mesure dans un module custom.

.. figure:: _static/user_roles.png
  :align: center
  :width: 100%

.. figure:: _static/user_permissions.png
  :align: center
  :width: 100%

Les modalités de création de compte
===================================

Plusieurs possibilités pour définir les modalités qui régissent la création d'un
compte utilisateur :

* par l'administrateur seul,
* par l'utilisateur lui même, avec une modération par l'administrateur,
* par l'utilisateur lui même, sans modération, avec un système de vérification mail.

.. figure:: _static/user_entite.png
  :align: center
  :width: 100%

Les messages de communication, envoyés automatiquement à chaque action réalisée
sur le compte sont personnalisables : message de bienvenue, suppression de compte,
demande de mot de passe...

.. figure:: _static/user_config_mail.png
  :align: center
  :width: 100%

Gestion des utilisateurs
========================

Une interface d'administration permet de gérer les utilisateurs. Comme pour la
gestion des contenus, c'est une vue. Les informations affichées sur les
utilisateurs, les filtres de recherche disponibles sont des configurations de la
vue par défaut, mais elles sont personnalisables.

.. figure:: _static/user_administration.png
  :align: center
  :width: 100%
