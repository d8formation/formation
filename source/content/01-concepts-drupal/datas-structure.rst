.. |br| raw:: html

  <br />

.. role:: underline


****************************
La structuration des données
****************************


Il faut distinguer deux grandes catégories de données, administrables dans le
back-office de Drupal :

* les données de configuration, à savoir ce qui décrit l'état du système,
* les contenus même du site, c'est à dire les informations exposées aux visiteurs.


Les données de configuration
============================

Ce sont des éléments d'informations, relativement permanents, qui décrivent la
configuration du système mais n'en constituent par le contenu principal. |br|
*Ex: Titre du site, courriel de l'administrateur, paramètre d'affichage, configuration
de module, positionnement des blocs...*

:underline:`Les configurations sont représentées un ensemble de fichiers yaml.`

  Ces données font partie du code source de votre application, ce qui présente
  plusieurs avantages :

  * elles sont versionnables,
  * elles sont modifiables beaucoup plus facilement.

  Un exemple avec les données concernant les informations du site :

  .. code-block:: yaml

    # system.site.yaml
    uuid: b4859f3b-5ac1-42bb-a6cd-e55647d6fc80
    name: 'Drupal - Le site de la formation'
    mail: webmaster@training.local
    slogan: ''
    page:
      403: ''
      404: ''
      front: /node
    admin_compact_mode: false
    weight_select_max: 100
    langcode: fr
    default_langcode: fr
    _core:
      default_config_hash: yTxtFqBHnEWxQswuWvkjE8mKw2t8oKuCL1q8KnfHuGE

:underline:`Ces informations sont stockées dans la base de données.`

  En plus des fichiers yaml - dont le premier objectif est de simplifier les
  problématiques de gestion et de portage des configurations entre plusieurs
  instances - les configurations actives sont également stockées en base de données
  dans la table ``config``. Plusieurs raisons à cela  :

  * performance : lecture plus rapide des données,
  * sécurité : prévient des modifications intempestives,
  * synchronisation : pour la gestion des états de la configuration.

  .. code-block:: console

    # Requête SQL
    desc config;
    select collection, name from config ;
    select CAST(data as CHAR) from config where name = 'system.site' ;

:underline:`Les configurations sont synchronisables entre deux systèmes.`

  Les données de configuration peuvent être exportées / importées entre deux
  instances d'un même système, c'est à dire entre deux clones du site,
  possédant le même uuid.

  Ce mécanisme de synchronisation peut se faire de deux manières :

  1/ En ligne de commande, avec Drush :

  .. code-block:: console

    drush config:export (cex) # Export Drupal configuration to a directory.
    drush config:import (cim) # Import config from a config directory.

  2/ En back-office :

  * En synchronisant une configuration existante, fichier par fichier ;
  * En important / exportant les configurations, par lot ou par fichier ;

  .. figure:: _static/data_synchronisation.png
    :align: center
    :width: 100%

.. admonition:: Pour aller plus loin
  :class: note

  On peut gérer plus finement les configurations, en utilisant le module
  `Configuration split`_

.. _Configuration split: https://www.drupal.org/project/config_split


Le contenu de votre site
========================

Ce sont les informations principales exposées aux visiteurs. |br|
*Ex: Page, articles, événements, blocs de contact ...*

Une entité, c'est la structure de base de n'importe quel type de données dans Drupal.
Une entité peut être déclinée sous plusieurs formes, fonction des caractéristiques
qui la décrivent. Aussi, dans une installation standard de Drupal, on retrouve
plusieurs types d'entités préconfigurées :

+-------------------------+----------------------------------------------+
| Type d’entité           | Usage                                        |
+=========================+==============================================+
| `Node`_                 | Contenus principaux du site                  |
+-------------------------+----------------------------------------------+
| Comment                 | Éléments d'interaction attachés à un contenu |
+-------------------------+----------------------------------------------+
| `Users`_                | Comptes utilisateur                          |
+-------------------------+----------------------------------------------+
| `Block`_                | Éléments de mise en page des informations    |
+-------------------------+----------------------------------------------+
| `Taxonomy`_             | Éléments de classification des contenus      |
+-------------------------+----------------------------------------------+
| `Formulaire`_           | Éléments d'interaction avec le visiteur      |
+-------------------------+----------------------------------------------+
| Fichier                 | Données stockées                             |
+-------------------------+----------------------------------------------+

.. _Node: node.html
.. _Users: user.html
.. _Block: block-content.html#les-blocs-personnalises
.. _Taxonomy: taxonomy.html
.. _Formulaire: form.html

Ces entités sont configurables en back-office et sont stockées de deux manières
distinctes et indépendantes :

  * leur configuration - voir précédemment,
  * leur contenu, dans des tables spécifiques à chaque élément de l'entité.
