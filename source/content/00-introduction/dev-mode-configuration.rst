.. |br| raw:: html

  <br />


*********************
Le mode développement
*********************


Documentation
=============

Drupal dispose d'un système de cache très puissant qui permet d'améliorer de
manière significative les performances générales de votre système. Contrepartie,
pendant la période de développement, cela peut s'avérer particulièrement gênant.

Une solution : `configurer son système pour désactiver la cache`_.

.. _configurer son système pour désactiver la cache: https://www.drupal.org/node/2598914

.. admonition:: C'est à double tranchant
  :class: error

  Le cache désactivé, il sera beaucoup plus facile de tester vos développements... |br|
  Mais tout ce qui a trait au cache sera plus difficile à tester 🙂 !


Mise en œuvre
=============

1/ Créer un fichier de configuration local à partir de l'exemple fourni et décommenter
certaines lignes :

.. code-block:: bash

  # Root of the Drupal project
  cd /web/sites
  cp example.settings.local.php default/settings.local.php
  cd default
  sudo vim settings.local.php

.. code-block:: php

  // Uncomment
  $settings['cache']['bins']['render'] = 'cache.backend.null';
  $settings['cache']['bins']['page'] = 'cache.backend.null';
  $settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.null';


2/ Modifier le fichier de configuration des services en rajoutant les lignes suivantes :

.. code-block:: bash

  # Root of the Drupal project
  cd /web/sites
  sudo vim developpement.service.yml

.. code-block:: yaml

  # Report configuration
  parameters:
    http.response.debug_cacheability_headers: true
    twig.config:
      debug: true
      auto_reload: true
  services:
    cache.backend.null:
      class: Drupal\Core\Cache\NullBackendFactory


3/ Ouvrir le fichier settings.php et décommenter la ligne suivante :

.. code-block:: bash

  # Root of the Drupal project
  cd /web/sites/default
  sudo vim settings.php

.. code-block:: php

  // Uncomment
  if (file_exists($app_root . '/' . $site_path . '/settings.local.php')) {
    include $app_root . '/' . $site_path . '/settings.local.php';
  }


4 - Vider les caches.

.. code-block:: bash

  # Root of the Drupal project
  vendor/bin/drush cr


À savoir
========

Vous pouvez facilement passer en mode développement à l'aide de la Drupal console.

.. code-block:: bash

  # Root of the Drupal project
  vendor/bin/drupal site:mode dev
  vendor/bin/drupal site:mode prod
