.. |br| raw:: html

  <br />


*************************
Présentation du framework
*************************


Définition d'un CMS
===================

Les CMS - Content Management System - sont des outils qui permettent de concevoir
et gérer des sites web. Ils existent de nombreux CMS, pour différents langages,
mais tous partagent les mêmes caractéristiques :

* la possibilité d'enrichir leurs fonctionnalités avec des extensions,
* une séparation entre le fond et la forme,
* une gestion des utilisateurs,
* un faible temps de développement pour un site éditorial simple.

Parmi les CMS Php les plus connus, on peut citer :

* pour des sites éditoriaux : Wordpress, Joomla, Typo 3...,
* pour les sites e-commerce : Prestashop, Magento...

.. figure:: _static/intro_CMS.png
  :align: center
  :width: 40%


Dupal en quelques mots
======================

**Drupal est avant tout un CMS.**

* drupal utilisable out of the box avec un thème (bartik),
* permet de gérer des contenus, de les organiser,
* des modules pour étendre son comportement natif,
* des thèmes, gratuits / payants,
* la possibilité de développer ses propres modules & thèmes.

**Mais c'est aussi un framework Php.**

* orienté POO,
* intègre des composants Symfony,
* architecture découplée.

.. figure:: _static/intro_drupal_symfony.jpg
  :align: center
  :width: 40%


Un point sur l'architecture découplée
=====================================

Une architecture CMS headless se caractérise par **la dissociation entre le front
et le back**. |br|

Le back met à disposition les données qu'il traite au travers d'APIs, ce qui
laisse la possibilité, côté front, de servir ces données par le biais de
différentes technologies consommant ces services.

Quelques avantages :

* séparation des données et de leur présentation,
* toutes les fonctionnalités sont accessibles via l’API,
* la flexibilité du theming : plus de contraintes de technologies.

**Architecture traditionnelle**

.. figure:: _static/intro_archi_mono.png
  :align: center
  :width: 70%

**Architecture headless**

.. figure:: _static/intro_archi_headless.png
  :align: center
  :width: 70%

**Architecture découplée**

Drupal est API first : le front et le back se trouvent dans l’environnement du
CMS ce qui n'empêche pas d'autres technologies, en dehors du CMS de consommer ces
services pour afficher les données.

C'est une architecture headless en plus poussée.

.. figure:: _static/intro_archi_decoupled.png
  :align: center
  :width: 70%


Les composants Symfony
======================

`Liste de composants Symfony intégrés à Drupal`_

.. _Liste de composants Symfony intégrés à Drupal: https://symfony.com/projects/drupal

Nous en verrons des applications concrètes, notamment l'utilisation des composants :

* **Yaml** : pour l'utilisation de fichiers de configuration, |br|
* **Routing** : pour la gestion des routes, |br|
* **Translation** : pour la gestion du multilangue, |br|
* **EventDispatcher** : pour la gestion des événements, |br|
* **DependencyInjection** : pour l'injection de dépendances. |br|


Les normes PSR
==============
PSR pour **Propose a Standards Recommendation**

Ce sont de recommandations émises par le `FIG`_ - *Framework Interoperability Group*
- pour améliorer l’interopérabilité entre les frameworks php.

* PSR-1 : fixe les conventions minimales de codage,
* PSR-2 : pose les bases pour le style et l’organisation du code,
* PSR-3 : traite de l'interface commune aux librairies de logger,
* PSR-4 : traite du chargement des classes php (autoloading) - *remplace la norme PSR-0*.

`Pour en savoir plus sur les recommandations PHP`_

.. _FIG: https://www.php-fig.org
.. _Pour en savoir plus sur les recommandations PHP: https://www.php-fig.org/psr
