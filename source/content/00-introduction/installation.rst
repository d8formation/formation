.. |br| raw:: html

  <br />

.. role:: underline


************
Installation
************


.. admonition:: Documentation
  :class: Note

  `La documentation de référence`_

.. _La documentation de référence: https://www.drupal.org/docs/develop/using-composer/using-composer-to-install-drupal-and-manage-dependencies#download-core


Configurer ses serveurs
=======================

* Allumer vos serveurs php & mysql.
* `Configurer éventuellement vos hôtes virtuels`_.

.. _Configurer éventuellement vos hôtes virtuels: ../ressources/configuration-vhost.html

Créer votre base de données
===========================

.. tabs::

   .. tab:: En ligne de commande

    .. parsed-literal::

      mysql -u root -p
      create database training;
      exit;

   .. tab:: En back-office

     * http://localhost/phpmyadmin
     * name : training
     * interclassement : utf8mb4_general_ci


Télécharger les sources
=======================

Dans le répertoire web de votre serveur local.

.. code-block:: bash

  cd /var/www
  composer create-project drupal/recommended-project training


Installer Drupal
================

En vous rendant à l'adresse déclarée dans la configuration de votre serveur virtuel.

.. figure:: _static/intro_install_drupal.png
  :align: center
  :width: 100%

.. NOTE::
  Vous pouvez également installer votre projet en ligne de commande avec Drush

  .. code-block:: bash

    # Root of the Drupal project
    vendor/bin/drush si standard --db-url=mysql://[db_user]:[db_pass]@[ip-address]/[db_name]
