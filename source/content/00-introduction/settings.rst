.. |br| raw:: html

  <br />


**********************
Le fichier de settings
**********************


Outre ce qui est accessible en back-office, une partie de la configuration se fait
également dans le code source de l'application, dans le fichier ``/sites/default/settings.php``.

On peut configurer entre autre :

* la / les base(s) de données utilisée(s),
* la localisation du dossier de synchronisation des configurations ``sync``,
* la localisation du dossier ``private`` pour les médias dont l'accès est restreint,
* quelques paramètres de sécurité,
* ...

.. NOTE::
  Ce fichier est très amplement documenté. |br|
  Prenez le temps de le lire afin de mieux comprendre ces différents paramètres.


La configuration de la base de données
======================================

La déclaration de la base de données utilisée par Drupal se fait à l'installation
du site. Les informations que vous déclarez sont enregistrées dans le fichier de
configuration.

.. code-block:: php

  $databases['default']['default'] = [
    'database' => 'formation',
    'username' => 'root',
    'password' => '',
    'prefix' => '',
    'host' => '127.0.0.1',
    'port' => '3306',
    'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
    'driver' => 'mysql',
  ];

D'autres bases de données peuvent être utilisées dans l'application. Elles sont
également à déclarer dans le fichier de configuration. Nous verrons dans les
chapitres suivant comment l'interroger.

.. code-block:: php

  $databases['second_db']['default'] = [
    'database' => 'db_ext',
    'username' => 'root',
    'password' => '',
    'prefix' => '',
    'host' => 'path/to/another/host',
    'port' => '3306',
    'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
    'driver' => 'mysql',
  ];


Trusted host configuration
==========================

`Documentation Drupal`_

.. _Documentation Drupal: https://www.drupal.org/docs/8/install/trusted-host-settings

Ce paramètre de sécurité permet d'éviter l'usurpation d'en-tête de l'hôte HTTP,
en utilisant un nom de domaine différent. Dit autrement, Drupal considère l'en-tête
HTTP Host comme une entrée utilisateur et par conséquent non fiable.

Le paramètre de configuration est un tableau de modèles d'expression régulière,
sans délimiteurs, représentant les noms d'hôtes à partir desquels l'exécution
d'une requête est autorisée.

.. code-block:: php

  $settings['trusted_host_patterns'] = [
    '^www\.example\.com$',
    '^example\.com$',
  ];


Dossier de synchronisation et dossier privé
===========================================

`La configuration générale`_ de votre système - informations du site, configuration
des entités, configuration et positionnement des blocks ... - peut être exportée
et synchronisée entre deux instances de votre site.

Le dossier qui accueille les configurations est créé à l'installation, dans le
répertoire des fichiers publics, avec une nomenclature aléatoire - un token
adjoint au nom du répertoire. Cette configuration peut être personnalisés, en
préférant déclarer ce dossier à l'extérieur du dossier web.

.. _La configuration générale: ../01-concepts-drupal/datas-structure.html#les-donnees-de-configuration


De la même manière, le dossier ``private`` peut être déclaré dans ce fichier. Et
comme pour le dossier des synchronisations, il est préférable ce dossier en dehors
du dossier web.

.. admonition:: Attention !
  :class: error

  Il faut un fichier .htaccess dans votre dossier privé. |br|
  Drupal le génère automatiquement... Mais il peut arriver - *pour des raisons
  de configuration de droit d'écriture* - que le système n'y arrive pas ! |br|
  Penser à consulter vos logs !

.. code-block:: php

  $settings['config_sync_directory'] = '../config/sync';
  $settings['file_private_path'] = '../private';


Utiliser des variables d'environnement
======================================

Que ce soit pour des raisons de sécurité ou pour faciliter la gestion de multiples
environnements, la configuration d'un système peut se gérer à l'aide de variables
d'environnement, déclarées dans un fichier ``.env`` **placé à la racine de votre
projet, en dehors du dossier web.**

Le principe est simple :

* un fichier permet de déclarer toutes les variables nécessaires,
* on utilise ces variables dans le fichier de configuration grâce à la fonction ``getenv()``.

Fichier de configuration des variables d'environnement : ``.env``

.. code-block:: text

  HASH_SALT= Apib1f1TjWMWj4V9HYBLUAnJd_d9Y4_CwwJniVtJfpWV_6poRefOoN_iS_10kZIzAwgGGFfctg
  SYNC_CONFIG_DIRECTORY= ../config/sync
  MYSQL_DATABASE= training
  MYSQL_USER= root
  MYSQL_PASSWORD=
  MYSQL_HOSTNAME= 127.0.0.1
  MYSQL_PORT = 3306

Fichier de configuration du site : ``settings.php``

.. code-block:: php

  $settings['hash_salt'] = = getenv('HASH_SALT');

  $config_directories['sync'] = getenv('SYNC_CONFIG_DIRECTORY');

  $databases['default']['default'] = [
    'database' => getenv('MYSQL_DATABASE'),
    'username' => getenv('MYSQL_USER'),
    'password' => getenv('MYSQL_PASSWORD'),
    'prefix' => '',
    'host' => getenv('MYSQL_HOSTNAME'),
    'port' => getenv('MYSQL_PORT'),
    'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
    'driver' => 'mysql',
  ];


Droit d'accès aux fichiers
==========================

`Cette documentation`_ pose les grands principes de la gestion des permissions et
de la sécurisation des fichiers. `Ce script`_, à adapter à votre système d'information,
reprend dans les grandes lignes ces préconisations.

.. _Cette documentation: https://www.drupal.org/node/244924
.. _Ce script: https://www.drupal.org/node/244924#script-based-on-guidelines-given-above

+-------------------+--------------------------+
| Fichier           | Configuration            |
+===================+==========================+
| Tous les fichiers | chown -R [user]:www-data |
+-------------------+--------------------------+
| /web              | chmod 750                |
+-------------------+--------------------------+
| /sites            | chmod 750                |
+-------------------+--------------------------+
| /default          | chmod 770                |
+-------------------+--------------------------+
| /files            | chmod 770                |
+-------------------+--------------------------+
| /sync             | chmod 750                |
+-------------------+--------------------------+
| settings.php      | chmod 640                |
+-------------------+--------------------------+
| env               | chmod 640                |
+-------------------+--------------------------+

:download:`Le script : fix_permissions.sh<_static/fix_permissions.sh>`

.. code-block:: bash

  # Execute anywhere
  # drupal_path=/var/www/drupal/web
  sudo bash ./permissions.sh --drupal_path=[path/to/drupal] --drupal_user=$USER --httpd_group=www-data
