.. |br| raw:: html

  <br />

.. role:: underline


***********************
Installation de modules
***********************


.. admonition:: Documentation
  :class: Note

  `Catalogue des modules communautaires`_

.. _Catalogue des modules communautaires: https://www.drupal.org/project/project_module


À propos des modules
====================

Il existe beaucoup de modules, et parfois plusieurs modules pour une même fonctionnalité. |br|
Pour juger de la qualité d'un module, on peut se reposer sur plusieurs indices :

* la couverture par `la politique de sécurité`_,
* le nombre de téléchargements et d'utilisation rapportée,
* une pré-existence pour des versions antérieur de Drupal,
* la date de dernière MAJ - *à prendre avec des pincettes*,
* les informations sur la maintenance et les issues,
* l'existence d'une documentation.

Un exemple : `Module pathauto`_  **VS**  `Module layout`_

.. _la politique de sécurité: https://www.drupal.org/drupal-security-team/security-advisory-process-and-permissions-policy
.. _Module layout: https://www.drupal.org/project/layout
.. _Module pathauto: https://www.drupal.org/project/pathauto


Installer un module
===================

* ``Composer`` pour ajouter les sources du modules au projet,
* ``Drush`` pour activer, voire utiliser le module.

.. code-block:: bash

  # Root of the Drupal project
  composer require drupal/[module_name]
  vendor/bin/drush en -y [module_name]


Des modules très utilisés
=========================


:underline:`Back office`

* `Admin toolbar <https://www.drupal.org/project/admin_toolbar>`_
* `Module filter <https://www.drupal.org/project/module_filter>`_
* `Devel <https://www.drupal.org/project/devel>`_

:underline:`Template`

* `Adminimal theme <https://www.drupal.org/project/adminimal_theme>`_
* `Adminimal admin toolbar <https://www.drupal.org/project/adminimal_admin_toolbar>`_
* `Bootstrap 3.4 <https://www.drupal.org/project/bootstrap>`_

:underline:`Cartographie`

* `Geocoder <https://www.drupal.org/project/geocoder>`_
* `Geofield <https://www.drupal.org/project/geofield>`_
* `Leaflet <https://www.drupal.org/project/leaflet>`_

Cette série de module fonctionne très bien avec le module
`Address <https://www.drupal.org/project/address>`_

:underline:`Intégration de contenu`

* `Paragraphs <https://www.drupal.org/project/paragraphs>`_

:underline:`Formulaire`

* `Address <https://www.drupal.org/project/address>`_
* `Honeypot <https://www.drupal.org/project/honeypot>`_

:underline:`SEO`

* `Pathauto <https://www.drupal.org/project/pathauto>`_
* `Metatag <https://www.drupal.org/project/metatag>`_
* `Sitemap <https://www.drupal.org/project/sitemap>`_

:underline:`Administration`

* `Ultimate Cron <https://www.drupal.org/project/ultimate_cron>`_
* `Configuration split <https://www.drupal.org/project/config_split>`_

:underline:`RGPD`

* `Cookie compliance <https://www.drupal.org/project/eu_cookie_compliance>`_
