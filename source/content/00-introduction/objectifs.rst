*********
Objectifs
*********

À partir des notions abordées, vous serez capable de :

* définir Drupal en tant que CMS,
* lister les principaux prérequis,
* identifier les outils du développeur Drupal,
* installer un environnement de développement,
* utiliser les outils Drupal : Composer, Drush,
* installer et configurer des extensions : modules, thèmes,
* configurer votre environnement de développement.
