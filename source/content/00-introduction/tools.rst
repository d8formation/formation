.. |br| raw:: html

  <br />

.. role:: red
.. role:: underline


*************************
Les outils du développeur
*************************

Composer
========

`Composer`_ est un utilitaire php qui permet de déclarer et d'installer des librairies
nécessaires à un projet. Outre le versionning, Composer permet également de résoudre
les dépendances entre les librairies.

Par défaut, Composer va rechercher le core de Drupal dans le dépôt officiel,
mais pour l'ensemble des modules communautaires, Composer recherche dans un dépôt
spécifique, déclaré dans le ``composer.json``.

.. _Composer: https://getcomposer.org

.. code-block:: json

  "repositories": [
    {
      "type": "composer",
      "url": "https://packages.drupal.org/8"
    }
  ],


Tout au long de votre projet, Drupal & les modules communautaires seront gérés
avec Composer pour leur installation et leur mise à jour.

!!!!

.. rst-class:: uppercase-text

Installation

.. tabs::

  .. tab:: Linux

    .. code-block:: bash

      php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
      php -r "if (hash_file('sha384', 'composer-setup.php') === '756890a4488ce9024fc62c56153228907f1545c228516cbf63f885e036d37e9a59d27d63f46af1d4d07ee0f76181c7d3') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
      php composer-setup.php
      php -r "unlink('composer-setup.php');"

      # Install globally
      mv composer.phar /usr/local/bin/composer

      # Documentation
      composer

  .. tab:: Windows

    * Télécharger l’exécutable : `https://getcomposer.org/download/`_ ;
    * **OU** utiliser la procédure Linux pour l'utiliser dans l'émulateur de ligne
      de commande **cmder**. On s’arrêtera au téléchargement du fichier ``composer.phar``.

.. _https://getcomposer.org/download/: https://getcomposer.org/download/


Drush
=====

Drush = Drupal Shell |br|
C'est un ensemble de scripts qui facilitent l'administration de son site Drupal
au quotidien.

* pour vider les caches,
* pour installer / désinstaller des modules,
* pour gérer l'import/export des configurations,
* pour installer sur site,
* pour mettre à jour, exporter sa bdd,
* ...

.. admonition:: Documentation
  :class: Note

  `Liste des commandes drush`_

.. _Liste des commandes drush: https://drushcommands.com

!!!!

.. rst-class:: uppercase-text

Installation

.. tabs::

  .. tab:: Dans le projet

    Installer Drush avec Composer, au lancement de votre projet.

    .. code-block:: bash

      # Root of the Drupal project
      composer require drush/drush

      # Documentation
      vendor/bin/drush

  .. tab:: Launcher Linux

    Pour exécuter Drush dans n'importe quelle projet Drupal.

    * `http://docs.drush.org/en/master/install/ <http://docs.drush.org/en/master/install/>`_
    * `https://github.com/drush-ops/drush-launcher/ <https://github.com/drush-ops/drush-launcher/>`_

    .. code-block:: bash

      wget -O drush.phar https://github.com/drush-ops/drush-launcher/releases/download/0.6.0/drush.phar
      chmod +x drush.phar
      sudo mv drush.phar /usr/local/bin/drush
      drush

  .. tab:: Launcher Windows

    Pour exécuter Drush dans n'importe quelle projet Drupal.

    * `https://github.com/drush-ops/drush-launcher <https://github.com/drush-ops/drush-launcher>`_
    * `http://modulesunraveled.com/drush/installing-drush-windows <http://modulesunraveled.com/drush/installing-drush-windows>`_


La console Drupal
=================

`La console Drupal`_ est un autre outil ligne de commande, adapté de la console
Symfony, pour interagir avec votre système. En complément de Drush - *plutôt dédié
à la gestion courante de votre site* - la console Drupal est un outil d'aide au
développement, principalement utile pour générer du code "standardisé", autrement dit
des squelettes de composants : module, theme, controller, block ...

Vous trouverez `dans cet article`_ de plus amples explications sur la différence
entre ces deux outils.

.. _La console Drupal: https://drupalconsole.com/docs/en/
.. _dans cet article: https://pantheon.io/drupal-8/introduction-drush-and-drupal-console

!!!!

.. rst-class:: uppercase-text

Installation


.. tabs::

  .. tab:: Dans le projet

    Installer la console avec Composer, au lancement de votre projet.

    .. code-block:: bash

      # Root of the Drupal project
      composer require drupal/console

      # Documentation
      vendor/bin/drupal


  .. tab:: Launcher Linux

    Pour exécuter la console dans n'importe quelle projet Drupal.

    * `http://docs.drush.org/en/master/install/ <http://docs.drush.org/en/master/install/>`_
    * `https://github.com/drush-ops/drush-launcher/ <https://github.com/drush-ops/drush-launcher/>`_

    .. code-block:: console

      php -r "readfile('https://drupalconsole.com/installer');" > drupal.phar
      mv drupal.phar /usr/local/bin/drupal
      chmod +x /usr/local/bin/drupal


CodeSniffer
===========

`Php Code Sniffer`_ est une librairie qui permet de détecter les erreurs de style
par rapport à un/des standard(s) de code implémenté(s).

L'installation proposé ici permet un usage de CodeSniffer en ligne de commande
mais vous pouvez utiliser directement CodeSniffer dans votre IDE. |br|
*Possible sur Atom, PhpStorm, Eclipse ...*

.. _Php Code Sniffer: https://github.com/squizlabs/PHP_CodeSniffer

!!!!

.. rst-class:: uppercase-text

Installation

`Documentation`_

1. Installer Coder de manière globale en local.
2. Implémenter les standards de code Drupal.
3. Noter le chemin d'installation.
4. Configurer phpcs avec le chemin d'installation de CodeSniffer.

.. _Documentation: https://www.drupal.org/node/1419988

.. code-block:: bash

  # Root of the Drupal project
  composer global require drupal/coder
  composer global require dealerdirect/phpcodesniffer-composer-installer
  composer global show -P
  // php CodeSniffer Config installed_paths set to ~/.composer/vendor/drupal/coder/coder_sniffer
  phpcs -i
  phpcs --config-set installed_paths ~/workspace/coder/coder_sniffer


!!!!

.. rst-class:: uppercase-text

Utilisation

Pour vérifier le respect de votre style de code :

.. code-block:: bash

  # Root of the Drupal project
  phpcs --standard=Drupal --extensions=php,module,inc,install,test,profile,theme,css,info,txt,md,yml /path/to/module


Pour fixer les erreurs de style :

.. code-block:: bash

  # Root of the Drupal project
  phpcbf --standard=Drupal --extensions=php,module,inc,install,test,profile,theme,css,info,txt,md,yml /path/to/module
