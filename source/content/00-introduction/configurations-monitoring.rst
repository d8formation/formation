.. role:: underline


***************************
Configuration et monitoring
***************************


Les informations du site
========================

C'est une parties des informations saisies à l'installation de site. En plus des
informations de contact, se configurent également :

* la page d'accueil,
* les pages spécifiques aux erreurs 403 et 404.

.. figure:: _static/conf_site_information.png
  :align: center
  :width: 100%


Performance
===========

Ces sont les paramètres d'optimisation des performances du site, à savoir :

* la durée du cache pour le navigateur et le proxy,
* l'agrégation des fichiers css et js.

Ces paramètres de configuration sont amenés à changer entre l'environnement de
développement et celui de production.

.. figure:: _static/conf_performances.png
  :align: center
  :width: 100%


Mode maintenance
================

.. figure:: _static/conf_maintenance.png
  :align: center
  :width: 100%


Monitoring
==========

Plusieurs outils sont disponibles pour suivre l'activité de votre site :

:underline:`Une page dédiée à l'état de santé de votre site`

.. figure:: _static/conf_monitoring.png
  :align: center
  :width: 100%

:underline:`Une page de log`

.. figure:: _static/conf_log.png
  :align: center
  :width: 100%

:underline:`Une page pour les mises à jour disponible`

.. figure:: _static/conf_update.png
  :align: center
  :width: 100%
