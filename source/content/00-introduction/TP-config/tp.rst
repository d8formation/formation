.. role:: underline
.. role:: red


**************************
🏃 - Un petit échauffement
**************************


⏱️ | 40 min.

!!!!

Nous allons préparer notre environnement de développement. Pour cela, nous allons :

* Installer les outils CLI pour interagir avec Drupal ;
* Télécharger quelques modules pour étendre les fonctionnalités de notre site ;
* Configurer notre système en mode développement ;

:underline:`Installation des outils CLI :`

    Installer Drush et la Drupal console.

:underline:`Installation de modules :`

    Télécharger et activer les modules suivants :

    * `Admin toolbar <https://www.drupal.org/project/admin_toolbar>`_ ;
    * `Module filter <https://www.drupal.org/project/module_filter>`_ ;
    * `Devel <https://www.drupal.org/project/devel>`_ ;
    * `Pathauto <https://www.drupal.org/project/pathauto>`_ ;
    * `Address <https://www.drupal.org/project/address>`_ ;

:underline:`Configurer son système :`

  * Configurer le ``trusted_host_pattern`` ;
  * Déclarer le dossier de synchronisation et le dossier privé ;
  * Fixer les permissions sur les dossiers de votre système ;
  * Vérifier l'état de santé de votre système ;
  * Passer en mode développement ;

:underline:`Gestion des configurations :`

  * Exporter vos configurations ;
  * Modifier le nom du site dans le fichier ``system.site.yml`` ;
  * Importer la nouvelle configuration ;
