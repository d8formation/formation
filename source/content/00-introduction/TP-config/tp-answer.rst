.. |br| raw:: html

  <br />

.. role:: underline
.. role:: red


***************************
🏅 - Éléments de correction
***************************


Installation des outils CLI
===========================

:underline:`Drush :`

  .. code-block:: bash

    # Root of the Drupal project
    composer require drush/drush
    # Documentation
    vendor/bin/drush

:underline:`Drupal console :`

  .. code-block:: bash

    # Root of the Drupal project
    composer require drupal/console
    # Documentation
    vendor/bin/drupal


  .. NOTE::
    Vous pouvez vous retrouver avec des conflits de librairies, qu'il faudra résoudre
    de la manière suivante :

    * supprimer le ``composer.lock`` et le dossier ``vendor``,
    * rajouter manuellement dans le ``composer.json``, la ligne ``"drupal/console": "^1.9",``,
    * lancer l'installation du projet avec ``composer install``.


Installation de modules
=======================

:underline:`Téléchargement des modules avec Composer :`

  .. code-block:: bash

    # Root of the Drupal project
    composer require drupal/[module_name]

    # Root of the Drupal project
    composer require drupal/admin_toolbar
    composer require drupal/module_filter
    composer require drupal/pathauto
    composer require drupal/address

:underline:`Activation des modules avec Drush :`

  Vous pouvez activer plusieurs extensions en même temps

  .. code-block:: bash

    # Root of the Drupal project
    vendor/bin/drush en -y module_filter admin_toolbar admin_toolbar_tools address pathauto


Configurer son système
=======================

:underline:`Dans le fichier settings.php  :`

  .. code-block:: php

    $settings['trusted_host_patterns'] = [
      '^www\.training\.local$',
      '^training\.local$',
    ];
    $settings['config_sync_directory'] = '../config/sync';
    $settings['file_private_path'] = '../private';

:underline:`Mode développement  :`

  Suivez `la procédure`_ proposée dans le support de formation ou utiliser la Drupal
  console.

  .. code-block:: bash

    # Root of the Drupal project
    vendor/bin/drupal site:mode dev

.. _la procédure: ../dev-mode-configuration.html#mise-en-oeuvre


Gestion des configurations
==========================

:underline:`Exporter les configuration :`

  .. code-block:: bash

    # Root of the Drupal project
    vendor/bin/drush cex

:underline:`Modifier une configuration :`

  Les configurations sont exportées dans le dossier déclaré dans le fichier
  ``settings.php``. Vous pouvez directement modifier les configurations dans les
  fichiers.

  Ex. : Information du site → system.site.yml

  .. code-block:: yaml

    # system.site.yaml
    uuid: b4859f3b-5ac1-42bb-a6cd-e55647d6fc80
    name: 'Drupal - Le site de la formation'
    mail: webmaster@training.local
    slogan: ''
    page:
      403: ''
      404: ''
      front: /node
    admin_compact_mode: false
    weight_select_max: 100
    langcode: fr
    default_langcode: fr
    _core:
      default_config_hash: yTxtFqBHnEWxQswuWvkjE8mKw2t8oKuCL1q8KnfHuGE

:underline:`Appliquer une modification de configuration :`

  En important la nouvelle configuration.

  .. code-block:: bash

    # Root of the Drupal project
    vendor/bin/drush cim
