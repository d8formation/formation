.. |br| raw:: html

  <br />

.. role:: underline


************************
L'architecture de Drupal
************************


.. figure:: _static/drupal_structure.png
    :align: right
    :width: 100%

Pour mieux comprendre l'architecture de Drupal, il faut déjà distinguer :

* le dossier du projet : il contient **le dossier web**, les librairies, les médias
  privés, des fichiers de configuration, le fichier composer.son...
* le dossier ``/web``  : il contient les sources de l'application.

Le dossier ``/web`` - le webroot - doit être le seul dossier accessible vers lequel
doivent pointer toutes les requêtes. Son nommage est arbitraire et il peut être
personnalisé - htdocs, www.


Core
====

C'est le dossier qui regroupe toutes les composantes du core.

Il y a différents sous-dossiers, notamment :

* un dossier ``modules``, qui regroupe tous les modules du core,
* un dossier ``themes`` qui regroupe les thèmes utilisables par défaut,
* un dossier ``profiles`` qui regroupe les différents profils d'installation : minimal,
  standard, umami.


Modules
=======

C'est le dossier dans lequel sont regroupés tous les modules additionnels du site :

* un dossier ``contrib`` pour
  `les modules communautaires <https://www.drupal.org/project/project_module>`_,
* un dossier ``custom`` pour les modules personnalisés.


Thèmes
======

Identique au dossier ``modules``, avec les mêmes règles de bonnes pratiques :

* un dossier ``contrib`` pour les `thèmes communautaires <https://www.drupal.org/project/project_theme>`_,
* un dossier ``custom`` pour les thèmes personnalisés.


Sites
======

* toutes les ressources - *fichiers et médias* - du site,
* les fichiers de configuration.


Profiles
========

Un profil d'installation correspond à une version pré-configuration de Drupal.
Nativement, 3 profils d'installation nous sont proposés :

* standard - nous l'avons installé,
* minimum,
* umami.

.. figure:: _static/profile_installation.png
    :align: center
    :width: 75%

:underline:`Une illustration très concrète :`

  Une installation **standard** préconfigure un certain nombre de paramètres :

  * les modules activés,
  * des types d'entités : Node - article, page / Vocabulaire - Tags, Block custom...,
  * des rôles pour les utilisateurs,
  * des positionnements de blocs,
  * des configurations diverses : format de date, éditeur visuel...

Les profils d'installation sont personnalisables, ce qui permet d'industrialiser
plus facilement des déploiements. Il existe également des
`profiles communautaires`_ pour des sites à vocation spécifique : e-commerce,
site éditorial...

.. _profiles communautaires: https://www.drupal.org/project/project_distribution

.. figure:: _static/profile_umami.png
    :align: center
    :width: 75%


Les autres dossiers
===================

* le dossier ``/config`` regroupe les fichiers de configuration,
* le dossier ``/private`` regroupe tous les médias du site dont l'accès est restreint.

Ces deux dossiers se déclarent dans le fichier de configuration ``settings.php``.
