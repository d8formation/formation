.. # define break line
.. |br| raw:: html

  <br />


*************
Le hook_theme
*************

Avant de travailler direc, nous allons aborder cette

Principe de base
================

Un ``hook_theme()`` permet de définir un template pour gérer le rendu des composants
- Controlleur, Form, Block, ... - de votre module. |br|
Son implémentation se fait dans votre module custom, quelque soit le thème défini par défaut.


Déclarer un hook_theme()
========================

Dans le fichier de déclaration ``custom_module.module`` |br|
*Vous pouvez déclarer autant de template que nécessaire*

.. code-block:: php

  function custom_module_theme($existing, $type, $theme, $path){

    $themes['custom_template'] = [
      'template' => 'custom_template',
      'path' => drupal_get_path('module', 'custom_module').'/templates',
      'variables' => [
        'foo' => NULL,
        'bar' => NULL,
      ],
    ];
    return $themes;
  }


Dans le dossier ``custom_module/templates``, implémenter le template ``custom_template.html.twig``

::

  <div>
    <p>{{ foo }}</p>
  </div>
  <div>
    <p>{{ bar }}</p>
  </div>

Dans le render array d'un composant de votre module (Controller, block)

.. code-block:: php

  return [
    '#theme' => 'custom_template',
    '#foo'  => $foo,
    '#bar'  => $bar,
  ];


Render API |br|
`https://www.drupal.org/docs/8/api/render-api/render-arrays#properties <https://www.drupal.org/docs/8/api/render-api/render-arrays#properties>`_
