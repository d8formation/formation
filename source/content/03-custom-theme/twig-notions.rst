.. # define break line
.. |br| raw:: html

  <br />


********************
Notions sur twig
********************

Documentation
===================
Twig est un moteur de rendu php, issus de l'implémentation de Symfony.

* Documentation : `http://twig.sensiolabs.org <http://twig.sensiolabs.org>`_ ;
* API : `https://symfony.com/doc/current/reference/twig_reference.html#functions <https://symfony.com/doc/current/reference/twig_reference.html#functions>`_ ;


La syntaxe
================
Tous les fichiers de template doivent se terminer par l'extension ``.html.twig``. |br|
3 syntaxes à utiliser :

* {{ afficher }}
* {# commenter #}
* {% programmer %}

::

  <html>
    <head>
      <title>{{ head_title }}</title>
    </head>
    <body class="{{ classes }}" {{ attributes }}>
      <div class="row">
        {{ title }}
        {{ content.field_custom }} <!-- parcourir un tableau -->
      </div>
    </body>
  </html>


Utiliser des fonctions
============================

On peut utiliser des fonctions de deux manières différentes :

Dans le template, 'à la volée'
------------------------------

* Dans des structures de tests conditionnels ou des boucles :

::

  {% if item ... %} {% else %} {% endif %}
  {% for item in items %} ({% else %}) {% endfor %}


* À l'aide des fonctions mises à disposition par les framework :

En utilisant des fonctions natives de symfoni |br|
`https://symfony.com/doc/current/reference/twig_reference.html#functions <https://symfony.com/doc/current/reference/twig_reference.html#functions>`_ ;

En utilisant des fonctions fournies par Drupal |br|
`https://www.drupal.org/node/2486991 <https://www.drupal.org/node/2486991>`_ ;

Avec des fonctions macro
------------------------

On peut déclarer des fonctions dans le template.

::

  {% macro kamehameha(class, increment) %}
      <div class="{{ class }}">
        KAMEHAMEHA{% for i in range(0, increment) %}A{% endfor %}
      </div>
  {% endmacro %}

  <!-- pour importer la macro déclarée dans le template -->
  {% import _self as function %}
  <div class="dbz">
    {{ function.kamehameha('red', 10) }}
  </div>


Utiliser les filtres
==========================

Vous pouvez utiliser un certain nombre de filtre pour formater le rendu vos données.

::

  {{ title|upper }}

Tous les filtres disponibles |br|
`https://symfony.com/doc/current/reference/twig_reference.html#filters <https://symfony.com/doc/current/reference/twig_reference.html#filters>`_

.. IMPORTANT::
  Drupal rend des fields imbriqués dans des div. Vous ne pouvez pas appliquer directement un filtre. |br|
  Il vous faudra, soit parcourir la variable dans le template pour accéder à la valeur, soit isoler la valeur de la données dans une fonctions de preprocess.
