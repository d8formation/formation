.. # define break line
.. |br| raw:: html

  <br />


**********************
Gestion des assets
**********************

Déclaration
=================

On les précise dans le fichier de configuration ``custom_theme.librairies.yml``. On y déclare :

* Les fichiers css ;
* Les fichiers js ;
* Les fichiers ressources externes ;

::

  front:
    css:
      base:
        'https://fonts.googleapis.com/icon?family=Material+Icons': { type: external }
        'https://fonts.googleapis.com/css?family=Oxygen': { type: external }

      theme:
        css/style.css: {}
    js:
      js/script.js: {}
    dependencies:
      - core/jquery


Si on veut utiliser globalement une librairie - *c'est à dire chargé avec le thème* -, on la déclare dans le fichier de configuration ``custom_theme.info.yml``.

::

  libraries:
    - 'custom_theme/front'


Utilisation
=================

On peut utiliser ces librairie au globalement avec le thème, ou de manière contextuelle en déclarant celles-ci à la volée dans un fichier de preprocess ou directement dans le thème.

Exemple d'implémentation :

* Dans un preprocess

.. code-block:: php

  function custom_theme_preprocess_HOOK(&$variables) {
    $variables['#attached']['library'][] = 'custom_theme/custom_librairie';
  }


* Dans le thème

::

  {{ attach_library('custom_theme/custom_librairie') }}

* Dans un render array

.. code-block:: php

  $build['#attached']['library'][] = 'custom_theme/custom_librairie';
