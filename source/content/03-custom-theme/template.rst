.. # define break line
.. |br| raw:: html

  <br />


********************************
Fonctionnenemt du templating
********************************

Héritage
==============

Le mécanisme d'héritage permet de définir un **thème parent** à partir duquel on peut définir des **thèmes enfant**.

Exemple : |br|
Implémentation d'un thème parent de block et héritage de template pour un bloc spécifique.

::

  {# PARENT : Template block.html.twig #}
  {% block content %}
  <div class="row">
    <p>Mon content parent</p>
  </div>
  {% endblock %}
  {% block footer %}
  <div class="row">
    <p>Mon footer</p>
  </div>
  {% endblock %}

  {# ENFANT : Template block--bloctype.html.twig #}
  {% extends "block.html.twig" %}
  {% block footer %}
  <div class="row">
    <p>Mon footer de <i>bloctype</i></p>
  </div>
  {% endblock %}

  {# rendu block--bloctype.html.twig #}
  <div class="row">
    <p>Mon content</p>
  </div>
  <div class="row">
    <p>Mon footer de <i>bloctype</i></p>
  </div>


Inclusion de template
===========================

::

  {% include 'mon_template.html.twig' %}


La surchage de template
=============================

Vous pouvez, par simple déclaration, implémenter un template et le surcharger.

API Drupal |br|
`https://www.drupal.org/node/2354645 <https://www.drupal.org/node/2354645>`_
