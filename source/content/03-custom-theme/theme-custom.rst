.. # define break line
.. |br| raw:: html

  <br />


********************************
Création d'une thème de base
********************************

Principe de base
======================

Drupal s'installe avec - pour simplifier - deux thèmes natifs :

* Bartik, pour le front ;
* Seven, pour le back ;

.. figure:: _static/theme_administration.png
    :align: center


Les thèmes fonctionnent sur le même principe que les modules : ce sont des composants qui s'installent, s'activent ou se déactivent, se créent de toute pièce.

.. NOTE::
  Nous avons installé en début de formation le thème d'administration Adminimal.

Prenez le temps de regarder le thème bartik, en particulier les fichiers ``/core/themes/bartik/bartik.info.yml`` et ``/core/themes/bartik/bartik.module``.

.. ASTUCE::
  Un dossier ``contrib`` pour les thèmes communautaires et un dossier ``custom`` pour vos thèmes personnels.

Le rendu d'une page Drupal
================================

Une page est composée par un ensemble de templates, inclus les uns dans les autres.

Pour une page simple :

::

  html.html.twig
    page.html.twig
      region.html.twig
        block--menu.html.twig
          menu.html.twig
      region.html.twig
        block--maincontent.html.twig
          node.html.twig
            field.html.twig
      region.html.twig
        block--footer.html.twig


Comment fonctionne le templating
======================================

Des fichiers de configuration .yml pour

* déclarer la structure du thèmes ;
* déclarer des configuration : librairies, points de rupture... ;

Au niveau de la gestion des templates :

* Surcharge de template par simple déclaration : `https://www.drupal.org/node/2354645 <https://www.drupal.org/node/2354645>`_ ;
* Fonction de preprocess pour manipuler les données ;
* Hook disponible pour faire de la suggestion de template.

Au niveau des librairies :

* Déclaration des librairies custom ;
* Possibilité de charger contextuellement des librairies ;


Iplémenter un thème
=========================

Seul fichier nécessaire : ``custom_theme.info.yml``

::

  core: 8.x
  type: theme
  base theme: false
  name: 'Custom theme'
  description: ''

  regions:    // les régions de votre layout
  libraries:  // vos librairies css et js


Les autres fichiers de configuration

+------------------------------+--------------------------------------+
| Fichier                      | Configuration                        |
+==============================+======================================+
| custom_theme.theme           | Déclaration des hooks                |
+------------------------------+--------------------------------------+
| custom_theme.librairies.yml  | Librairies utilisées par votre thème |
+------------------------------+--------------------------------------+
| custom_theme.breakpoints.yml | Les points de rupture de votre thème |
+------------------------------+--------------------------------------+


Précision sur le layout des régions
=========================================

Vous définissez dans votre thème les régions que vous retrouverez sur la page d'administration du layout des blocks. |br|
Cependant, l'aperçu qui vous verrez dans layout de block repose sur le template de la page. |br|
Si, dans ce template, vous n’appelez pas une région, vous ne la verrez pas apparaître dans l'aperçu.

.. figure:: _static/layout_block.png
    :align: center


Twig debug
================

Configurer votre système en mode développement vous permet d'avoir un niveau d'information très important et très pratique sur les templates utilisés pour rendre une page. |br|
Ces informations sont accessibles dans l'inspecteur de votre navigateur.

Pour chaque template utilisé :

* Le hook theme utilisé ;
* L'adresse du template utilisé ;
* Les suggestions de templates implémentables ;

.. figure:: _static/twig_debug.png
    :align: center





    Le layout des blocs
    ===================

    Il est construit sur la base des régions qui sont déclarées dans votre thème.

    .. code-block:: yaml

      # /core/themes/bartik/bartik.info.yml
      name: Bartik
      type: theme
      core: 8.x
      regions:
        header: 'Header'
        primary_menu: 'Primary menu'
        secondary_menu: 'Secondary menu'
        page_top: 'Page top'
        page_bottom: 'Page bottom'
        highlighted: 'Highlighted'
        featured_top: 'Featured top'
        breadcrumb: 'Breadcrumb'
        content: 'Content'
        sidebar_first: 'Sidebar first'
        sidebar_second: 'Sidebar second'
        featured_bottom_first: 'Featured bottom first'
        featured_bottom_second: 'Featured bottom second'
        featured_bottom_third: 'Featured bottom third'
        footer_first: 'Footer first'
        footer_second: 'Footer second'
        footer_third: 'Footer third'
        footer_fourth: 'Footer fourth'
        footer_fifth: 'Footer fifth'


    Vous pouvez vous faire une idée du rendu de votre thème en cliquant sur le lien
    d'aperçu.


    .. admonition:: Ne pas confondre
      :class: error

      Ce rendu n'est pas construit sur la base de la déclaration de votre thème,
      mais sur le template page.html.twig
      
