*********
Objectifs
*********

* Comprendre les principes du theming dans Drupal ;
* Créer son premier thème ;
* Manipuler les templates ;
* Implémenter des librairies css, js ;
* Mettre en pratique ces nouvelles connaissances ;
