***************************************
TP 1 - Implémentation d'un hook_theme()
***************************************

Consigne
========

Créer un block custom dans lequel afficher le résultat des requêtes faites sur la permanence dans la base de données (cf. TP précédent) et déclarer un hook_theme() pour ce bloc.

Niveau 1
--------

* Just do it !
