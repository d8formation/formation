***************************************
TP 2 - Réalisation d'un template custom
***************************************

Consigne
========

Créer un template custom pour les articles : mode full ou teaser

Niveau 1
--------

* Afficher la date de création ;
* Donner, "en dur", une couleur spécifique (texte, background ...) à chaque tag ;
* Afficher le contenu si l'utilisateur est connecté, un lien pour se connecter le cas contraire ;


Niveau 2
--------

* Permettre au contributeur de choisir la couleur de son tag ;


Guide
=====

* Prenez le temps de lire la documentation des templates, beaucoup de variables sont accessibles ;
* Dans les fonctions de preprocess, l'argument ``&$variables`` peut être modifiée ... ;
* Un terme de taxonomie étant une entité, il peut être enrichi ... ;
