*****************************
TP 2 - Éléments de correction
*****************************

Implementation du hook_preprocess_node()
========================================

.. code-block:: php

  function custom_theme_preprocess_node(&$variables) {
    $node = $variables['node'];
    $nid = $node->id();
    $type = $variables['node']->getType();
    $view_mode = $variables['view_mode'];

    if($type == 'article') {

      if($view_mode == 'teaser') {

      }

      if($view_mode == 'full') {

      }
    }
  }
