
**************************************************************
TP 3 - Implémentation d'une bibliothèque JS dans un formulaire
**************************************************************

Consigne
========

Vous devez créer un formulaire et injecter votre propre librairie js.

Niveau 1
--------

Reprendre le formulaire de saisie de permanence et implémenter un compteur de caractère pour le champs de saisie de caractère ;

Niveau 2
--------

Créer un nouveau formulaire avec deux champs (ville + code postal) et implémenter la librairie JS `vicopo <https://vicopo.selfbuild.fr/>`_.


Niveau 2
--------

Implémenter un bloc, avec un formulaire de configuration dans lequel vous afficherez
un slideshow des images des 3 derniers événements renseignés.

  * Le formulaire doit permettre de choisir un des pôles d'activité ;
  * Le bloc ne doit figurer que sur les pages dédiées ;
  * Créer un thème custom et utiliser la librairie
    `Responsiveslides <http://responsiveslides.com/>`_

Niveau 3
--------

Utiliser la librairie JS `Highcharts <https://www.highcharts.com/>`_ pour avoir un rendu graphique de la répartition des permanences par pôle ;


  * Le bloc ne doit figurer que sur la page d'accueil ;
  * Créer un thème custom et utiliser la librairie
    `Responsiveslides <http://responsiveslides.com/>`_

Question subsidiaire :

  Utiliser un formulaire de configuration pour déterminer, en fonction du sport,
  les évènements à faire remonter dans le slideshow.


Guide
=====

* Soyez bien attentif à faire toutes les déclarations dans les fichiers de configuration ;
* Ne pas oublier de modifier le formulaire pour implémenter l'appel à la fonction au niveau du champs ;
* Pour le compteur JS, vous pouvez reprendre cet exemple d'implémentation :

.. code-block:: html

  <script>
    function countChar(val) {
      var len = val.value.length;
      if (len >= 500) {
        val.value = val.value.substring(0, 500);
      } else {
        $('#charNum').text(500 - len);
      }
    };
  </script>

  <textarea id="field" onkeyup="countChar(this)"></textarea>

* Vicopo : vous pouvez vous aider de `l'exemple <https://github.com/kylekatarnls/vicopo/tree/master/exemple>`_ pour démarrer votre implémentation ;
* Highcharts : utiliser une représentation en `camenbert <https://www.highcharts.com/demo/pie-legend>`_ qui est facile à construire ;
