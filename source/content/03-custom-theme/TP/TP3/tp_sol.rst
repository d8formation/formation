.. # define break line
.. |br| raw:: html

  <br />

*****************************
TP 3 - Éléments de correction
*****************************

Formulaire de saisie des permanences
====================================

Dans le fichier de déclaration des librairies ``custom_module.librairies.yml``

::

  count_char:
    js:
      js/countChar.js: {}
    dependencies:
      - core/jquery


Implémentation de la libraire dans le formulaire

.. code-block:: php

  $form['comment'] = [
    '#type' => 'textarea',
    '#title' => 'Commentaire',
    '#description' => 'Laisser votre commentaire',
    '#attributes' => [
        'onkeyup' => 'countChar(this)',
      ],
    '#states' => [
      'invisible' => [
        ':input[name="post_comment"]' => ['checked' => FALSE],
      ],
    ],
  ];

  $form['countChar'] = [
    '#markup' => '<div id="countChar"></div>',
  ];

  $form['#attached']['library'][] = 'custom_module/count_char';


Formulaire vicopo
=================

Vous pouvez repartir de l'exemple proposé, dont vous trouverez ici tous les détails. |br|
Structure du formulaire : `https://github.com/kylekatarnls/vicopo/blob/master/exemple/index.html <https://github.com/kylekatarnls/vicopo/blob/master/exemple/index.html>`_.

.. WARNING::

  * Respecter les ids des champs de formulaires : ``code`` et ``city`` ;
  * Inclure les deux fichiers js : ``api.min.js`` && ``index.js`` ;
  * On peut modifier index.js pour améliorer l'output : ``return <li><a href="#">...</a></li>``

Déclaration de la librairie
---------------------------

Dans le fichier de déclaration ``custom_module.librairies.yml``

::

  vicopo:
    js:
      js/vicopo.js: {}
      js/vicopo_form.js: {}
    dependencies:
      - core/jquery


Implémentation dans le formulaire
---------------------------------

.. code-block:: php

  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['code'] = [
      '#type' => 'textfield',
      '#title' => 'Code postal',
      '#description' => 'Saisissez un code postal',
      '#attributes' => [
        'id' => ['code'],
      ],
    ];

    $form['city'] = [
      '#type' => 'textfield',
      '#title' => 'Ville correspondante',
      '#description' => 'Choississez une ville',
      '#attributes' => [
        'id' => ['city'],
      ],
    ];

    $form['output'] = [
      '#markup' => '<div id="output"></div>',
    ];

    $form['#attached']['library'][] = 'module_core/vicopo';

    return $form;
  }

Déclaration d'une route pour tester votre formulaire
-----------------------------------------------------

::

  custom_module.vicopo:
    path: '/vicopo'
    defaults:
      _form: '\Drupal\custom_module\Form\VicopoForm'
      _title: 'Vicopo form'
    requirements:
      _permission: 'access content'







Niveau 2 : Mise en place d'un slideshow
---------------------------------------

Le formulaire de configuration du bloc
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Le formulaire de configuration est très simple, et il nous suffit d'implémenter
l'input de type autocomplete que nous avons déjà utilisé dans le TP sur les
formulaires.

.. code-block:: php

  $form['pole'] = [
    '#type' => 'entity_autocomplete',
    '#title' => 'Choisir dans un pole',
    '#required' => TRUE,
    '#target_type' => 'taxonomy_term',
    '#selection_settings' => [
      'target_bundles' => [
        'pole',
      ],
    ],
  ];

Pour lui affecter une valeur par défaut, la `documentation <https://www.drupal.org/node/2418529>`_
nous précise que nous devons lui transmettre l'entité correspondante.
Il nous faut pouvoir la charger, avec l'``entiyTypeManager`` que nous injectons
à la construction du block.

.. code-block:: php

  if (isset($config['pole'])) {
    $form['pole']['#default_value' ] = $this->entityTypeManager->getStorage('taxonomy_term')->load($config['pole']);
  }


Le traitement des images
^^^^^^^^^^^^^^^^^^^^^^^^

La requête nous permet de rechercher les 3 derniers articles concernant le tag.
Il suffit ensuite de parcourir les entités et pour chacune d'entre elle, charger
l'image pour récupérer les informations.

.. code-block:: php

  $slides = [];
  $nids = $this->entityTypeManager->getStorage('node')->getQuery()
    ->condition('type', 'article', '')
    ->condition('field_tags', $pole)
    ->sort('created', 'DESC')
    ->range(0, 3)
    ->execute();

  $entities = $this->entityTypeManager->getStorage('node')->loadMultiple($nids);
  foreach ($entities as $entity) {
    if($entity->hasField('field_image')) {
      $field_image = $entity->get('field_image')->getValue();
      if(!empty($field_image)) {
        $field_image = reset($field_image);
        $image_id = $field_image['target_id'];
        $file = $this->entityTypeManager->getStorage('file')->load($image_id);
        $slides[] = [
          'caption' => $entity->getTitle(),
          'url' => file_create_url($file->getFileUri()),
          'alt' => $file->getFileName(),
        ];
      }
    }
  }

La gestion des librairies
^^^^^^^^^^^^^^^^^^^^^^^^^

Il faut, dans le fichier de configuration des librairies ``custom_module.librairies``,
déclarer nos librairies pour les utiliser ensuite dans un render array - ou un
template.

* Récupérer les fichiers sources du plugin et les intégrer à votre module ;
* Déclarer les librairies dans le fichier de configuration ;
* Implémenter la dite librairie dans le render array

Pour en savoir plus, conultez
l'`API <https://www.drupal.org/docs/8/creating-custom-modules/adding-stylesheets-css-and-javascript-js-to-a-drupal-8-module>`_

.. code-block:: yaml

  rslides:
    css:
      theme:
        css/rslides.css: {}
    js:
      js/rslides.js: {}
      js/responsiveslides.min.js: {}


.. code-block:: php

  $build = [
    '#theme' => 'bloc_slideshow',
    '#slides' => $slides,
  ];
  // Manage asset.
  $build['#attached']['library'][] = 'custom_module/rslides';

!!!!
