*****************************
TP 1 - Éléments de correction
*****************************

Implementation du hook_theme()
==============================

* Vous déclarer votre hook dans le fichier de déclaration ``custom_module.module`` ;
* Vous déclarer votre templates dans un dossier ``templates`` ;

.. code-block:: php

  function custom_module_theme($existing, $type, $theme, $path){

    $themes['custom_template_block'] = [
      'template'  => 'custom_template',
      'path'  => drupal_get_path('module', 'custom_module').'/templates',
      'variables' => [
        'foo'  => NULL,
        'bar'  => NULL,
      ],
    ];
    return $themes;
  }


Implémentation dans le block
============================

.. code-block:: php

  $build = [
    '#theme' => 'custom_template_block',
    '#foo' => $foo,
    '#bar' => $bar,
  ];
  return $build;
