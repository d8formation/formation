.. # define break line
.. |br| raw:: html

  <br />


****************************
Les fonctions preprocess
****************************

Les fonctions de preprocess vous permettent d'intervenir sur les données avant que celles-ci ne soient rendues. |br|
Dans ces fonctions, vous avez accès aux variables rendues et vous pouvez réaliser des tests sur des valeurs pour conditionner leur affichage, leur mise en forme, ajouter des structures (block, vue ...) de manière dynamique....

C'est un hook générique : ``THEME_NAME_preprocess_HOOK`` |br|
ou ``HOOK`` définit la portion de thème rendue.

.. code-block:: php

  custom_theme_preprocess_node(&$variables) {}
  custom_theme_preprocess_page(&$variables) {}
  custom_theme_preprocess_block(&$variables) {}
  custom_theme_preprocess_input(&$variables) {}


.. NOTE::
  Seul les données traitées par le hook sont disponibles. |br|
  Un **hook_preprocess_page** embarque plus de données qu'un **hook_preprocess_input**.


Implémentation
====================

Dans le fichier de déclaration ``custom_theme.theme``.

.. code-block:: php

  function custom_theme_preprocess_node(&$variables) {
    $node = $variables['node'];
    $type = $variables['node']->getType();
    $view_mode  = $variables['view_mode'];
  }


Suggestion de thème
=========================

Avec le même principe de généricité, l'utilisation du hook ``THEME_NAME_theme_suggestions_HOOK_alter`` vous permettra de définir des suggestions de template.

Un exemple d'implémentation pour avoir la possibilité d'utiliser des gabarits de page différents, fonction du type de node rendu.

.. code-block:: php

  function custom_theme_theme_suggestions_page_alter(array &$suggestions, array $variables) {

    if ($node = \Drupal::routeMatch()->getParameter('node')) {
      $content_type = $node->bundle();
      $suggestions[] = 'page__'.$content_type;
    }
  }
