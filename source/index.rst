.. |br| raw:: html

  <br />


Formation Drupal Développement
==============================

.. toctree::
  :caption: Introduction
  :numbered:
  :maxdepth: 1

  content/00-introduction/objectifs
  content/00-introduction/framework
  content/00-introduction/tools
  content/00-introduction/installation
  content/00-introduction/drupal-structure
  content/00-introduction/modules
  content/00-introduction/settings
  content/00-introduction/configurations-monitoring
  content/00-introduction/dev-mode-configuration


.. toctree::
  :caption: TP Installation
  :maxdepth: 1

  content/00-introduction/TP-config/index.rst


.. toctree::
  :caption: Concepts et back-office Drupal
  :numbered:
  :maxdepth: 1

  content/01-concepts-drupal/objectifs
  content/01-concepts-drupal/datas-structure
  content/01-concepts-drupal/entites-configurations
  content/01-concepts-drupal/node
  content/01-concepts-drupal/taxonomy
  content/01-concepts-drupal/user
  content/01-concepts-drupal/block-content

.. toctree::
  :caption: TP concepts et back-office
  :maxdepth: 1

  content/01-concepts-drupal/TP-bo/index.rst

.. toctree::
  :caption: Développer ses composants
  :numbered:
  :maxdepth: 1

  content/02-module-custom/objectifs
  content/02-module-custom/module-structure
  content/02-module-custom/routing
  content/02-module-custom/controller
  content/02-module-custom/entity-management
  content/02-module-custom/services-injection
  content/02-module-custom/entity-query
  content/02-module-custom/database-query
  content/02-module-custom/form
  content/02-module-custom/hook
  content/02-module-custom/block
  content/02-module-custom/cache
  content/02-module-custom/services
  content/02-module-custom/events


.. toctree::
  :caption: TP Développer ses composants
  :maxdepth: 1

  content/02-module-custom/TP/TP-controller/index.rst
  content/02-module-custom/TP/TP-query/index.rst
  content/02-module-custom/TP/TP-form/index.rst
  content/02-module-custom/TP/TP-hook/index.rst
  content/02-module-custom/TP/TP-block/index.rst
  content/02-module-custom/TP/TP-cache/index.rst
  content/02-module-custom/TP/TP-service/index.rst
  content/02-module-custom/TP/TP-event/index.rst


.. toctree::
  :caption: Création d'un thème
  :numbered:
  :maxdepth: 1

  content/03-custom-theme/objectifs
  content/03-custom-theme/hook-theme
  content/03-custom-theme/theme-custom
  content/03-custom-theme/fonctions-preprocess
  content/03-custom-theme/template
  content/03-custom-theme/twig-notions
  content/03-custom-theme/gestion-asset


.. toctree::
  :caption: TP Création d'un thème

  content/03-custom-theme/TP/TP1/index
  content/03-custom-theme/TP/TP2/index
  content/03-custom-theme/TP/TP3/index


.. toctree::
  :caption: Gestion de la configuration
  :numbered:
  :maxdepth: 1

  content/04-configuration/objectifs
  content/04-configuration/gestion-configuration
  content/04-configuration/exemple-workflow
  content/04-configuration/MAJ-drupal
  content/04-configuration/profil-installation


.. toctree::
  :caption: TP Gestion de la configuration

  content/04-configuration/TP/tp


.. toctree::
  :caption: Ressources
  :maxdepth: 1

  content/ressources/configuration-vhost
  content/ressources/git
  content/ressources/entity-configuration
  content/ressources/injection-service
  content/ressources/drush
  content/ressources/drupal-console


Licence
=======

.. figure:: _static/by-nc-sa.eu.png
    :align: center

En savoir plus |br|
`https://creativecommons.org/licenses/by-nc-sa/4.0/ <https://creativecommons.org/licenses/by-nc-sa/4.0/>`_
