### A propos de cette documentation

[https://readthedocs.org/](https://readthedocs.org/)
[https://docs.readthedocs.io/en/stable/index.html](https://docs.readthedocs.io/en/stable/index.html)


#### Installation

##### Linux


```
// installation readthedocs
pip install sphinx sphinx-autobuild

// installation theme
pip install sphinx_rtd_theme
pip install sphinx-tabs
```

##### Windows

Pour faire cette installation, nous allons utiliser [Chocolatey](https://chocolatey.org/), un gestionnaire de paquet
pour installer Python et son installateur de projet.

Documentation :

* [https://chocolatey.org/install](https://chocolatey.org/install)
* [https://docs.python-guide.org/starting/install3/win/](https://docs.python-guide.org/starting/install3/win/)
* [https://www.sphinx-doc.org/en/master/usage/installation.html#windows](https://www.sphinx-doc.org/en/master/usage/installation.html#windows)


###### Installer chocolatey

* Ouvrir PowerShell ;
* Taper la commande `Get-ExecutionPolicy`
	* Si la commande renvoie `Restricted`, tapez la commande `Set-ExecutionPolicy Bypass -Scope Process`
* Tapez ensuite la commande  

```
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
```
* Taper ensuite `choco -?` pour vérifier que l'installation s'est passé.

###### Installer python et l'installateur de projet

```
choco install python
// redemarrer powershell
python -m pip install -U pip
```

###### Installer Sphinx et les composantes du thème

```
// installation readthedocs
pip install sphinx sphinx-autobuild

// installation theme
pip install sphinx_rtd_theme sphinx-tabs sphinxcontrib-phpdomain
```

### Builder la documentation

cd formation
```
make html
```

### Pour en savoir plus

* [https://docs.readthedocs.io/en/rel/getting_started.html](https://docs.readthedocs.io/en/rel/getting_started.html)
* [https://docutils.sourceforge.io/docs/user/rst/quickstart.html](https://docutils.sourceforge.io/docs/user/rst/quickstart.html)
* [https://sphinx-rtd-theme.readthedocs.io/en/latest/index.html](https://sphinx-rtd-theme.readthedocs.io/en/latest/index.html)
* [https://github.com/djungelorm/sphinx-tabs](https://github.com/djungelorm/sphinx-tabs)
* [https://pygments.org/](https://pygments.org/)
